/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import MainApp from './src/MainApp';
import moment from 'moment';

moment.locale('id');

export default class App extends Component {
  render() {
    return <MainApp />;
  }
}

console.disableYellowBox = true;
