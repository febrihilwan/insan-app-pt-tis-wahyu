import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/dist/FontAwesome";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { widthPercentageToDP, heightPercentageToDP } from "../../utils";
import { updatePDIData } from "../../actions/pdi_actions";
import colors from '../../utils/colors'

class InputInterior extends Component {
  constructor(props) {
    super(props);
  }

  selectLevel(value) {
    const { index, photo, updatePDIData } = this.props;
    photo.level === value ? (photo.level = "") : (photo.level = value);
    updatePDIData("photoInterior", index, photo);
  }

  selectDamage(value) {
    const { index, photo, updatePDIData } = this.props;
    photo.damage === value ? (photo.damage = "") : (photo.damage = value);
    updatePDIData("photoInterior", index, photo);
  }

  render() {
    const { photo, onPressPhoto } = this.props;
    return (
      <View style={styles.inputContainer}>
        <View style={styles.photoContainer}>
          <Text style={styles.photoText}>{photo.name}</Text>
        </View>
        <View style={styles.engineButton}>
          {photo.name === "Interior Overall" ? (
            <View style={styles.row}>
              <TouchableOpacity
                onPress={() => this.selectLevel("Light")}
                style={[styles.iconContainer, photo.level === "Light" ? styles.iconSelected : styles.iconActive]}
              >
                <Text style={styles.buttonText}>Light</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.selectLevel("Medium")}
                style={[styles.iconContainer, photo.level === "Medium" ? styles.iconSelected : styles.iconActive]}
              >
                <Text style={styles.buttonText}>Med</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.selectLevel("Heavy")}
                style={[styles.iconContainer, photo.level === "Heavy" ? styles.iconSelected : styles.iconActive]}
              >
                <Text style={styles.buttonText}>Heavy</Text>
              </TouchableOpacity>
            </View>
          ) : photo.name === "Odour" ? (
            <View style={styles.row}>
              <TouchableOpacity
                onPress={() => this.selectLevel("Strong")}
                style={[styles.iconContainer, photo.level === "Strong" ? styles.iconSelected : styles.iconActive]}
              >
                <Text style={styles.buttonText}>Strong</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.selectLevel("Heavy")}
                style={[styles.iconContainer, photo.level === "Heavy" ? styles.iconSelected : styles.iconActive]}
              >
                <Text style={styles.buttonText}>Heavy</Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View style={styles.row}>
              <TouchableOpacity
                onPress={() => this.selectDamage("P")}
                style={[styles.iconContainer, photo.damage === "P" ? styles.iconSelected : styles.iconActive]}
              >
                <Text style={styles.buttonText}>P</Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.selectDamage("B")}
                style={[styles.iconContainer, photo.damage === "B" ? styles.iconSelected : styles.iconActive]}
              >
                <Text style={styles.buttonText}>B</Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.selectDamage("X")}
                style={[styles.iconContainer, photo.damage === "X" ? styles.iconSelected : styles.iconActive]}
              >
                <Text style={styles.buttonText}>X</Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.selectLevel("1")}
                style={[styles.iconContainer, photo.level === "1" ? styles.iconSelected : styles.iconActive]}
              >
                <Text style={styles.buttonText}>1</Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.selectLevel("2")}
                style={[styles.iconContainer, photo.level === "2" ? styles.iconSelected : styles.iconActive]}
              >
                <Text style={styles.buttonText}>2</Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.selectLevel("3")}
                style={[styles.iconContainer, photo.level === "3" ? styles.iconSelected : styles.iconActive]}
              >
                <Text style={styles.buttonText}>3</Text>
              </TouchableOpacity>
            </View>
          )}

          <TouchableOpacity
            onPress={() => onPressPhoto(photo.name)}
            style={[styles.iconContainer, photo.imageUrl !== "" ? styles.iconSelected : styles.iconActive]}
          >
            <Icon name="camera" size={widthPercentageToDP('6.5%')} color="white" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: "column",
    justifyContent: "center",
    paddingVertical: widthPercentageToDP("4%"),
    borderBottomWidth: 2,
    borderColor: colors.light_sapphire_bluish_gray,
  },
  photoContainer: {
    flex: 1,
    justifyContent: "center",
    marginBottom: widthPercentageToDP('2%')
  },
  iconContainer: {
    width: widthPercentageToDP('12.4%'),
    height: widthPercentageToDP('12.4%'),
    alignItems: "flex-start",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "white",
    borderWidth: 1,
    borderRadius: 5
  },
  iconDisabled: {
    backgroundColor: colors.very_soft_blue,
  },
  iconActive: {
    backgroundColor: colors.cyan_blue,
  },
  iconSelected: {
    backgroundColor: colors.cyan_green,
  },
  photoText: {
    // fontSize: widthPercentageToDP('4.5%'),
    // fontFamily: 'Roboto',
    color: colors.dope_blue,
    fontWeight: 'bold'
  },
  engineButton: {
    flexDirection: "row",
    justifyContent: 'space-between',
    paddingRight: widthPercentageToDP('5%'),
  },
  row: {
    flexDirection: 'row'
  },
  buttonText: {
    textAlign: "center",
    color: "white",
    fontFamily: 'Roboto',
    fontSize: widthPercentageToDP('3.7%')
  },
});

const mapDispatchToProps = dispatch => bindActionCreators({ updatePDIData }, dispatch);

export default connect(
  null,
  mapDispatchToProps,
)(InputInterior);
