import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";

function AccidentButton(props) {
  return (
    <View style={styles.frameButton}>
      <TouchableOpacity
        disabled={props.photo === "disabled"}
        onPress={() => {
          props.selectAccident("Dent");
        }}
        style={[
          styles.iconContainer,
          props.accident === "Dent" ? styles.iconSelected : styles.iconActive,
          buttonDisabled ? styles.iconDisabled : {},
        ]}
      >
        <Text style={styles.buttonText}>Dent</Text>
      </TouchableOpacity>

      <TouchableOpacity
        disabled={buttonDisabled}
        onPress={() => {
          props.selectAccident("Repaired");
        }}
        style={[
          styles.iconContainer,
          props.accident === "Repaired" ? styles.iconSelected : styles.iconActive,
          buttonDisabled ? styles.iconDisabled : {},
        ]}
      >
        <Text style={styles.buttonText}>Repaired</Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => {
          props.selectAccident("Replaced");
        }}
        disabled={props.replaceAndS}
        style={[
          styles.iconContainer,
          props.replaceAndS ? styles.iconDisabled : styles.iconActive,
          props.accident === "Replaced" ? styles.iconSelected : {},
        ]}
      >
        <Text style={styles.buttonText}>Replaced</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = {
  iconContainer: {
    width: 50,
    height: 50,
    alignItems: "flex-start",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "white",
    borderWidth: 1,
  },
  iconDisabled: {
    backgroundColor: "#82B1FF",
  },
  iconActive: {
    backgroundColor: "#1E88E5",
  },
  iconSelected: {
    backgroundColor: "#d50000",
  },
  photoText: {
    fontWeight: "600",
    fontSize: 18,
  },
  frameButton: {
    flexDirection: "row",
  },
  buttonText: {
    textAlign: "center",
    color: "white",
  },
};

export default AccidentButton;
