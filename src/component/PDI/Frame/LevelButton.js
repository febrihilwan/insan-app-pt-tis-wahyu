import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { widthPercentageToDP, heightPercentageToDP} from '../../../utils/index'
import colors from '../../../utils/colors'

function LevelButton(props) {
  return (
    <View style={styles.frameButton}>
      <TouchableOpacity
        disabled={props.level === "Disabled" || props.rust === "Yes"}
        onPress={() => {
          props.selectLevel("S");
        }}
        style={[
          styles.iconContainer,
          props.level === "Disabled" ? styles.iconDisabled : styles.iconActive,
          props.level === "S" ? styles.iconSelected : {},
          props.rust === "Yes" ? styles.iconDisabled : {},
        ]}
      >
        <Text style={styles.buttonText}>S</Text>
      </TouchableOpacity>

      <TouchableOpacity
        disabled={props.level === "Disabled"}
        onPress={() => {
          props.selectLevel("M");
        }}
        style={[
          styles.iconContainer,
          props.level === "M" ? styles.iconSelected : styles.iconActive,
          props.level === "Disabled" ? styles.iconDisabled : {},
        ]}
      >
        <Text style={styles.buttonText}>M</Text>
      </TouchableOpacity>

      <TouchableOpacity
        disabled={props.level === "Disabled"}
        onPress={() => {
          props.selectLevel("L");
        }}
        style={[
          styles.iconContainer,
          props.level === "L" ? styles.iconSelected : styles.iconActive,
          props.level === "Disabled" ? styles.iconDisabled : {},
        ]}
      >
        <Text style={styles.buttonText}>L</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet. create({
  iconContainer: {
    width: widthPercentageToDP('12.4%'),
    height: widthPercentageToDP('12.4%'),
    alignItems: "flex-start",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "white",
    borderWidth: 1,
    borderRadius: 5
  },
  iconDisabled: {
    backgroundColor: colors.very_soft_blue,
  },
  iconActive: {
    backgroundColor: colors.cyan_blue,
  },
  iconSelected: {
    backgroundColor: colors.cyan_green,
  },
  photoText: {
    fontWeight: "600",
    fontSize: 18,
  },
  frameButton: {
    flexDirection: "row",
  },
  buttonText: {
    textAlign: "center",
    color: "white",
  },
});

export default LevelButton;
