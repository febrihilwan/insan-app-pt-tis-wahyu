import React, {useState} from 'react';
import {TouchableOpacity, Text, View} from 'react-native';
import {Icon, Overlay} from 'react-native-elements';

const FormSelect = ({selectedValue, items, onSelectedItem, label}) => {
  const [visible, setVisible] = useState(false);
  return (
    <TouchableOpacity onPress={() => setVisible(true)} disabled={true}>
      <View
        style={{
          padding: 8,
          borderWidth: 0.5,
          borderRadius: 3,
          borderColor: 'grey',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          width: '100%',
        }}>
        <Text style={{color: selectedValue ? 'black' : '#eee'}}>
          {selectedValue ? selectedValue : `Pilih ${label}`}
        </Text>
        <Icon name="keyboard-arrow-down" type="material" />
      </View>
      <Overlay
        overlayStyle={{width: '80%'}}
        isVisible={visible}
        windowBackgroundColor="rgba(0, 0, 0, 0.3)"
        overlayBackgroundColor="white"
        onBackdropPress={() => setVisible(false)}
        width="100%"
        height="auto">
        {items.map(item => {
          return (
            <TouchableOpacity
              onPress={() => {
                setVisible(false);
                onSelectedItem({text: item[label], id: item.id});
              }}
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginVertical: 10,
                // justifyContent: 'space-around',
              }}>
              <Icon
                name={
                  selectedValue == item[label]
                    ? 'radio-button-checked'
                    : 'radio-button-unchecked'
                }
                type="material"
                iconStyle={{color: 'grey'}}
              />
              <Text style={{marginLeft: 15, color: 'grey', fontWeight: 'bold'}}>
                {item[label]}
              </Text>
            </TouchableOpacity>
          );
        })}
      </Overlay>
    </TouchableOpacity>
  );
};

export default FormSelect;
