import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/dist/FontAwesome";
import Colors from '../utils/colors'

import { widthPercentageToDP, heightPercentageToDP } from "../utils";

class InputPhoto extends Component {
  render() {
    const { photo, onPressPhoto } = this.props;
    // console.log(photo)
    return (
      <View style={styles.inputContainer}>
        <View style={styles.photoContainer}>
          <Text style={styles.photoText}>{photo.name}</Text>
        </View>
        <View style={{ alignItems: "flex-start", flexDirection: "row" }}>
          <TouchableOpacity
            onPress={() => onPressPhoto(photo.name)}
            style={[styles.iconContainer, photo.imageUrl !== "" ? styles.iconSelected : styles.iconActive]}
          >
            <Icon name="camera" size={30} color="white" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: "row",
    justifyContent: "center",
    paddingHorizontal: widthPercentageToDP("3%"),
    paddingVertical: heightPercentageToDP("1%"),
    borderBottomWidth: 2,
    borderColor: Colors.light_sapphire_bluish_gray,
  },
  photoContainer: {
    flex: 1,
    justifyContent: "center",
  },
  iconContainer: {
    width: 50,
    height: 50,
    // alignItems: "flex-start",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "white",
    borderWidth: 1,
    borderRadius: 5
  },
  iconDisabled: {
    backgroundColor: "#82B1FF",
  },
  iconActive: {
    backgroundColor: Colors.cyan_blue
  },
  iconSelected: {
    backgroundColor: Colors.cyan_green,
  },
  photoText: {
    fontSize: widthPercentageToDP('4.5%'),
    fontFamily: 'Roboto',
    fontWeight: '400',
    color: Colors.dope_blue
  },
});

export default InputPhoto;
