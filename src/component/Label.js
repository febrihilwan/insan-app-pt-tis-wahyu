import React from 'react';
import {Text} from 'react-native';

const Label = ({text, color, fontSize, style}) => {
  return (
    <Text
      style={{
        color: color ? color : 'black',
        fontSize: fontSize ? fontSize : 14,
        fontWeight: 'bold',
        marginBottom: 5,
        ...style,
      }}>
      {text}
    </Text>
  );
};

export default Label;
