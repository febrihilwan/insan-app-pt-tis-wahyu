import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/dist/FontAwesome";
import { connect } from "react-redux";

import { widthPercentageToDP, heightPercentageToDP } from "../utils";
import { updateAppraisalData } from "../actions/appraisal_actions";
import colors from '../utils/colors'

class InputEngine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      level: "",
    };
  }

  selectLevel(value) {
    let { photo, updateAppraisalData, index } = this.props;
    photo.level === value ? (photo.level = "") : (photo.level = value);

    updateAppraisalData("photoEngine", index, photo);
  }

  render() {
    const { photo, onPressPhoto } = this.props;
    return (
      <View style={[styles.box, !photo.type ? styles.background : null]}>
        <View style={styles.inputContainer}>
          <View style={styles.photoContainer}>
            <Text style={styles.photoText}>{photo.name}</Text>
          </View>
          {photo.type ? null : (
            <View style={styles.engineButton}>
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity
                  onPress={() => this.selectLevel("S")}
                  style={[styles.iconContainer, photo.level === "S" ? styles.iconSelected : styles.iconActive]}
                >
                  <Text style={styles.buttonText}>S</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this.selectLevel("L")}
                  style={[styles.iconContainer, photo.level === "L" ? styles.iconSelected : styles.iconActive]}
                >
                  <Text style={styles.buttonText}>L</Text>
                </TouchableOpacity>
              </View>
              

              <TouchableOpacity
                onPress={() => onPressPhoto(photo.name)}
                style={[styles.iconContainer, photo.imageUrl !== "" ? styles.iconSelected : styles.iconActive]}
              >
                <Icon name="camera" size={widthPercentageToDP('6.5%')} color="white" />
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: "column",
    justifyContent: "center",
    paddingVertical: widthPercentageToDP("4%"),
    borderColor: colors.light_sapphire_bluish_gray,
    borderBottomWidth: 2,
  },
  box: {
    paddingLeft: widthPercentageToDP("5%"),    
  },
  background: {
    backgroundColor: 'white'
  },
  photoContainer: {
    flex: 1,
    justifyContent: "center",
  },
  iconContainer: {
    width: widthPercentageToDP('12.4%'),
    height: widthPercentageToDP('12.4%'),
    alignItems: "flex-start",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "white",
    borderWidth: 2,
    borderRadius: 5
  },
  iconDisabled: {
    backgroundColor: colors.very_soft_blue,
  },
  iconActive: {
    backgroundColor: colors.cyan_blue,
  },
  iconSelected: {
    backgroundColor: colors.cyan_green,
  },
  frameButton: {
    flexDirection: "row",
    
  },
  photoText: {
    fontSize: widthPercentageToDP('4.5%'),
    fontFamily: 'Roboto',
    color: colors.dope_blue
  },
  engineButton: {
    flexDirection: "row",
    justifyContent: 'space-between',
    paddingRight: widthPercentageToDP('5%'),
    marginTop: widthPercentageToDP('2%')
  },
  buttonText: {
    textAlign: "center",
    color: "white",
    fontSize: widthPercentageToDP('4%'),
    fontFamily: 'Roboto',
  },
});

const mapDispatchToProps = dispatch => {
  return {
    updateAppraisalData: (partName, index, data) => {
      dispatch(updateAppraisalData(partName, index, data));
    },
  };
};

export default connect(
  null,
  mapDispatchToProps,
)(InputEngine);
