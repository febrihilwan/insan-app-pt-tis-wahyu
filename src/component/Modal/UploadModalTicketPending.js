/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-alert */
import React, {Component} from 'react';
import Modal from 'react-native-modal';
import _ from 'lodash';
import moment from 'moment';
import {
  View,
  Text,
  Platform,
  TouchableOpacity,
  Image,
  ScrollView,
  Dimensions,
  StyleSheet,
  AsyncStorage,
  UIManager,
} from 'react-native';
import Axios from 'axios';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import {widthPercentageToDP, heightPercentageToDP} from '../../utils';
import colors from '../../utils/colors';
import Spinner from 'react-native-loading-spinner-overlay';
import TicketInfo from './TicketWorkPendingAccordion/TicketInfo';
import ProblemInfo from './TicketWorkPendingAccordion/ProblemInfo';
import EngineerInfo from './TicketWorkPendingAccordion/EngineerInfo';
import AtmEngineerInfo from './TicketWorkPendingAccordion/AtmEngineerInfo';
import TimeStamp from './TicketWorkPendingAccordion/TimeStamp';
import HealthCheck from './TicketWorkPendingAccordion/HealthCheck';
import ProblemAndAction from './TicketWorkPendingAccordion/ProblemAndAction';
import Miscellaneous from './TicketWorkPendingAccordion/Miscellaneous';
import Geotaging from './TicketWorkPendingAccordion/Geotaging';
import {API_URL} from 'react-native-dotenv';
import ReplacementPart from './TicketWorkPendingAccordion/ReplacementPart';
import checkFields from '../../utils/checkFields';

class UploadModalTicketPending extends Component {
  constructor(props) {
    super(props);
    this.onChangeText = this.onChangeText.bind(this);
    this._onChangeReplacePart = this._onChangeReplacePart.bind(this);
    this.state = {
      name: '',
      email: '',
      no_telp: '',
      updated_at: '',
      username: '',
      api_token: '',
      img_user: '',
      image_problem: '',
      password: '',
      kelanjutan_problem: '',
      grounding: '',
      it: '',
      voltage: '',
      ac: '',
      ups: '',
      notes: '',
      cm: '',
      pm: '',
      charge: '',
	  work_finish: new Date(),
      spinner: false,
      expanded: false,
      statusLocationUser: true,
      replacementPart: [],
    };
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  captureDistance = (dis) => {
    if (dis <= 50) {
      this.setState({
        statusLocationUser: false,
      });
    }
  };

  async componentDidMount() {
    // [TEST] SPINNER
    // setInterval(() => {
    //   this.setState({
    //     spinner: !this.state.spinner,
    //   });
    // }, 3000);
    try {
      let userData = await AsyncStorage.getItem('userData');
      if (userData) {
        let data = JSON.parse(userData);
        // console.log('userData', data);
        this.setState({
          api_token: data.api_token.api_token,
        });
      }
    } catch (err) {
      // console.log(err);
      // console.log('Failed local login');
    }
  }

  onChangeText(text, name) {
    this.setState({
      [name]: text,
    });
  }

  _onChangeReplacePart(objectReplacement) {
    this.setState((prevState) => ({
      replacementPart: [...prevState.replacementPart, objectReplacement],
    }));
  }

  _takePicture(field) {
    const options = {
      title: `Upload foto ${field} anda`,
      storageOptions: {
        path: 'images',
      },
      compressImageQuality: 0.1,
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
        console.error('Error pick image', response.error);
      } else {
        // if(this.state[field]) {
        //   let temp = {
        //     uri: response.uri,
        //     type: response.type,
        //     name: response.fileName,
        //   }

        //   let tempField = this.state[field]
        //   tempField.push(temp)

        //   this.setState({
        //     [field]: tempField
        //   });

        // } else {
        this.setState({
          [field]: {
            uri: response.uri,
            type: 'image/jpeg',
            name: response.fileName,
            path: response.path,
          },
        });
        // }
      }
    });
  }

  handleOnScroll = (event) => {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y,
    });
  };

  handleScrollTo = (p) => {
    if (this.scrollViewRef) {
      this.scrollViewRef.scrollTo(p);
    }
  };

  checkingImage = () => {
    let {image_problem} = this.state;
    let {document, AppraisalData} = this.props;
    let data = _.find(AppraisalData.data, {
      objectField: {name: image_problem.name},
    });
    // console.log("--",document)
    if (image_problem) {
      return (
        <Image
          source={{uri: image_problem.uri}}
          style={styles.modalImagePicker}
        />
      );
    } else {
      return null;
    }
  };

  async _pushToDatabase(formData) {
    const {api_token} = this.state;
    const {dataWorkStart, togglePhotoModal} = this.props;

    const url = `${API_URL}/pending_array/${dataWorkStart.id}`;
    const config = {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${api_token}`,
        'Access-Control-Allow-Origin': '*',
        Accept: 'multipart/form-data',
        'content-type': 'multipart/form-data',
      },
      data: formData,
    };

    try {
      const response = await Axios(url, config);
	  console.log(response);
      await this.setState({spinner: false});
      await togglePhotoModal();
      await alert('Sukses Submit Data');
      await this.setState({
        displayPhotoModal: !this.props.displayPhotoModal,
      });
    } catch (error) {
      console.log(error.response);
      await this.setState({spinner: false});
      await alert('Gagal Submit Data');
    }
  }

  onPressUpload = async () => {
    const {
      api_token,
      grounding,
      it,
      voltage,
      ac,
      ups,
      notes,
      kelanjutan_problem,
      pm,
      cm,
      charge,
      image_problem,
      replacementPart,
	  work_finish,
    } = this.state;

    const {dataWorkStart, togglePhotoModal} = this.props;

    let poData = [
      {name: 'Grounding', value: grounding, required: false},
      {name: 'IT', value: it, required: false},
      {name: 'Voltage', value: voltage, required: false},
      {name: 'AC', value: ac, required: false},
      {name: 'UPS', value: ups, required: false},
      {name: 'Notes', value: notes, required: false},
      {name: 'PM', value: pm, required: true},
      {name: 'CM', value: cm, required: true},
      {name: 'Charge', value: charge, required: true},
      {name: 'Kelanjutan Problem', value: kelanjutan_problem, required: true},
    ];

    let emptyField = checkFields(poData);

    const isEmptyFields = _.size(emptyField) === 0;

    if (isEmptyFields) {
      this.setState({
        spinner: true,
      });
	  
      const isEmptyReplacePart = _.size(replacementPart) === 0;

      let formData = new FormData();
      formData.append('problem_id', `${dataWorkStart.id}`);
      formData.append('grounding', grounding || '');
      formData.append('it', it || '');
      formData.append('voltage', voltage || '');
      formData.append('ac', ac || '');
      formData.append('ups', ups || '');
      formData.append('notes', notes || '');
      formData.append('kelanjutan_problem', kelanjutan_problem);
      formData.append('pm', pm);
      formData.append('cm', cm);
      formData.append('charge', charge);
	  formData.append('work_start', dataWorkStart.work_start || moment().format('YYYY-MM-DD HH:mm'));
	  formData.append('work_finish', moment(work_finish).format('YYYY-MM-DD HH:mm'));

	  const uniqReplacement = _.uniqWith(replacementPart, function (o){
	  	return o.numberPart;
	   });
      
	  for(let i=0; i < 5; i++) {
        formData.append('number_part[]', i + 1);
		formData.append('part_name[]', uniqReplacement[i] ? uniqReplacement[i]['partName'] : '');
		formData.append('removed_serial[]', uniqReplacement[i] ? uniqReplacement[i]['removeSerialNumber'] : '');
		formData.append('removed_part[]', uniqReplacement[i] ? uniqReplacement[i]['removePartNumber'] : '');
		formData.append('replacing_part[]', uniqReplacement[i] ? uniqReplacement[i]['replacingPartNumber'] : '');
		formData.append('replacing_serial[]', uniqReplacement[i] ? uniqReplacement[i]['replacingSerialNumber'] : '');
      }
	  
	  // const uniqReplacement = _.uniqWith(replacementPart, function (o){
	  //	return o.numberPart;
	  // });
	  // uniqReplacement.map(itemPart => {
		// formData.append('part_name[]', itemPart.partName);
		// formData.append('removed_serial[]', itemPart.removeSerialNumber);
		// formData.append('removed_part[]', itemPart.removePartNumber);
		// formData.append('replacing_part[]', itemPart.replacingPartNumber);
		// formData.append('replacing_serial[]', itemPart.replacingSerialNumber);
	  // });

      formData.append('image_problem', image_problem);

      this._pushToDatabase(formData);
    } else {
      if (_.isArray(emptyField)) {
        const messageText = emptyField.map((itemField) => {
          return `Field ${itemField.name} wajib di isi.\n`;
        });
        alert(messageText.join(''));
      }
    }
  };

  checkPartNumber(e, name) {
    console.log(e, name);
    if (e) {
      if (name === 'number_part_satu') {
        this.setState({
          [name]: 1,
        });
      } else if (name === 'number_part_dua') {
        this.setState({
          [name]: 2,
        });
      } else if (name === 'number_part_tiga') {
        this.setState({
          [name]: 3,
        });
      } else if (name === 'number_part_empat') {
        this.setState({
          [name]: 4,
        });
      } else if (name === 'number_part_lima') {
        this.setState({
          [name]: 5,
        });
      }
    }
  }

  render() {
    const {displayPhotoModal, togglePhotoModal, dataWorkStart} = this.props;

    const {
      spinner,
      kelanjutan_problem,
      grounding,
      it,
      voltage,
      ac,
      ups,
      pm,
      cm,
      charge,
      notes,
      statusLocationUser,
	  replacementPart,
    } = this.state;
    const partNumber = [1, 2, 3, 4, 5];
	
    return (
      <Modal
        supportedOrientations={['portrait', 'landscape']}
        onBackdropPress={() => togglePhotoModal()}
        isVisible={displayPhotoModal}
        style={styles.photoModal}>
        <Spinner
          visible={spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <View
          style={
            Dimensions.get('window').height <= 360 ? styles.scrollableModal : {}
          }>
          <View style={styles.modalContainer}>
            <View style={styles.modalHeader}>
              <TouchableOpacity onPress={() => togglePhotoModal()}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: widthPercentageToDP('8%'),
                    fontFamily: 'Roboto',
                  }}>
                  x
                </Text>
              </TouchableOpacity>
            </View>
            <ScrollView
              ref={(ref) => (this.scrollViewRef = ref)}
              onScroll={this.handleOnScroll}
              scrollEventThrottle={16}>
              <View style={styles.modalContent}>
                <Text style={styles.modalPhotoName}>Pending</Text>
                <View style={styles.containerAccordian}>
                  <TicketInfo dataWorkStart={dataWorkStart} />
                </View>
                <View style={styles.containerAccordian}>
                  <ProblemInfo
                    kelanjutan_problem={kelanjutan_problem}
                    dataWorkStart={dataWorkStart}
                    onChangeText={this.onChangeText}
                  />
                </View>
                <View style={styles.containerAccordian}>
                  <EngineerInfo dataWorkStart={dataWorkStart} />
                </View>
                <View style={styles.containerAccordian}>
                  <AtmEngineerInfo dataWorkStart={dataWorkStart} />
                </View>
                <View style={styles.containerAccordian}>
                  <TimeStamp dataWorkStart={dataWorkStart} />
                </View>
                <View style={styles.containerAccordian}>
                  <HealthCheck
                    onChangeText={this.onChangeText}
                    dataWorkStart={dataWorkStart}
                    it={it}
                    voltage={voltage}
                    grounding={grounding}
                    ac={ac}
                    ups={ups}
                  />
                </View>
                <View style={styles.containerAccordian}>
                  <View style={styles.headerAccordian}>
                    <Text style={[styles.titleAccordian, styles.fontAccordian]}>
                      PART REMOVAL & REPLACEMENT
                    </Text>
                  </View>
                </View>
                {partNumber.map((itemPart, _index) => (
                  <View style={styles.containerAccordian}>
                    <ReplacementPart
                      numberPart={itemPart}
                      onChange={this._onChangeReplacePart}
                    />
                  </View>
                ))}
                <View style={styles.containerAccordian}>
                  <ProblemAndAction
                    notes={notes}
                    onChangeText={this.onChangeText}
                    dataWorkStart={dataWorkStart}
                  />
                </View>
                <View style={styles.containerAccordian}>
                  <Miscellaneous
                    onChangeText={this.onChangeText}
                    dataWorkStart={dataWorkStart}
                    pm={pm}
                    cm={cm}
                    charge={charge}
                  />
                </View>
                <View style={styles.containerAccordian}>
                  <Geotaging
                    captureDistance={this.captureDistance}
                    dataWorkStart={dataWorkStart}
                  />
                </View>

                <ScrollView horizontal={true} nestedScrollEnabled={true}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-around',
                      width: '100%',
                      height: heightPercentageToDP('23%'),
                    }}>
                    {this.checkingImage()}
                    <TouchableOpacity
                      onPress={() => this._takePicture('image_problem')}>
                      <View style={styles.modalImagePicker}>
                        <Icon name="camera" size={50} />
                      </View>
                    </TouchableOpacity>
                  </View>
                </ScrollView>
                {/* <TextInput
                  onChangeText={(text) => onChangeText(text, document.name)}
                  multiline={true}
                  numberOfLines={5}
                  style={styles.modalNotesInput}
                  value={document.notes}
                  placeholder="Tulis keterangan di sini..."
                  placeholderTextColor={colors.light_grayish_blue}
                /> */}

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    marginTop: 20,
                    marginBottom: 30,
                  }}>
                  <TouchableOpacity
                    style={[
                      styles.viewPhotoButton,
                      styles.viewPhotoButtonActive,
                    ]}
                    onPress={() => togglePhotoModal()}>
                    <Icon
                      name="close"
                      size={widthPercentageToDP('6%')}
                      color="white"
                    />
                    <Text style={styles.buttonText}>Batalkan</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={[
                      styles.uploadPhotoButton,
                      statusLocationUser
                        ? styles.uploadPhotoButtonInactive
                        : styles.uploadPhotoButtonActive,
                    ]}
                    disabled={statusLocationUser ? true : false}
                    onPress={() => this.onPressUpload()}>
                    <Icon
                      name="save"
                      size={widthPercentageToDP('6%')}
                      color="white"
                    />
                    <Text style={styles.buttonText}>Simpan</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  containerModal: {
    flex: 1,
  },
  modalContainer: {
    height: heightPercentageToDP('90%'),
  },
  modalHeader: {
    height: heightPercentageToDP('7%'),
    backgroundColor: 'transparent',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    padding: 5,
  },
  modalContent: {
    paddingHorizontal: widthPercentageToDP('5%'),
    paddingVertical: heightPercentageToDP('1.5%'),
    backgroundColor: '#fff',
    borderRadius: 6,
  },
  modalPhotoName: {
    fontSize: widthPercentageToDP('6%'),
    fontFamily: 'Roboto',
    color: colors.dope_blue,
  },
  modalImagePicker: {
    height: widthPercentageToDP('25.3%'),
    width: widthPercentageToDP('25.3%'),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#778899',
    marginRight: widthPercentageToDP('2%'),
    borderRadius: 6,
  },
  modalNotesInput: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    textAlignVertical: 'top',
    fontSize: widthPercentageToDP('4.5%'),
    borderRadius: 5,
  },
  scrollableModal: {
    height: 250,
  },
  photoModal: {
    paddingBottom: 20,
  },
  scrollableModalContent1: {
    height: 200,
    backgroundColor: 'orange',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollableModalContent2: {
    height: 200,
    backgroundColor: 'lightgreen',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Roboto',
    fontSize: widthPercentageToDP('4%'),
    fontWeight: 'bold',
    marginLeft: 10,
  },
  viewPhotoButton: {
    width: widthPercentageToDP('37.5%'),
    borderRadius: 5,
    backgroundColor: colors.bright_red,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%'),
    justifyContent: 'center',
    flexDirection: 'row',
  },
  viewPhotoButtonActive: {
    backgroundColor: colors.strong_grey,
  },
  viewPhotoButtonInactive: {
    backgroundColor: colors.very_soft_red,
  },
  uploadPhotoButton: {
    width: widthPercentageToDP('37.5%'),
    borderRadius: 5,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%'),
    justifyContent: 'center',
    flexDirection: 'row',
  },
  uploadPhotoButtonActive: {
    backgroundColor: colors.strong_blue,
  },
  uploadPhotoButtonInactive: {
    backgroundColor: colors.very_soft_blue,
  },
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
  },
  containerAccordian: {
    flex: 1,
    paddingTop: 0,
    backgroundColor: 'white',
    borderColor: colors.light_sapphire_bluish_gray,
    borderWidth: 2,
    borderRadius: 10,
    paddingHorizontal: 16,
    marginTop: 16,
  },
  titleAccordian: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  headerAccordian: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
});

const mapStateToProps = ({AppraisalData}) => ({AppraisalData});

export default connect(mapStateToProps)(UploadModalTicketPending);
