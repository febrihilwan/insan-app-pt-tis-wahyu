import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  LayoutAnimation,
  Platform,
  UIManager,
  TextInput,
  Picker,
} from 'react-native';
import colors from '../../../utils/colors';
import {widthPercentageToDP, heightPercentageToDP} from '../../../utils';
import Icon from 'react-native-vector-icons/MaterialIcons';
import DatePicker from 'react-native-datepicker';

export default class RemovalAndReplacementPartFive extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      expanded: false,
    };

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  render() {
    const {
      onChangeText,
      number_part_lima,
      part_name_lima,
      removed_part_lima,
      removed_serial_lima,
      replacing_part_lima,
      replacing_serial_lima,
    } = this.props;
    return (
      <View>
        <TouchableOpacity
          ref={this.accordian}
          style={styles.row}
          onPress={() => this.toggleExpand()}>
          <Text style={[styles.title, styles.font]}>Part #5</Text>
          <Icon
            name={
              this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'
            }
            size={30}
            color={colors.strong_grey}
          />
        </TouchableOpacity>
        <View style={styles.parentHr} />
        {this.state.expanded && (
          <View>
            <Text style={styles.label}>Number Part</Text>
            <TextInput
              editable={false}
              value={`${number_part_lima}`}
              placeholder="Please fill = 5"
              autoCapitalize="none"
              onChangeText={(text) => onChangeText(text, 'number_part_lima')}
              // keyboardType="numeric"
              style={styles.inputStyleReadOnly}
            />
            <Text style={styles.label}>Part Name</Text>
            <TextInput
              value={part_name_lima}
              placeholder="Part Name"
              autoCapitalize="none"
              onChangeText={(text) => onChangeText(text, 'part_name_lima')}
              style={styles.inputStyle}
            />

            <Text style={styles.label}>Removed Part Number</Text>
            <TextInput
              value={removed_part_lima}
              placeholder="Removed Part Number"
              autoCapitalize="none"
              // keyboardType="numeric"
              onChangeText={(text) => onChangeText(text, 'removed_part_lima')}
              style={styles.inputStyle}
            />

            <Text style={styles.label}>Removed Serial Number</Text>
            <TextInput
              value={removed_serial_lima}
              placeholder="Removed Serial Number"
              autoCapitalize="none"
              // keyboardType="numeric"
              onChangeText={(text) => onChangeText(text, 'removed_serial_lima')}
              style={styles.inputStyle}
            />

            <Text style={styles.label}>Replacing Part Number</Text>
            <TextInput
              value={replacing_part_lima}
              placeholder="Replacing Part Number"
              autoCapitalize="none"
              // keyboardType="numeric"
              onChangeText={(text) => onChangeText(text, 'replacing_part_lima')}
              style={styles.inputStyle}
            />

            <Text style={styles.label}>Replacing Serial Number</Text>
            <TextInput
              value={replacing_serial_lima}
              placeholder="Replacing Serial Number"
              autoCapitalize="none"
              // keyboardType="numeric"
              onChangeText={(text) =>
                onChangeText(text, 'replacing_serial_lima')
              }
              style={styles.inputStyle}
            />
          </View>
        )}
      </View>
    );
  }

  toggleExpand = () => {
    const {expanded} = this.state;
    const {checkPartNumber} = this.props;
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({expanded: !expanded});

    checkPartNumber(!expanded, 'number_part_lima');
  };
}

const styles = StyleSheet.create({
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  parentHr: {
    height: 1,
    color: 'white',
    width: '100%',
  },
  child: {
    backgroundColor: colors.light_sapphire_bluish_gray,
    padding: 16,
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputStyleReadOnly: {
    width: '100%',
    backgroundColor: colors.light_bluish_gray,
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
    marginBottom: widthPercentageToDP('4%'),
  },
  inputFormDisabled: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    backgroundColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
  },
  inputFormPicker: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
  },
});
