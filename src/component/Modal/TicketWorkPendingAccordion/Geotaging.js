import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  LayoutAnimation,
  Platform,
  UIManager,
  TextInput,
  Picker,
  SafeAreaView,
  ToastAndroid,
  PermissionsAndroid,
} from 'react-native';
import colors from '../../../utils/colors';
import {widthPercentageToDP, heightPercentageToDP} from '../../../utils';
import Icon from 'react-native-vector-icons/MaterialIcons';
import DatePicker from 'react-native-datepicker';
import {WebView} from 'react-native-webview';
import html_script from '../../../utils/html_script';
import Geolocation from 'react-native-geolocation-service';
import {getDistance} from 'geolib';

export default class Geotaging extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      expanded: false,
      initialPosition: {},
      forceLocation: true,
      highAccuracy: true,
      loading: false,
      showLocationDialog: true,
      significantChanges: false,
      updatesEnabled: false,
      foregroundService: false,
    };

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  // componentWillUnmount() {
  //   this.removeLocationUpdates();
  // }

  componentDidMount() {
    PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    Geolocation.getCurrentPosition(
      (position) => {
        this.setState({initialPosition: position, loading: false});
        console.log(position);
      },
      (error) => {
        console.log(error);
      },
      {
        enableHighAccuracy: true,
        timeout: 15000,
        maximumAge: 10000,
      },
    );
  }

  componentDidUpdate(prevProps, prevState) {
    const {expanded} = this.state;
    if (expanded !== prevState.expanded) {
      PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );

      Geolocation.getCurrentPosition(
        (position) => {
          this.setState({initialPosition: position, loading: false});
          console.log(position);
        },
        (error) => {
          console.log(error);
        },
        {
          enableHighAccuracy: true,
          timeout: 15000,
          maximumAge: 10000,
        },
      );
    }
  }

  _goToMyPosition = async (lat, lon) => {
    const {dataWorkStart, captureDistance} = this.props;
    console.log('dataWorkStart', dataWorkStart);
    console.log('lat, lon', lat, lon);

    let latCheck = await lat;
    let lonCheck = await lon;

    let data_mesin_cleaning_latCheck = await dataWorkStart?.ticket?.machine
      ?.latitude;
    let data_mesin_cleaning_lonCheck = await dataWorkStart?.ticket?.machine
      ?.longitude;

    this.refs.Map_Ref.injectJavaScript(`
      mymap.setView([${latCheck}, ${lonCheck}], 25)
      L.marker([${latCheck}, ${lonCheck}]).addTo(mymap)
    `);

    let dis = getDistance(
      // lokasi user
      {latitude: latCheck, longitude: lonCheck},
      // lokasi ATM
      {
        latitude: dataWorkStart?.ticket?.machine?.latitude,
        longitude: dataWorkStart?.ticket?.machine?.longitude,
      },
    );

    // alert(`Jarak anda dengan ATM adalah\n\n${dis} Meter\nOR\n${dis / 1000} KM`);
    const messageText = `Jarak anda dengan Machine adalah ${dis} Meter\n\nLokasi Machine\n\nLatitude\n ${dataWorkStart?.ticket?.machine?.latitude}\nLongitude\n ${dataWorkStart?.ticket?.machine?.longitude}\n\nLokasi User\n\nLatitude\n ${latCheck}\nLongitude\n ${lonCheck} \n\n*jarak anda harus d bawah 50 Meter dengan Machine untuk bisa submit data`;
    alert(messageText);

    captureDistance(dis);
  };

  render() {
    const {onChangeText, dataWorkStart} = this.props;
    const {initialPosition, loading} = this.state;
    return (
      <View>
        <TouchableOpacity
          ref={this.accordian}
          style={styles.row}
          onPress={() => this.toggleExpand()}>
          <Text style={[styles.title, styles.font]}>GEO-TAGGING</Text>
          <Icon
            name={
              this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'
            }
            size={30}
            color={colors.strong_grey}
          />
        </TouchableOpacity>
        <View style={styles.parentHr} />
        {this.state.expanded && (
          <View>
            <Text style={styles.label}>PM</Text>
            <SafeAreaView
              style={{
                marginTop: widthPercentageToDP('4%'),
                width: widthPercentageToDP('70%'),
                height: widthPercentageToDP('70%'),
                padding: 6,
                backgroundColor: colors.strong_grey,
              }}>
              {/* {console.log('html_script', html_script)} */}
              <WebView ref={'Map_Ref'} source={{html: html_script}} />
            </SafeAreaView>
            <TouchableOpacity
              onPress={() =>
                this._goToMyPosition(
                  initialPosition?.coords?.latitude,
                  initialPosition?.coords?.longitude,
                )
              }
              style={styles.findLocationATMButton}>
              <Text style={styles.logoutTextFindLocationButton}>
                Update Location
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }

  toggleExpand = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({expanded: !this.state.expanded});
  };
}

const styles = StyleSheet.create({
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  parentHr: {
    height: 1,
    color: 'white',
    width: '100%',
  },
  child: {
    backgroundColor: colors.light_sapphire_bluish_gray,
    padding: 16,
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputStyleReadOnly: {
    width: '100%',
    backgroundColor: colors.light_bluish_gray,
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
    marginBottom: widthPercentageToDP('4%'),
  },
  inputFormDisabled: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    backgroundColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
  },
  inputFormPicker: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
  },
  logoutTextFindLocationButton: {
    color: '#fff',
    fontWeight: '800',
    textAlign: 'center',
    fontSize: widthPercentageToDP('5%'),
  },
  findLocationATMButton: {
    backgroundColor: colors.strong_blue,
    marginTop: widthPercentageToDP('4%'),
    borderRadius: 6,
    borderColor: colors.strong_blue,
    borderWidth: 2,
    width: '100%',
    paddingVertical: widthPercentageToDP('2%'),
    alignItems: 'center',
  },
});
