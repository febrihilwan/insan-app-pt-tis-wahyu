import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  LayoutAnimation,
  Platform,
  UIManager,
  TextInput,
  Picker,
} from 'react-native';
import colors from '../../../utils/colors';
import {widthPercentageToDP, heightPercentageToDP} from '../../../utils';
import Icon from 'react-native-vector-icons/MaterialIcons';
import DatePicker from 'react-native-datepicker';

export default class ProblemInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      expanded: false,
    };

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  render() {
    const {onChangeText, dataWorkStart, kelanjutan_problem} = this.props;
    // console.log('dataWorkStart', dataWorkStart);
    return (
      <View>
        <TouchableOpacity
          ref={this.accordian}
          style={styles.row}
          onPress={() => this.toggleExpand()}>
          <Text style={[styles.title, styles.font]}>PROBLEM INFO</Text>
          <Icon
            name={
              this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'
            }
            size={30}
            color={colors.strong_grey}
          />
        </TouchableOpacity>
        <View style={styles.parentHr} />
        {this.state.expanded && (
          <View>
            <Text style={styles.label}>Problem Number</Text>
            <TextInput
              editable={false}
              placeholder="-"
              autoCapitalize="none"
              value={`${dataWorkStart.id}`}
              onChangeText={(text) => onChangeText(text, 'ticket_id')}
              style={styles.inputStyleReadOnly}
            />

            <Text style={styles.label}>Problem Status</Text>
            <TextInput
              editable={false}
              placeholder="please input your username"
              autoCapitalize="none"
              value={dataWorkStart.problem_status}
              onChangeText={(text) => onChangeText(text, 'status_ticket')}
              style={styles.inputStyleReadOnly}
            />

            <Text style={styles.label}>Status Kelanjutan Problem</Text>
            <View style={styles.inputFormPicker}>
              <Picker
                selectedValue={kelanjutan_problem}
                mode="dropdown"
                onValueChange={(text) =>
                  onChangeText(text, 'kelanjutan_problem')
                }>
                <Picker.Item label="Pilih Kelanjutan Problem" value="" />
                <Picker.Item label="OUTSTANDING" value="OUTSTANDING" />
                <Picker.Item label="DONE" value="DONE" />
              </Picker>
            </View>
          </View>
        )}
      </View>
    );
  }

  toggleExpand = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({expanded: !this.state.expanded});
  };
}

const styles = StyleSheet.create({
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  parentHr: {
    height: 1,
    color: 'white',
    width: '100%',
  },
  child: {
    backgroundColor: colors.light_sapphire_bluish_gray,
    padding: 16,
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputStyleReadOnly: {
    width: '100%',
    backgroundColor: colors.light_bluish_gray,
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
    marginBottom: widthPercentageToDP('4%'),
  },
  inputFormDisabled: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    backgroundColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
  },
  inputFormPickerReadOnly: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    backgroundColor: colors.light_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
  },
  inputFormPicker: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
  },
});
