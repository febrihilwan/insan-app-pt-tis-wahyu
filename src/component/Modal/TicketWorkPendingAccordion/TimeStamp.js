import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  LayoutAnimation,
  Platform,
  UIManager,
  TextInput,
} from 'react-native';
import colors from '../../../utils/colors';
import {widthPercentageToDP, heightPercentageToDP} from '../../../utils';
import Icon from 'react-native-vector-icons/MaterialIcons';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import DatePicker from 'react-native-datepicker';

export default class TimeStamp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      expanded: false,
	  isWorkFinish: false,
	  work_finish: new Date(),
    };

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  render() {
    const {onChangeText, dataWorkStart} = this.props;
	const {isWorkFinish} = this.state;
    return (
      <View>
        <TouchableOpacity
          ref={this.accordian}
          style={styles.row}
          onPress={() => this.toggleExpand()}>
          <Text style={[styles.title, styles.font]}>TIMESTAMP</Text>
          <Icon
            name={
              this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'
            }
            size={30}
            color={colors.strong_grey}
          />
        </TouchableOpacity>
        <View style={styles.parentHr} />
        {this.state.expanded && (
          <View>
            <Text style={styles.label}>Ticket Open (Created)</Text>
            <View style={styles.inputForm}>
              <DatePicker
                date={dataWorkStart.ticket.ticket_open}
                disabled
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Pilih tanggal"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'open_date');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>

            <Text style={styles.label}>Ticket Closed</Text>
            <View style={styles.inputForm}>
              <DatePicker
                date={dataWorkStart.ticket.ticket_close}
                disabled
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Empty date"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'close_date');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>

            <Text style={styles.label}>Ticket Duration</Text>
            <TextInput
              editable={false}
              value={
                dataWorkStart.ticket.ticket_duration
                  ? dataWorkStart.ticket.ticket_duration
                  : 'Empty ticket duration'
              }
              placeholder="please input your name"
              autoCapitalize="none"
              onChangeText={(text) => onChangeText(text, 'name')}
              style={styles.inputStyleReadOnly}
            />

            <Text style={styles.label}>Problem Open (Created)</Text>
            <View style={styles.inputForm}>
              <DatePicker
                date={dataWorkStart.problem_open}
                disabled
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Empty date"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'close_date');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>

            <Text style={styles.label}>Problem Confirmed</Text>
            <View style={styles.inputForm}>
              <DatePicker
                date={dataWorkStart.problem_confirmed}
                disabled
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Empty date"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'close_date');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>

            <Text style={styles.label}>Problem Closed</Text>
            <View style={styles.inputForm}>
              <DatePicker
                date={dataWorkStart.problem_close}
                disabled
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Pilih tanggal"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'close_date');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>

            <Text style={styles.label}>Problem Duration</Text>
            <View style={styles.inputForm}>
              <DatePicker
                date={dataWorkStart.problem_duration}
                disabled
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Pilih tanggal"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'close_date');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>
            <Text style={styles.label}>Departure</Text>
            <View style={styles.inputForm}>
              <DatePicker
                disabled={true}
                date={dataWorkStart.departured}
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Pilih tanggal"
                // format="dddd, DD-MM-YYYY HH:mm:ss"
                format="YYYY-MM-DD hh:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'departured');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>
            <Text style={styles.label}>Arrival</Text>
            <View style={styles.inputForm}>
              <DatePicker
                disabled={true}
                date={dataWorkStart.arrival}
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Pilih tanggal"
                // format="dddd, DD-MM-YYYY HH:mm:ss"
                format="YYYY-MM-DD hh:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'arrival');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>
            <Text style={styles.label}>Work Start</Text>
            <View style={styles.inputForm}>
              <DatePicker
                date={dataWorkStart.work_start}
                disabled
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Pilih tanggal"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'work_start');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>
            <Text style={styles.label}>Work Finish</Text>
            <View style={styles.inputForm}>
              <DatePicker
                date={this.state.work_finish}
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Empty date"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'work_finish');
				  this.setState({ work_finish: date })
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
				<TouchableOpacity
				  style={[styles.uploadPhotoButton, styles.uploadPhotoButtonActive]}
				  onPress={() => this.setState({isWorkFinish: true})}>
				  <Icon
					name="perm-contact-calendar"
					size={widthPercentageToDP('6%')}
					color="white"
				  />
				  <Text style={styles.buttonText}>Choose Date and Time</Text>
				</TouchableOpacity>
				<DateTimePickerModal
				  isVisible={isWorkFinish}
				  mode="datetime"
				  onConfirm={(date) => this.setState({work_finish: date})}
				  onCancel={() => this.setState({isWorkFinish: false})}
				/>
            </View>
          </View>
        )}
      </View>
    );
  }

  toggleExpand = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({expanded: !this.state.expanded});
  };
}

const styles = StyleSheet.create({
  uploadPhotoButton: {
    width: widthPercentageToDP('60%'),
    borderRadius: 5,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%'),
	marginTop: 10,
    marginBottom: widthPercentageToDP('3%'),
    justifyContent: 'center',
    flexDirection: 'row',
  },
  uploadPhotoButtonActive: {
    backgroundColor: colors.strong_blue,
  },
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  parentHr: {
    height: 1,
    color: 'white',
    width: '100%',
  },
  child: {
    backgroundColor: colors.light_sapphire_bluish_gray,
    padding: 16,
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputStyleReadOnly: {
    width: '100%',
    backgroundColor: colors.light_bluish_gray,
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputForm: {
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
    marginBottom: widthPercentageToDP('4%'),
  },
  inputFormDisabled: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    backgroundColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
  },
});
