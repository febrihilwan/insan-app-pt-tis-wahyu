import React, {Component} from 'react';
import Modal from 'react-native-modal';
import _ from 'lodash';
import moment from 'moment';
import axios from 'axios';
import {
  View,
  Text,
  Button,
  TouchableOpacity,
  TextInput,
  Image,
  ScrollView,
  Dimensions,
  StyleSheet,
  AsyncStorage,
  Picker,
  LayoutAnimation,
  UIManager,
  ToastAndroid,
} from 'react-native';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import {widthPercentageToDP, heightPercentageToDP} from '../../utils';
import colors from '../../utils/colors';
import Spinner from 'react-native-loading-spinner-overlay';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import {API_URL} from 'react-native-dotenv';
import DatePicker from 'react-native-datepicker';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

class UploadModalTicketConfirm extends Component {
  constructor(props) {
    super(props);
    this.onChangeText = this.onChangeText.bind(this);
    this.state = {
      name: '',
      email: '',
      no_telp: '',
      updated_at: '',
      username: '',
      api_token: '',
      img_user: '',
      img_user: '',
      password: '',
      dateConfirm: null,
	  dateTime: new Date(),
      spinner: false,
      expanded: false,
	  isDatePickerVisible: false,
    };
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  async componentDidMount() {
    // [TEST] SPINNER
    // setInterval(() => {
    //   this.setState({
    //     spinner: !this.state.spinner,
    //   });
    // }, 3000);
    try {
      let userData = await AsyncStorage.getItem('userData');
      if (userData) {
        let data = JSON.parse(userData);
        // console.log('userData', data);
        this.setState({
          api_token: data.api_token.api_token,
        });
      }
    } catch (err) {
      // console.log(err);
      // console.log('Failed local login');
    }
  }

  onChangeText(text, name) {
    this.setState(
      {
        [name]: text,
      },
    );
  }

  _takePicture(field) {
    const options = {
      title: `Upload foto ${field} anda`,
      storageOptions: {
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response upload photo = ', response);
      if (response.didCancel) {
        console.log('Pick image cancelled');
      } else if (response.error) {
        console.log('Error pick image', response.error);
      } else {
        // if(this.state[field]) {
        //   let temp = {
        //     uri: response.uri,
        //     type: response.type,
        //     name: response.fileName,
        //   }

        //   let tempField = this.state[field]
        //   tempField.push(temp)

        //   this.setState({
        //     [field]: tempField
        //   });

        // } else {
        this.setState({
          [field]: {
            uri: response.uri,
            type: 'image/jpeg',
            name: response.fileName,
            path: response.path,
          },
        });
        // }
      }
    });
  }

  handleOnScroll = (event) => {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y,
    });
  };

  handleScrollTo = (p) => {
    if (this.scrollViewRef) {
      this.scrollViewRef.scrollTo(p);
    }
  };

  checkingImage = () => {
    let {img_user} = this.state;
    let {document, AppraisalData} = this.props;
    let data = _.find(AppraisalData.data, {objectField: {name: img_user.name}});
    // console.log("--",document)
    if (img_user) {
      return (
        <Image source={{uri: img_user.uri}} style={styles.modalImagePicker} />
      );
    } else {
      return null;
    }
  };

  onPressUpload = async () => {
    const {dataConfirm, togglePhotoModal} = this.props;
    const {dateTime, api_token} = this.state;
    this.setState({
      spinner: true,
    });

    let headers = {
      Authorization: `Bearer ${api_token}`,
    };

    axios(`${API_URL}/confirmed/${dataConfirm.id}`, {
      method: 'POST',
      headers,
      data: {
		  problem_confirmed: moment(dateTime).format('YYYY-MM-DD HH:mm:ss'),
	  },
    })
      .then(async (responseData) => {
        this.setState({
          spinner: false,
        });
        togglePhotoModal();
        alert('Sukses Confirm');
      })
      .catch((err) => {
        ToastAndroid.showWithGravity(
          'Gagal post data confirmed',
          2000,
          ToastAndroid.BOTTOM,
        );
      });
  };
  
  handleConfirm = (date) => {
	this.setState({
		isDatePickerVisible: false,
		dateTime: date,
	});
  };

  render() {
    const {
      displayPhotoModal,
      togglePhotoModal,
      document,
      dataConfirm,
      previewPhoto,
      AppraisalData,
    } = this.props;

    const {
      name,
      email,
      no_telp,
      updated_at,
      img_user,
      username,
      spinner,
      dateConfirm,
	  dateTime,
	  isDatePickerVisible,
    } = this.state;

    let images = document.imageUrl;
    let data = _.find(AppraisalData.data, {objectField: {name: document.name}});
    return (
      <Modal
        supportedOrientations={['portrait', 'landscape']}
        onBackdropPress={() => togglePhotoModal()}
        isVisible={displayPhotoModal}
        style={styles.photoModal}
        onModalShow={() => console.log('ini', document)}>
        <Spinner
          visible={spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <View
          style={
            Dimensions.get('window').height <= 360 ? styles.scrollableModal : {}
          }>
          <View style={styles.modalContainer}>
            <View style={styles.modalHeader}>
              <TouchableOpacity onPress={() => togglePhotoModal()}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: widthPercentageToDP('8%'),
                    fontFamily: 'Roboto',
                  }}>
                  x
                </Text>
              </TouchableOpacity>
            </View>
            <ScrollView
              ref={(ref) => (this.scrollViewRef = ref)}
              onScroll={this.handleOnScroll}
              scrollEventThrottle={16}>
              <View style={styles.modalContent}>
                <Text style={styles.modalPhotoName}>Confirm Problem</Text>
                <Text style={styles.label}>
                  Anda yakin ingin confirm problem ini?
                </Text>
                <Text style={styles.label}>Problem Confirmed</Text>
                <View style={styles.inputForm}>
                  <DatePicker
					disabled
					date={dateTime}
                    style={{width: '100%'}}
                    locale="id"
                    mode="date"
                    androidMode="calendar"
                    placeholder="Pilih tanggal"
                    format="dddd, DD-MM-YYYY HH:mm:ss"
                    confirmBtnText="Oke"
                    cancelBtnText="Batal"
                    minuteInterval={15}
					onDateChange={(date) => this.setState({dataConfirm: new Date(date)})}
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0,
                      },
                      dateInput: {
                        borderWidth: 0,
                        backgroundColor: colors.light_bluish_gray,
                      },
                      placeholderText: {
                        color: '#CAD9E4',
                      },
                      dateText: {
                        color: '#172344',
                        fontSize: 14,
                      },
                    }}
                  />
                </View>
				<TouchableOpacity
				  style={[styles.confirmDateButton, styles.confirmDateButtonActive]}
				  onPress={() => this.setState({isDatePickerVisible: true})}>
				  <IconMaterial
					name="perm-contact-calendar"
					size={widthPercentageToDP('6%')}
					color="white"
				  />
				  <Text style={styles.buttonText}>Choose Date and Time</Text>
				</TouchableOpacity>
				<DateTimePickerModal
				  isVisible={isDatePickerVisible}
				  mode="datetime"
				  onConfirm={this.handleConfirm}
				  onCancel={() => this.setState({isDatePickerVisible: false})}
				/>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    marginTop: 20,
                    marginBottom: 30,
                  }}>
                  <TouchableOpacity
                    style={[
                      styles.viewPhotoButton,
                      styles.viewPhotoButtonActive,
                    ]}
                    onPress={() => togglePhotoModal()}>
                    <Text style={styles.buttonText}>Batalkan</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={[
                      styles.uploadPhotoButton,
                      styles.uploadPhotoButtonActive,
                    ]}
                    onPress={() => this.onPressUpload()}>
                    <Text style={styles.buttonText}>Confirm</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  containerModal: {
    flex: 1,
  },
  modalContainer: {
    height: heightPercentageToDP('90%'),
  },
  modalHeader: {
    height: heightPercentageToDP('7%'),
    backgroundColor: 'transparent',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    padding: 5,
  },
  modalContent: {
    paddingHorizontal: widthPercentageToDP('5%'),
    paddingVertical: heightPercentageToDP('1.5%'),
    backgroundColor: '#fff',
    borderRadius: 6,
  },
  modalPhotoName: {
    fontSize: widthPercentageToDP('6%'),
    fontFamily: 'Roboto',
    color: colors.dope_blue,
  },
  modalImagePicker: {
    height: widthPercentageToDP('25.3%'),
    width: widthPercentageToDP('25.3%'),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#778899',
    marginRight: widthPercentageToDP('2%'),
    borderRadius: 6,
  },
  modalNotesInput: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    textAlignVertical: 'top',
    fontSize: widthPercentageToDP('4.5%'),
    borderRadius: 5,
  },
  scrollableModal: {
    height: 250,
  },
  photoModal: {
    paddingBottom: 20,
  },
  scrollableModalContent1: {
    height: 200,
    backgroundColor: 'orange',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollableModalContent2: {
    height: 200,
    backgroundColor: 'lightgreen',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Roboto',
    fontSize: widthPercentageToDP('4%'),
    fontWeight: 'bold',
  },
  viewPhotoButton: {
    width: widthPercentageToDP('37.5%'),
    borderRadius: 5,
    backgroundColor: colors.bright_red,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%'),
  },
  viewPhotoButtonActive: {
    backgroundColor: colors.strong_grey,
  },
  viewPhotoButtonInactive: {
    backgroundColor: colors.very_soft_red,
  },
  confirmDateButton: {
    width: widthPercentageToDP('60%'),
    borderRadius: 5,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%'),
    marginBottom: widthPercentageToDP('3%'),
    justifyContent: 'center',
    flexDirection: 'row',
  },
  confirmDateButtonActive: {
    backgroundColor: colors.strong_blue,
  },
  uploadPhotoButton: {
    width: widthPercentageToDP('37.5%'),
    borderRadius: 5,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%'),
  },
  uploadPhotoButtonActive: {
    backgroundColor: colors.orange_insan,
  },
  uploadPhotoButtonInactive: {
    backgroundColor: colors.very_soft_blue,
  },
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
    marginTop: heightPercentageToDP('1%'),
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
  },
  containerAccordian: {
    flex: 1,
    paddingTop: 0,
    backgroundColor: 'white',
    borderColor: colors.light_sapphire_bluish_gray,
    borderWidth: 2,
    borderRadius: 10,
    paddingHorizontal: 16,
    marginTop: 16,
  },
  titleAccordian: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  headerAccordian: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
});

const mapStateToProps = ({AppraisalData}) => ({AppraisalData});

export default connect(mapStateToProps)(UploadModalTicketConfirm);
