import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  LayoutAnimation,
  Platform,
  UIManager,
  TextInput,
  Picker,
} from 'react-native';
import colors from '../../../utils/colors';
import {widthPercentageToDP, heightPercentageToDP} from '../../../utils';
import Icon from 'react-native-vector-icons/MaterialIcons';
import DatePicker from 'react-native-datepicker';

export default class ToCheck extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      expanded: false,
    };

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  render() {
    const {
      onChangeTextCheckandToDo,
      dataTicketDetail,
      cleaning_tambah,
      status,
      status_lainnya,
    } = this.props;
    console.log('status', status);
    console.log('status_lainnya', status_lainnya);

    return (
      <View>
        <TouchableOpacity
          ref={this.accordian}
          style={styles.row}
          onPress={() => this.toggleExpand()}>
          <Text style={[styles.title, styles.font]}>CHECKLIST: TO-CHECK</Text>
          <Icon
            name={
              this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'
            }
            size={30}
            color={colors.strong_grey}
          />
        </TouchableOpacity>
        <View style={styles.parentHr} />
        {this.state.expanded && (
          <View>
            {cleaning_tambah.map((e, i) => (
              <View key={i}>
                <Text style={styles.label}>{e.name_checklist}</Text>
                <View style={styles.inputFormPicker}>
                  <Picker
                    selectedValue={status.val[i]}
                    mode="dropdown"
                    onValueChange={(text) => {
                      onChangeTextCheckandToDo(text, e.id, i);
                    }}>
                    <Picker.Item
                      label={`Choose ${
                        status_lainnya.val[i] === 'Lainnya'
                          ? 'Lainnya'
                          : status_lainnya.val[i] === 'Bermasalah'
                          ? 'Bermasalah'
                          : status_lainnya.val[i] === 'Tidak ada'
                          ? 'Tidak ada'
                          : e.name_checklist
                      }`}
                      value=""
                    />
                    <Picker.Item label="Layak" value="Layak" />
                    <Picker.Item label="Bermasalah" value="Bermasalah" />
                    <Picker.Item label="Tidak ada" value="Tidak ada" />
                    <Picker.Item label="Lainnya" value="Lainnya" />
                  </Picker>
                </View>

                {status_lainnya.val[i] === 'Lainnya' ? (
                  <View>
                    <Text style={styles.label}>Check Layar Lainnya</Text>
                    <TextInput
                      placeholder="Lainnya"
                      autoCapitalize="none"
                      onChangeText={(text) =>
                        onChangeTextCheckandToDo(text, e.id, i)
                      }
                      style={styles.inputStyle}
                    />
                  </View>
                ) : null}
              </View>
            ))}
          </View>
        )}
      </View>
    );
  }

  toggleExpand = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({expanded: !this.state.expanded});
  };
}

const styles = StyleSheet.create({
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  parentHr: {
    height: 1,
    color: 'white',
    width: '100%',
  },
  child: {
    backgroundColor: colors.light_sapphire_bluish_gray,
    padding: 16,
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputStyleReadOnly: {
    width: '100%',
    backgroundColor: colors.light_bluish_gray,
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
    marginBottom: widthPercentageToDP('4%'),
  },
  inputFormDisabled: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    backgroundColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
  },
  inputFormPicker: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
  },
  inputFormPickerReadOnly: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    backgroundColor: colors.light_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
  },
});
