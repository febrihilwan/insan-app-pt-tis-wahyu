import React, {Component} from 'react';
import Modal from 'react-native-modal';
import _ from 'lodash';
import {
  View,
  Text,
  Button,
  TouchableOpacity,
  TextInput,
  Image,
  ScrollView,
  Dimensions,
  StyleSheet,
  AsyncStorage,
  Picker,
  LayoutAnimation,
  UIManager,
  ToastAndroid,
} from 'react-native';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import {widthPercentageToDP, heightPercentageToDP} from '../../utils';
import colors from '../../utils/colors';
import Spinner from 'react-native-loading-spinner-overlay';
import CleaningInfo from './CleaningRegisterAccordion/CleaningInfo';
import CleanedMechine from './CleaningRegisterAccordion/CleanedMechine';
import Geotaging from './CleaningRegisterAccordion/Geotaging';
import TimeStamp from './CleaningRegisterAccordion/TimeStamp';
import ToCheck from './CleaningRegisterAccordion/ToCheck';
import ToDo from './CleaningRegisterAccordion/ToDo';
import {API_URL} from 'react-native-dotenv';
import axios from '../../services/axios';

class UploadModalCleaningRegister extends Component {
  constructor(props) {
    super(props);
    this.onChangeText = this.onChangeText.bind(this);
    this.onChangeTextCheckandToDo = this.onChangeTextCheckandToDo.bind(this);
    this.fetchBankNames = this.fetchBankNames.bind(this);
    this.fetchBankBranchs = this.fetchBankBranchs.bind(this);
    this.captureDistance = this.captureDistance.bind(this);
    this.state = {
      name: '',
      email: '',
      no_telp: '',
      updated_at: '',
      username: '',
      api_token: '',
      img_user: '',
      image_clean: '',
      password: '',
      cleaning_start: '',
      cleaning_finish: '',
      bank_id: '',
      bank_branch_id: '',
      mechine_type: '',
      bank_machine_type_id: '',
      bank_machine_sn_name: '',
      spinner: false,
      expanded: false,
      cleaning_tambah: [],
      bank_names: [],
      bank_branchs: [],
      bank_machine_types: [],
      bank_machine_sn: [],
      data_mechine_id: '',
      bank_machine_sn_details_address: '',
      status: {val: []},
      status_lainnya: {val: []},
      checklist_id: {val: []},
      data_mesin_cleaning: '',
      statusLocationUser: true,
      lengthToCheck: 0,
    };
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  captureDistance(dis) {
    console.log('dis', dis, 'Location');
    const {statusLocationUser} = this.state;
    if (dis <= 50) {
      this.setState({
        statusLocationUser: false,
      });
    }
  }

  async componentDidMount() {
    // [TEST] SPINNER
    // setInterval(() => {
    //   this.setState({
    //     spinner: !this.state.spinner,
    //   });
    // }, 3000);
    try {
      let userData = await AsyncStorage.getItem('userData');
      if (userData) {
        let data = JSON.parse(userData);
        // console.log('userData', data);
        this.setState({
          api_token: data.api_token.api_token,
          name: data.api_token.name,
        });
      }
    } catch (err) {
      // console.log(err);
      // console.log('Failed local login');
    }
    this.fetchBankNames();
    this.fetchCleaningTambah();
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      bank_id,
      bank_branch_id,
      bank_machine_type_id,
      bank_machine_sn_name,
      bank_machine_types,
      bank_machine_sn_details_address,
      data_mechine_id,
      status_lainnya,
    } = this.state;
    if (bank_id !== prevState.bank_id) {
      this.fetchBankBranchs();
      this.setState({
        bank_machine_types: [],
        bank_machine_sn_name: '',
        bank_machine_sn_details_address: '',
        data_mechine_id: '',
        bank_machine_sn: [],
      });
    }
    if (bank_branch_id !== prevState.bank_branch_id) {
      this.fetchBankMachineType();
      this.setState({
        bank_machine_sn_name: '',
        bank_machine_sn_details_address: '',
        data_mechine_id: '',
        bank_machine_sn: [],
      });
    }
    if (bank_machine_type_id !== prevState.bank_machine_type_id) {
      this.fetchBankMachineSn();
    }
    if (bank_machine_sn_name !== prevState.bank_machine_sn_name) {
      this.fetchBankMachineSnDetail();
    }
    if (bank_machine_sn_name !== prevState.bank_machine_sn_name) {
      this.fetchBankMachineSnDetail();
    }
  }

  // componentWillUnmount() {
  //   // this.navigationWillFocusListener.remove();
  //   setApprasialData([]);
  // }

  onChangeText(text, name) {
    this.setState({
      [name]: text,
    });
  }

  onChangeTextCheckandToDo(text, id, index) {
    const {status, checklist_id, status_lainnya} = this.state;

    if (text === 'Lainnya') {
      let vals_lainnya = [...status_lainnya.val];
      vals_lainnya[index] = text;

      this.setState({
        status_lainnya: {val: vals_lainnya},
      });
    }

    if (text === 'Layak') {
      let vals_lainnya = [...status_lainnya.val];
      vals_lainnya[index] = text;

      this.setState({
        status_lainnya: {val: vals_lainnya},
      });
    }

    if (text === 'Bermasalah') {
      let vals_lainnya = [...status_lainnya.val];
      vals_lainnya[index] = text;

      this.setState({
        status_lainnya: {val: vals_lainnya},
      });
    }

    if (text === 'Tidak ada') {
      let vals_lainnya = [...status_lainnya.val];
      vals_lainnya[index] = text;

      this.setState({
        status_lainnya: {val: vals_lainnya},
      });
    }

    if (text === 'Sudah') {
      let vals_lainnya = [...status_lainnya.val];
      vals_lainnya[index] = text;

      this.setState({
        status_lainnya: {val: vals_lainnya},
      });
    }

    if (text === 'Belum') {
      let vals_lainnya = [...status_lainnya.val];
      vals_lainnya[index] = text;

      this.setState({
        status_lainnya: {val: vals_lainnya},
      });
    }

    // if (
    //   text === 'Bermasalah' ||
    //   text === 'Layak' ||
    //   text === 'Tidak ada' ||
    //   text === 'Sudah' ||
    //   text === 'Belum'
    // ) {
    //   this.setState({
    //     status_lainnya: {val: []},
    //   });
    // }

    let vals = [...status.val];
    vals[index] = text;

    let valsId = [...checklist_id.val];
    valsId[index] = id;

    this.setState({
      status: {val: vals},
      checklist_id: {val: valsId},
    });
  }

  fetchCleaningTambah() {
    this.setState({
      spinner: true,
    });
    const {api_token} = this.state;

    axios
      .get('/cleaningtambah', {
        headers: {
          Authorization: `Bearer ${api_token}`,
        },
      })
      .then((result) => {
        this.setState({
          cleaning_tambah: result.data[0],
          cleaning_tambah_to_do: result.data[1],
          lengthToCheck: result.data[0].length,
          spinner: false,
        });
      })
      .catch((err) => {
        // console.log('ini error fetchBankNames', err.response);
        ToastAndroid.showWithGravity(
          'Gagal menampilkan cleaning tambah',
          2000,
          ToastAndroid.BOTTOM,
        );
        this.setState({
          spinner: false,
        });
      });
  }

  fetchBankNames() {
    this.setState({
      spinner: true,
    });
    const {api_token, bank_names} = this.state;

    axios
      .get('/bank', {
        headers: {
          Authorization: `Bearer ${api_token}`,
        },
      })
      .then((result) => {
        this.setState({
          bank_names: result.data,
          spinner: false,
        });
      })
      .catch((err) => {
        // console.log('ini error fetchBankNames', err.response);
        ToastAndroid.showWithGravity(
          'Gagal menampilkan bank names',
          2000,
          ToastAndroid.BOTTOM,
        );
        this.setState({
          spinner: false,
        });
      });
  }

  fetchBankBranchs() {
    this.setState({
      spinner: true,
    });
    const {api_token, bank_id} = this.state;

    axios
      .get(`/branch/${bank_id}`, {
        headers: {
          Authorization: `Bearer ${api_token}`,
        },
      })
      .then((result) => {
        if (result.data.map((e) => e.length) == 0) {
          alert('Data cabang bank kosong');
        }
        this.setState({
          bank_branchs: result.data,
          spinner: false,
        });
      })
      .catch((err) => {
        // console.log('ini error fetchBankBranchs', err.response);
        ToastAndroid.showWithGravity(
          'Gagal menampilkan bank branchs',
          2000,
          ToastAndroid.BOTTOM,
        );
        this.setState({
          spinner: false,
        });
      });
  }

  fetchBankMachineType() {
    this.setState({
      spinner: true,
    });
    const {api_token, bank_branch_id} = this.state;

    axios
      .get(
        `/type_dropdown/${bank_branch_id.bank_id}/branch/${bank_branch_id.branch_id}/`,
        {
          headers: {
            Authorization: `Bearer ${api_token}`,
          },
        },
      )
      .then((result) => {
        this.setState({
          bank_machine_types: result.data,
          spinner: false,
        });
      })
      .catch((err) => {
        // console.log('ini error fetchBankMachineType', err.response);
        ToastAndroid.showWithGravity(
          'Gagal menampilkan bank branchs',
          2000,
          ToastAndroid.BOTTOM,
        );
        this.setState({
          spinner: false,
        });
      });
  }

  fetchBankMachineSn() {
    this.setState({
      spinner: true,
    });
    const {api_token, bank_machine_type_id} = this.state;

    axios
      .get(
        `/machinesn/${bank_machine_type_id.bank.id}/branch/${bank_machine_type_id.branch.id}/type/${bank_machine_type_id.type.id}/`,
        {
          headers: {
            Authorization: `Bearer ${api_token}`,
          },
        },
      )
      .then((result) => {
        this.setState({
          bank_machine_sn: result.data,
          spinner: false,
        });
      })
      .catch((err) => {
        // console.log('ini error fetchBankMachineSn', err.response);
        ToastAndroid.showWithGravity(
          'Gagal menampilkan bank branchs',
          2000,
          ToastAndroid.BOTTOM,
        );
        this.setState({
          spinner: false,
        });
      });
  }

  fetchBankMachineSnDetail() {
    this.setState({
      spinner: true,
    });
    const {api_token, bank_machine_sn_name} = this.state;

    axios
      .get(
        `/machinesn/${bank_machine_sn_name.bank_id}/branch/${bank_machine_sn_name.branch_id}/type/${bank_machine_sn_name.type_id}/machinesn/${bank_machine_sn_name.machine_sn}`,
        {
          headers: {
            Authorization: `Bearer ${api_token}`,
          },
        },
      )
      .then((result) => {
        const {data_mechine_id} = this.state;
        this.setState({
          data_mechine_id: `${result.data.id}`,
          bank_machine_sn_details_address: result.data.name_address,
          data_mesin_cleaning: result.data,
          spinner: false,
        });
      })
      .catch((err) => {
        // console.log('ini error fetchBankMachineSnDetail', err.response);
        ToastAndroid.showWithGravity(
          'Gagal menampilkan bank branchs',
          2000,
          ToastAndroid.BOTTOM,
        );
        this.setState({
          spinner: false,
        });
      });
  }

  _takePicture(field) {
    const options = {
      title: `Upload foto ${field} anda`,
      storageOptions: {
        path: 'images',
      },
      compressImageQuality: 0.1,
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
        console.error('Error pick image', response.error);
      } else {
        // if(this.state[field]) {
        //   let temp = {
        //     uri: response.uri,
        //     type: response.type,
        //     name: response.fileName,
        //   }

        //   let tempField = this.state[field]
        //   tempField.push(temp)

        //   this.setState({
        //     [field]: tempField
        //   });

        // } else {
        this.setState({
          [field]: {
            uri: response.uri,
            type: 'image/jpeg',
            name: response.fileName,
            path: response.path,
          },
        });
        // }
      }
    });
  }

  handleOnScroll = (event) => {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y,
    });
  };

  handleScrollTo = (p) => {
    if (this.scrollViewRef) {
      this.scrollViewRef.scrollTo(p);
    }
  };

  checkingImage = () => {
    let {image_clean} = this.state;
    let {document, AppraisalData} = this.props;
    let data = _.find(AppraisalData.data, {
      objectField: {name: image_clean.name},
    });
    // console.log("--",document)
    if (image_clean) {
      return (
        <Image
          source={{uri: image_clean.uri}}
          style={styles.modalImagePicker}
        />
      );
    } else {
      return null;
    }
  };

  onPressUpload = async () => {
    const {
      api_token,
      data_mechine_id,
      cleaning_start,
      cleaning_finish,
      status,
      checklist_id,
      image_clean,
    } = this.state;

    const {togglePhotoModal} = this.props;

    let poData = [
      // {name: 'cleaning_start', value: cleaning_start},
      // {name: 'cleaning_finish', value: cleaning_finish},
    ];

    let emptyField = poData.find((field) => field.value === '');

    if (!emptyField) {
      this.setState({
        spinner: true,
      });

      let headers = {
        Authorization: `Bearer ${api_token}`,
        'Access-Control-Allow-Origin': '*',
        Accept: 'multipart/form-data',
        'content-type': 'multipart/form-data',
      };

      let formData = new FormData();
      formData.append('machine_id', data_mechine_id);
      formData.append('cleaning_start', cleaning_start);
      formData.append('cleaning_finish', cleaning_finish);
      formData.append('checklist_id', `${checklist_id.val}`);
      formData.append('status', `${status.val}`);
      formData.append('image_clean', image_clean);

      fetch(`${API_URL}/addchecklist`, {
        method: 'POST',
        headers,
        body: formData,
      })
        .then((response) => console.log(response.text()))
        .then(async (responseData) => {
          console.log(responseData);
          this.setState({
            spinner: false,
          });
          togglePhotoModal();
          alert('Add Cleaning Berhasil');
        })
        .catch((error) => {
          console.log('error add clenaing', error);
          this.setState({
            spinner: false,
          });
          alert('Add Cleaning Gagal');
        });
    } else {
      alert(`field ${emptyField.name} wajib di isi`);
    }
  };

  render() {
    const {
      displayPhotoModal,
      togglePhotoModal,
      document,
      previewPhoto,
      AppraisalData,
      dataTicketDetail,
    } = this.props;

    const {
      name,
      email,
      no_telp,
      updated_at,
      image_clean,
      username,
      spinner,
      cleaning_start,
      cleaning_finish,
      bank_names,
      bank_id,
      bank_branchs,
      bank_branch_id,
      bank_machine_types,
      bank_machine_type_id,
      bank_machine_sn,
      bank_machine_sn_name,
      data_mechine_id,
      bank_machine_sn_details_address,
      data_mesin_cleaning,
      statusLocationUser,
      cleaning_tambah,
      status,
      checklist_id,
      cleaning_tambah_to_do,
      lengthToCheck,
      status_lainnya,
    } = this.state;

    let images = document.imageUrl;
    let data = _.find(AppraisalData.data, {objectField: {name: document.name}});

    // console.log("aa", data)
    return (
      <Modal
        supportedOrientations={['portrait', 'landscape']}
        onBackdropPress={() => togglePhotoModal()}
        isVisible={displayPhotoModal}
        style={styles.photoModal}>
        <Spinner
          visible={spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <View
          style={
            Dimensions.get('window').height <= 360 ? styles.scrollableModal : {}
          }>
          <View style={styles.modalContainer}>
            <View style={styles.modalHeader}>
              <TouchableOpacity onPress={() => togglePhotoModal()}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: widthPercentageToDP('8%'),
                    fontFamily: 'Roboto',
                  }}>
                  x
                </Text>
              </TouchableOpacity>
            </View>
            <ScrollView
              ref={(ref) => (this.scrollViewRef = ref)}
              onScroll={this.handleOnScroll}
              scrollEventThrottle={16}>
              <View style={styles.modalContent}>
                <Text style={styles.modalPhotoName}>Cleaning Add</Text>
                <View style={styles.containerAccordian}>
                  <CleaningInfo name={name} />
                </View>
                <View style={styles.containerAccordian}>
                  <CleanedMechine
                    bank_names={bank_names}
                    bank_branchs={bank_branchs}
                    bank_id={bank_id}
                    bank_branch_id={bank_branch_id}
                    bank_machine_types={bank_machine_types}
                    bank_machine_type_id={bank_machine_type_id}
                    bank_machine_sn={bank_machine_sn}
                    bank_machine_sn_name={bank_machine_sn_name}
                    data_mechine_id={data_mechine_id}
                    bank_machine_sn_details_address={
                      bank_machine_sn_details_address
                    }
                    dataTicketDetail={dataTicketDetail}
                    onChangeText={this.onChangeText}
                  />
                </View>
                <View style={styles.containerAccordian}>
                  <TimeStamp
                    cleaning_start={cleaning_start}
                    cleaning_finish={cleaning_finish}
                    dataTicketDetail={dataTicketDetail}
                    onChangeText={this.onChangeText}
                  />
                </View>
                <View style={styles.containerAccordian}>
                  <ToCheck
                    status={status}
                    status_lainnya={status_lainnya}
                    cleaning_tambah={cleaning_tambah}
                    dataTicketDetail={dataTicketDetail}
                    onChangeTextCheckandToDo={this.onChangeTextCheckandToDo}
                  />
                </View>
                <View style={styles.containerAccordian}>
                  <ToDo
                    onChangeTextCheckandToDo={this.onChangeTextCheckandToDo}
                    dataTicketDetail={dataTicketDetail}
                    cleaning_tambah_to_do={cleaning_tambah_to_do}
                    status={status}
                    status_lainnya={status_lainnya}
                    lengthToCheck={lengthToCheck}
                  />
                </View>
                <View style={styles.containerAccordian}>
                  <Geotaging
                    captureDistance={this.captureDistance}
                    data_mesin_cleaning={data_mesin_cleaning}
                  />
                </View>

                <ScrollView horizontal={true} nestedScrollEnabled={true}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-around',
                      width: '100%',
                      height: heightPercentageToDP('23%'),
                    }}>
                    {this.checkingImage()}
                    <TouchableOpacity
                      onPress={() => this._takePicture('image_clean')}>
                      <View style={styles.modalImagePicker}>
                        <Icon name="camera" size={50} />
                      </View>
                    </TouchableOpacity>
                  </View>
                </ScrollView>
                {/* <TextInput
                  onChangeText={(text) => onChangeText(text, document.name)}
                  multiline={true}
                  numberOfLines={5}
                  style={styles.modalNotesInput}
                  value={document.notes}
                  placeholder="Tulis keterangan di sini..."
                  placeholderTextColor={colors.light_grayish_blue}
                /> */}

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    marginTop: 20,
                    marginBottom: 30,
                  }}>
                  <TouchableOpacity
                    style={[
                      styles.viewPhotoButton,
                      styles.viewPhotoButtonActive,
                    ]}
                    onPress={() => togglePhotoModal()}>
                    <Text style={styles.buttonText}>Batalkan</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={[
                      styles.uploadPhotoButton,
                      statusLocationUser
                        ? styles.uploadPhotoButtonInactive
                        : styles.uploadPhotoButtonActive,
                    ]}
                    disabled={statusLocationUser ? true : false}
                    onPress={() => this.onPressUpload()}>
                    <Text style={styles.buttonText}>Simpan</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  containerModal: {
    flex: 1,
  },
  modalContainer: {
    height: heightPercentageToDP('90%'),
  },
  modalHeader: {
    height: heightPercentageToDP('7%'),
    backgroundColor: 'transparent',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    padding: 5,
  },
  modalContent: {
    paddingHorizontal: widthPercentageToDP('5%'),
    paddingVertical: heightPercentageToDP('1.5%'),
    backgroundColor: '#fff',
    borderRadius: 6,
  },
  modalPhotoName: {
    fontSize: widthPercentageToDP('6%'),
    fontFamily: 'Roboto',
    color: colors.dope_blue,
  },
  modalImagePicker: {
    height: widthPercentageToDP('25.3%'),
    width: widthPercentageToDP('25.3%'),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#778899',
    marginRight: widthPercentageToDP('2%'),
    borderRadius: 6,
  },
  modalNotesInput: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    textAlignVertical: 'top',
    fontSize: widthPercentageToDP('4.5%'),
    borderRadius: 5,
  },
  scrollableModal: {
    height: 250,
  },
  photoModal: {
    paddingBottom: 20,
  },
  scrollableModalContent1: {
    height: 200,
    backgroundColor: 'orange',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollableModalContent2: {
    height: 200,
    backgroundColor: 'lightgreen',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Roboto',
    fontSize: widthPercentageToDP('4%'),
    fontWeight: 'bold',
  },
  viewPhotoButton: {
    width: widthPercentageToDP('37.5%'),
    borderRadius: 5,
    backgroundColor: colors.bright_red,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%'),
  },
  viewPhotoButtonActive: {
    backgroundColor: colors.strong_grey,
  },
  viewPhotoButtonInactive: {
    backgroundColor: colors.very_soft_red,
  },
  uploadPhotoButton: {
    width: widthPercentageToDP('37.5%'),
    borderRadius: 5,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%'),
  },
  uploadPhotoButtonActive: {
    backgroundColor: colors.strong_blue,
  },
  uploadPhotoButtonInactive: {
    backgroundColor: colors.very_soft_blue,
  },
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
  },
  containerAccordian: {
    flex: 1,
    paddingTop: 0,
    backgroundColor: 'white',
    borderColor: colors.light_sapphire_bluish_gray,
    borderWidth: 2,
    borderRadius: 10,
    paddingHorizontal: 16,
    marginTop: 16,
  },
  titleAccordian: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  headerAccordian: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
});

const mapStateToProps = ({AppraisalData}) => ({AppraisalData});

export default connect(mapStateToProps)(UploadModalCleaningRegister);
