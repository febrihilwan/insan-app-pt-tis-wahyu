import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import Modal from "react-native-modal";
import AutoHeightImage from "react-native-auto-height-image";

import { widthPercentageToDP } from "../../utils";

const PreviewImageModal = props => {
  return (
    <Modal isVisible={props.viewImage} onBackdropPress={props.closeImage}>
      <View>
        <TouchableOpacity style={styles.closeBtn} onPress={props.closeImage}>
          <Text style={{ fontWeight: "bold", color: "white", fontSize: 20 }}>X</Text>
        </TouchableOpacity>

        <AutoHeightImage width={widthPercentageToDP("90%")} source={{ uri: props.imageLink }} />
      </View>
    </Modal>
  );
};

const styles = {
  closeBtn: {
    alignSelf: "flex-end",
    marginBottom: 10,
    padding: 10,
  },
};

export default PreviewImageModal;
