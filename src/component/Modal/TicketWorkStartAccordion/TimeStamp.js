import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  LayoutAnimation,
  Platform,
  UIManager,
  TextInput,
} from 'react-native';
import colors from '../../../utils/colors';
import {widthPercentageToDP, heightPercentageToDP} from '../../../utils';
import Icon from 'react-native-vector-icons/MaterialIcons';
import DatePicker from 'react-native-datepicker';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';

export default class TimeStamp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      expanded: false,
      isDatePickerVisible: false,
      isDatePickerVisibleArrival: false,
    };

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  showDatePicker = () => {
    this.setState({
      isDatePickerVisible: true,
    });
  };

  showDatePickerArrival = () => {
    this.setState({
      isDatePickerVisibleArrival: true,
    });
  };

  hideDatePicker = () => {
    this.setState({
      isDatePickerVisible: false,
    });
  };

  hideDatePickerArrival = () => {
    this.setState({
      isDatePickerVisibleArrival: false,
    });
  };

  handleConfirmDeparture = (date) => {
    const {onChangeText} = this.props;
    onChangeText(moment(date).format('YYYY-MM-DD HH:mm:ss'), 'departured');
    this.hideDatePicker();
  };

  handleConfirmArrival = (date) => {
    const {onChangeText} = this.props;
    onChangeText(moment(date).format('YYYY-MM-DD HH:mm:ss'), 'arrival');
    this.hideDatePicker();
  };

  render() {
    const {onChangeText, dataWorkStart, departured, arrival} = this.props;
    const {isDatePickerVisible, isDatePickerVisibleArrival} = this.state;

    return (
      <View>
        <TouchableOpacity
          ref={this.accordian}
          style={styles.row}
          onPress={() => this.toggleExpand()}>
          <Text style={[styles.title, styles.font]}>TIMESTAMP</Text>
          <Icon
            name={
              this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'
            }
            size={30}
            color={colors.strong_grey}
          />
        </TouchableOpacity>
        <View style={styles.parentHr} />
        {this.state.expanded && (
          <View>
            <Text style={styles.label}>Ticket Open (Created)</Text>
            <View style={styles.inputForm}>
              <DatePicker
                date={dataWorkStart.ticket.ticket_open}
                disabled
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Pilih tanggal"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'open_date');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>

            <Text style={styles.label}>Ticket Closed</Text>
            <View style={styles.inputForm}>
              <DatePicker
                date={
                  dataWorkStart.ticket.ticket_close === null
                    ? ''
                    : dataWorkStart.ticket.ticket_close
                }
                disabled
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder={
                  dataWorkStart.ticket.ticket_close === null ? 'Empty date' : ''
                }
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'close_date');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>

            <Text style={styles.label}>Ticket Duration</Text>
            <TextInput
              editable={false}
              value={
                dataWorkStart.ticket.ticket_duration
                  ? dataWorkStart.ticket.ticket_duration
                  : 'Empty ticket duration'
              }
              placeholder="please input your name"
              autoCapitalize="none"
              onChangeText={(text) => onChangeText(text, 'name')}
              style={styles.inputStyleReadOnly}
            />

            <Text style={styles.label}>Problem Open (Created)</Text>
            <View style={styles.inputForm}>
              <DatePicker
                date={dataWorkStart.problem_open}
                disabled
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Pilih tanggal"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'close_date');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>

            <Text style={styles.label}>Problem Confirmed</Text>
            <View style={styles.inputForm}>
              <DatePicker
                date={dataWorkStart.problem_confirmed}
                disabled
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Pilih tanggal"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'close_date');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>

            <Text style={styles.label}>Problem Closed</Text>
            <View style={styles.inputForm}>
              <DatePicker
                date={dataWorkStart.problem_close}
                disabled
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Empty date"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'close_date');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>

            <Text style={styles.label}>Problem Duration</Text>
            <View style={styles.inputForm}>
              <DatePicker
                date={dataWorkStart.problem_duration}
                disabled
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Empty date"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'close_date');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>
            <Text style={styles.label}>Departure</Text>
            <TouchableOpacity
              style={[styles.uploadPhotoButton, styles.uploadPhotoButtonActive]}
              onPress={() => this.showDatePicker()}>
              <Icon
                name="perm-contact-calendar"
                size={widthPercentageToDP('6%')}
                color="white"
              />
              <Text style={styles.buttonText}>Choose Date and Time</Text>
            </TouchableOpacity>
            <DateTimePickerModal
              isVisible={isDatePickerVisible}
              mode="datetime"
              onConfirm={this.handleConfirmDeparture}
              onCancel={this.hideDatePicker}
            />
            <Text style={styles.label}>{departured}</Text>
            <Text style={styles.label}>Arrival</Text>
            <TouchableOpacity
              style={[styles.uploadPhotoButton, styles.uploadPhotoButtonActive]}
              onPress={() => this.showDatePickerArrival()}>
              <Icon
                name="perm-contact-calendar"
                size={widthPercentageToDP('6%')}
                color="white"
              />
              <Text style={styles.buttonText}>Choose Date and Time</Text>
            </TouchableOpacity>
            <DateTimePickerModal
              isVisible={isDatePickerVisibleArrival}
              mode="datetime"
              onConfirm={this.handleConfirmArrival}
              onCancel={this.hideDatePickerArrival}
            />
            <Text style={styles.label}>{arrival}</Text>
            {/* <View style={styles.inputForm}>
              <DatePicker
                date={arrival}
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Pilih tanggal"
                // format="dddd, DD-MM-YYYY HH:mm:ss"
                format="YYYY-MM-DD"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'arrival');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View> */}
            <Text style={styles.label}>Work Start</Text>
            <View style={styles.inputForm}>
              <DatePicker
                date={new Date()}
                disabled
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Empty date"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'work_start');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>
            <Text style={styles.label}>Work Finish</Text>
            <View style={styles.inputForm}>
              <DatePicker
                date={dataWorkStart.work_finish}
                disabled
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Empty date"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'arrival');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>
          </View>
        )}
      </View>
    );
  }

  toggleExpand = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({expanded: !this.state.expanded});
  };
}

const styles = StyleSheet.create({
  buttonText: {
    color: 'white',
    fontFamily: 'Roboto',
    fontSize: widthPercentageToDP('4%'),
    fontWeight: 'bold',
    marginLeft: 10,
  },
  uploadPhotoButton: {
    width: widthPercentageToDP('60%'),
    borderRadius: 5,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%'),
    marginBottom: widthPercentageToDP('3%'),
    justifyContent: 'center',
    flexDirection: 'row',
  },
  uploadPhotoButtonActive: {
    backgroundColor: colors.strong_blue,
  },
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  parentHr: {
    height: 1,
    color: 'white',
    width: '100%',
  },
  child: {
    backgroundColor: colors.light_sapphire_bluish_gray,
    padding: 16,
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputStyleReadOnly: {
    width: '100%',
    backgroundColor: colors.light_bluish_gray,
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
    marginBottom: widthPercentageToDP('4%'),
  },
  inputFormDisabled: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    backgroundColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
  },
});
