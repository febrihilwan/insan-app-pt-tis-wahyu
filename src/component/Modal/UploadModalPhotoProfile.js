/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import Modal from 'react-native-modal';
import _ from 'lodash';
import {
  View,
  Text,
  Button,
  TouchableOpacity,
  TextInput,
  Image,
  ScrollView,
  Dimensions,
  StyleSheet,
  AsyncStorage,
  Picker,
  ToastAndroid,
} from 'react-native';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import {widthPercentageToDP, heightPercentageToDP} from '../../utils';
import colors from '../../utils/colors';
import Spinner from 'react-native-loading-spinner-overlay';
import {API_URL} from 'react-native-dotenv';
import Axios from 'axios';

class UploadModalPhotoProfile extends Component {
  constructor(props) {
    super(props);
    this.onChangeText = this.onChangeText.bind(this);
    this.state = {
      name: '',
      email: '',
      no_telp: '',
      updated_at: '',
      username: '',
      api_token: '',
      img_user: null,
      password: null,
      spinner: false,
    };
  }

  async componentDidMount() {
    this.setState({
      spinner: true,
    });
    // [TEST] SPINNER
    // setInterval(() => {
    //   this.setState({
    //     spinner: !this.state.spinner,
    //   });
    // }, 3000);
    try {
      let userData = await AsyncStorage.getItem('userData');
      if (userData) {
        let data = JSON.parse(userData);
        console.log('userData', data);
        this.setState({
          // name: data.api_token.name,
          // email: data.api_token.email,
          // no_telp: data.api_token.no_telp,
          // role: data.api_token.role,
          // updated_at: data.api_token.updated_at,
          // img_user: data.api_token.img_user,
          // username: data.api_token.username,
          // username: data.api_token.username,
          api_token: data.api_token.api_token,
          spinner: false,
        });
      }
    } catch (err) {
      // console.log(err);
      // console.log('Failed local login');
    }
  }

  onChangeText(text, name) {
    this.setState({
      [name]: text,
    });
  }

  _takePicture(field) {
    const options = {
      title: `Upload foto ${field} anda`,
      storageOptions: {
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
        console.error('Error pick image', response.error);
      } else {
        // if(this.state[field]) {
        //   let temp = {
        //     uri: response.uri,
        //     type: response.type,
        //     name: response.fileName,
        //   }

        //   let tempField = this.state[field]
        //   tempField.push(temp)

        //   this.setState({
        //     [field]: tempField
        //   });

        // } else {
        this.setState({
          [field]: {
            uri: response.uri,
            type: 'image/jpeg',
            name: response.fileName,
            path: response.path,
          },
        });
        // }
      }
    });
  }

  handleOnScroll = (event) => {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y,
    });
  };

  handleScrollTo = (p) => {
    if (this.scrollViewRef) {
      this.scrollViewRef.scrollTo(p);
    }
  };

  checkingImage = () => {
    let {img_user} = this.state;
    console.log(img_user);
    // let {document, AppraisalData} = this.props;
    // let data = _.find(AppraisalData.data, {objectField: {name: img_user.name}});
    // console.log("--",document)
    if (img_user) {
      return (
        <Image
          source={{
            uri: img_user.uri
              ? img_user.uri
              : `http://demo-kota.com/insan5/public/storage/public/user/${img_user}`,
          }}
          style={styles.modalImagePicker}
        />
      );
    } else {
      return null;
    }
  };

  async _onChangePhoto(formData) {
    const {api_token} = this.state;
    const {togglePhotoModal, onRefresh} = this.props;
    const url = `${API_URL}/photo`;
    const config = {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${api_token}`,
        'Access-Control-Allow-Origin': '*',
        Accept: 'multipart/form-data',
        'content-type': 'multipart/form-data',
      },
      data: formData,
    };

    try {
      await Axios(url, config);
      await this.setState({spinner: false});
      await togglePhotoModal();
      await alert('Edit profil Berhasil');
      await onRefresh();
    } catch (error) {
      this.setState({spinner: false});
      alert('Edit profil Gagal');
    }
  }

  onPressUpload = () => {
    const {img_user} = this.state;
    const poData = [{name: 'img_user', value: img_user}];
    const emptyField = poData.find((field) => field.value === '');

    if (!emptyField) {
      this.setState({
        spinner: true,
      });

      let formData = new FormData();
      formData.append('img_user', img_user);

      this._onChangePhoto(formData);
    } else {
      alert(`field ${emptyField.name} wajib di isi`);
    }
  };

  render() {
    const {displayPhotoModal, togglePhotoModal} = this.props;
    const {spinner} = this.state;

    return (
      <Modal
        supportedOrientations={['portrait', 'landscape']}
        onBackdropPress={() => togglePhotoModal()}
        isVisible={displayPhotoModal}
        style={styles.photoModal}>
        <Spinner
          visible={spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <View
          style={
            Dimensions.get('window').height <= 360 ? styles.scrollableModal : {}
          }>
          <View style={styles.modalContainer}>
            <View style={styles.modalHeader}>
              <TouchableOpacity onPress={() => togglePhotoModal()}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: widthPercentageToDP('8%'),
                    fontFamily: 'Roboto',
                  }}>
                  x
                </Text>
              </TouchableOpacity>
            </View>
            <ScrollView
              ref={(ref) => (this.scrollViewRef = ref)}
              onScroll={this.handleOnScroll}
              scrollEventThrottle={16}>
              <View style={styles.modalContent}>
                <Text style={styles.modalPhotoName}>Edit Photo Profil</Text>

                <ScrollView horizontal={true} nestedScrollEnabled={true}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-around',
                      width: '100%',
                      height: heightPercentageToDP('23%'),
                    }}>
                    {this.checkingImage()}
                    <TouchableOpacity
                      onPress={() => this._takePicture('img_user')}>
                      <View style={styles.modalImagePicker}>
                        <Icon name="camera" size={50} />
                      </View>
                    </TouchableOpacity>
                  </View>
                </ScrollView>
                {/* <TextInput
                  onChangeText={(text) => onChangeText(text, document.name)}
                  multiline={true}
                  numberOfLines={5}
                  style={styles.modalNotesInput}
                  value={document.notes}
                  placeholder="Tulis keterangan di sini..."
                  placeholderTextColor={colors.light_grayish_blue}
                /> */}

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    marginTop: 20,
                    marginBottom: 30,
                  }}>
                  <TouchableOpacity
                    style={[
                      styles.viewPhotoButton,
                      styles.viewPhotoButtonActive,
                    ]}
                    onPress={() => togglePhotoModal()}>
                    <Text style={styles.buttonText}>Batalkan</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={[
                      styles.uploadPhotoButton,
                      styles.uploadPhotoButtonActive,
                    ]}
                    onPress={() => this.onPressUpload()}>
                    <Text style={styles.buttonText}>Simpan</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  containerModal: {
    flex: 1,
  },
  modalContainer: {
    height: heightPercentageToDP('90%'),
  },
  modalHeader: {
    height: heightPercentageToDP('7%'),
    backgroundColor: 'transparent',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    padding: 5,
  },
  modalContent: {
    paddingHorizontal: widthPercentageToDP('5%'),
    paddingVertical: heightPercentageToDP('1.5%'),
    backgroundColor: '#fff',
    borderRadius: 6,
  },
  modalPhotoName: {
    fontSize: widthPercentageToDP('6%'),
    fontFamily: 'Roboto',
    color: colors.dope_blue,
  },
  modalImagePicker: {
    height: widthPercentageToDP('25.3%'),
    width: widthPercentageToDP('25.3%'),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#778899',
    marginRight: widthPercentageToDP('2%'),
    borderRadius: 6,
  },
  modalNotesInput: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    textAlignVertical: 'top',
    fontSize: widthPercentageToDP('4.5%'),
    borderRadius: 5,
  },
  scrollableModal: {
    height: 250,
  },
  photoModal: {
    paddingBottom: 20,
  },
  scrollableModalContent1: {
    height: 200,
    backgroundColor: 'orange',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollableModalContent2: {
    height: 200,
    backgroundColor: 'lightgreen',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Roboto',
    fontSize: widthPercentageToDP('4%'),
    fontWeight: 'bold',
  },
  viewPhotoButton: {
    width: widthPercentageToDP('37.5%'),
    borderRadius: 5,
    backgroundColor: colors.bright_red,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%'),
  },
  viewPhotoButtonActive: {
    backgroundColor: colors.strong_grey,
  },
  viewPhotoButtonInactive: {
    backgroundColor: colors.very_soft_red,
  },
  uploadPhotoButton: {
    width: widthPercentageToDP('37.5%'),
    borderRadius: 5,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%'),
  },
  uploadPhotoButtonActive: {
    backgroundColor: colors.strong_blue,
  },
  uploadPhotoButtonInactive: {
    backgroundColor: colors.very_soft_blue,
  },
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
  },
});

const mapStateToProps = ({AppraisalData}) => ({AppraisalData});

export default connect(mapStateToProps)(UploadModalPhotoProfile);
