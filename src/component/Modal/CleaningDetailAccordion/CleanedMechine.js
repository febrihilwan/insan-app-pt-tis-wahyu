import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  LayoutAnimation,
  Platform,
  UIManager,
  TextInput,
} from 'react-native';
import colors from '../../../utils/colors';
import {widthPercentageToDP, heightPercentageToDP} from '../../../utils';
import Icon from 'react-native-vector-icons/MaterialIcons';
import DatePicker from 'react-native-datepicker';

export default class CleanedMechine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      expanded: false,
    };

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  render() {
    const {onChangeText, dataTicketDetail} = this.props;
    // console.log('dataTicketDetailASDASD', dataTicketDetail);
    return (
      <View>
        <TouchableOpacity
          ref={this.accordian}
          style={styles.row}
          onPress={() => this.toggleExpand()}>
          <Text style={[styles.title, styles.font]}>MACHINE INFO</Text>
          <Icon
            name={
              this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'
            }
            size={30}
            color={colors.strong_grey}
          />
        </TouchableOpacity>
        <View style={styles.parentHr} />
        {this.state.expanded && (
          <View>
            <Text style={styles.label}>Bank</Text>
            <TextInput
              editable={false}
              value={dataTicketDetail.machine.bank.name_bank}
              placeholder="please input your name"
              autoCapitalize="none"
              onChangeText={(text) => onChangeText(text, 'name')}
              style={styles.inputStyleReadOnly}
            />

            <Text style={styles.label}>Branch</Text>
            <TextInput
              editable={false}
              value={dataTicketDetail.machine.branch.name_branch}
              placeholder="please input your username"
              autoCapitalize="none"
              keyboardType="numeric"
              onChangeText={(text) => onChangeText(text, 'no_telp')}
              style={styles.inputStyleReadOnly}
            />

            <Text style={styles.label}>Machine ID</Text>
            <TextInput
              editable={false}
              value={dataTicketDetail.machine.machine_id}
              placeholder="please input your username"
              autoCapitalize="none"
              onChangeText={(text) => onChangeText(text, 'pic_bank')}
              style={styles.inputStyleReadOnly}
            />

            <Text style={styles.label}>Machine SN</Text>
            <TextInput
              editable={false}
              value={dataTicketDetail.machine.machine_sn}
              placeholder="please input your username"
              autoCapitalize="none"
              keyboardType="numeric"
              onChangeText={(text) => onChangeText(text, 'pic_telp')}
              style={styles.inputStyleReadOnly}
            />
            <Text style={styles.label}>Machine Type</Text>
            <TextInput
              editable={false}
              value={dataTicketDetail.machine.type.name_type}
              placeholder="please input your username"
              autoCapitalize="none"
              keyboardType="numeric"
              onChangeText={(text) => onChangeText(text, 'pic_telp')}
              style={styles.inputStyleReadOnly}
            />

            <Text style={styles.label}>Location</Text>
            <TextInput
              editable={false}
              value={dataTicketDetail.machine.name_address}
              placeholder="please input your username"
              autoCapitalize="none"
              keyboardType="numeric"
              onChangeText={(text) => onChangeText(text, 'pic_telp')}
              style={styles.inputStyleReadOnly}
            />
          </View>
        )}
      </View>
    );
  }

  toggleExpand = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({expanded: !this.state.expanded});
  };
}

const styles = StyleSheet.create({
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  parentHr: {
    height: 1,
    color: 'white',
    width: '100%',
  },
  child: {
    backgroundColor: colors.light_sapphire_bluish_gray,
    padding: 16,
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputStyleReadOnly: {
    width: '100%',
    backgroundColor: colors.light_bluish_gray,
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
    marginBottom: widthPercentageToDP('4%'),
  },
  inputFormDisabled: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    backgroundColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
  },
});
