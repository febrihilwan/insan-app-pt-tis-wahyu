import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  LayoutAnimation,
  Platform,
  UIManager,
  TextInput,
} from 'react-native';
import colors from '../../../utils/colors';
import {widthPercentageToDP, heightPercentageToDP} from '../../../utils';
import Icon from 'react-native-vector-icons/MaterialIcons';
import DatePicker from 'react-native-datepicker';

export default class TimeStamp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      expanded: false,
    };

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  render() {
    const {onChangeText, dataTicketDetail} = this.props;
    return (
      <View>
        <TouchableOpacity
          ref={this.accordian}
          style={styles.row}
          onPress={() => this.toggleExpand()}>
          <Text style={[styles.title, styles.font]}>TIMESTAMP</Text>
          <Icon
            name={
              this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'
            }
            size={30}
            color={colors.strong_grey}
          />
        </TouchableOpacity>
        <View style={styles.parentHr} />
        {this.state.expanded && (
          <View>
            <Text style={styles.label}>Cleaning Start</Text>
            <View style={styles.inputForm}>
              <DatePicker
                disabled
                date={dataTicketDetail.cleaning_start}
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Pilih tanggal"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'cleaning_start');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>

            <Text style={styles.label}>Cleaning Finished</Text>
            <View style={styles.inputForm}>
              <DatePicker
                disabled
                date={dataTicketDetail.cleaning_finish}
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Pilih tanggal"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'cleaning_finish');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>
          </View>
        )}
      </View>
    );
  }

  toggleExpand = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({expanded: !this.state.expanded});
  };
}

const styles = StyleSheet.create({
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  parentHr: {
    height: 1,
    color: 'white',
    width: '100%',
  },
  child: {
    backgroundColor: colors.light_sapphire_bluish_gray,
    padding: 16,
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputStyleReadOnly: {
    width: '100%',
    backgroundColor: colors.light_bluish_gray,
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
    marginBottom: widthPercentageToDP('4%'),
  },
  inputFormDisabled: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    backgroundColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
  },
});
