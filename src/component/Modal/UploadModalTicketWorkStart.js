/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import Modal from 'react-native-modal';
import _, {filter, isArray, size} from 'lodash';
import moment from 'moment';
import {
  View,
  Text,
  Button,
  TouchableOpacity,
  TextInput,
  Image,
  ScrollView,
  Dimensions,
  StyleSheet,
  AsyncStorage,
  Picker,
  LayoutAnimation,
  UIManager,
  ToastAndroid,
} from 'react-native';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import {widthPercentageToDP, heightPercentageToDP} from '../../utils';
import colors from '../../utils/colors';
import Spinner from 'react-native-loading-spinner-overlay';
import TicketInfo from './TicketWorkStartAccordion/TicketInfo';
import ProblemInfo from './TicketWorkStartAccordion/ProblemInfo';
import EngineerInfo from './TicketWorkStartAccordion/EngineerInfo';
import AtmEngineerInfo from './TicketWorkStartAccordion/AtmEngineerInfo';
import TimeStamp from './TicketWorkStartAccordion/TimeStamp';
import HealthCheck from './TicketWorkStartAccordion/HealthCheck';
import RemovalAndReplacementPartOne from './TicketWorkStartAccordion/RemovalAndReplacementPartOne';
import RemovalAndReplacementPartTwo from './TicketWorkStartAccordion/RemovalAndReplacementPartTwo';
import RemovalAndReplacementPartThree from './TicketWorkStartAccordion/RemovalAndReplacementPartThree';
import RemovalAndReplacementPartFour from './TicketWorkStartAccordion/RemovalAndReplacementPartFour';
import RemovalAndReplacementPartFive from './TicketWorkStartAccordion/RemovalAndReplacementPartFive';
import ProblemAndAction from './TicketWorkStartAccordion/ProblemAndAction';
import Miscellaneous from './TicketWorkStartAccordion/Miscellaneous';
import Geotaging from './TicketWorkStartAccordion/Geotaging';
import {API_URL} from 'react-native-dotenv';
import Axios from 'axios';
import checkFields from '../../utils/checkFields';

class UploadModalTicketWorkStart extends Component {
  constructor(props) {
    super(props);
    this.onChangeText = this.onChangeText.bind(this);
    this.captureDistance = this.captureDistance.bind(this);
    this.state = {
      name: '',
      email: '',
      no_telp: '',
      updated_at: '',
      username: '',
      api_token: '',
      img_user: '',
      img_user: '',
      password: '',
      spinner: false,
      expanded: false,
      statusLocationUser: true,
      grounding: '',
      it: '',
      voltage: '',
      ac: '',
      ups: '',
      notes: '',
      image_problem: '',
      departured: '',
      arrival: '',
	  work_start: new Date(),
    };
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  captureDistance(dis) {
    if (dis <= 50) {
      this.setState({
        statusLocationUser: false,
      });
    }
  }

  async componentDidMount() {
    // [TEST] SPINNER
    // setInterval(() => {
    //   this.setState({
    //     spinner: !this.state.spinner,
    //   });
    // }, 3000);
    try {
      let userData = await AsyncStorage.getItem('userData');
      if (userData) {
        let data = JSON.parse(userData);
        // console.log('userData', data);
        this.setState({
          api_token: data.api_token.api_token,
        });
      }
    } catch (err) {
      // console.log(err);
      // console.log('Failed local login');
    }
  }

  onChangeText(text, name) {
    this.setState(
      {
        [name]: text,
      },
      () => console.log(name, '=', text),
    );
  }

  _takePicture(field) {
    const options = {
      title: `Upload foto ${field} anda`,
      storageOptions: {
        path: 'images',
      },
      compressImageQuality: 0.1,
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response upload photo = ', response);
      if (response.didCancel) {
        console.log('Pick image cancelled');
      } else if (response.error) {
        console.log('Error pick image', response.error);
      } else {
        // if(this.state[field]) {
        //   let temp = {
        //     uri: response.uri,
        //     type: response.type,
        //     name: response.fileName,
        //   }

        //   let tempField = this.state[field]
        //   tempField.push(temp)

        //   this.setState({
        //     [field]: tempField
        //   });

        // } else {
        this.setState({
          [field]: {
            uri: response.uri,
            type: 'image/jpeg',
            name: response.fileName,
            path: response.path,
          },
        });
        // }
      }
    });
  }

  handleOnScroll = (event) => {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y,
    });
  };

  handleScrollTo = (p) => {
    if (this.scrollViewRef) {
      this.scrollViewRef.scrollTo(p);
    }
  };

  checkingImage = () => {
    let {image_problem} = this.state;
    let {document, AppraisalData} = this.props;
    let data = _.find(AppraisalData.data, {
      objectField: {name: image_problem.name},
    });
    // console.log("--",document)
    if (image_problem) {
      return (
        <Image
          source={{uri: image_problem.uri}}
          style={styles.modalImagePicker}
        />
      );
    } else {
      return null;
    }
  };

  async postData(formData) {
    const {dataWorkStart, togglePhotoModal} = this.props;
    const {api_token} = this.state;
    const url = `${API_URL}/work_start/${dataWorkStart.id}`;
    const config = {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${api_token}`,
        'Access-Control-Allow-Origin': '*',
        Accept: 'multipart/form-data',
        'content-type': 'multipart/form-data',
      },
      data: formData,
    };

    try {
      const response = await Axios(url, config);
      await togglePhotoModal();
      await alert('Work Start Berhasil');
      await this.setState({spinner: false});
    } catch (error) {
      alert('Work Start Gagal');
      await this.setState({spinner: false});
    }
  }

  onPressUpload = async () => {
    const {
      grounding,
      it,
      voltage,
      ac,
      ups,
      notes,
      image_problem,
      arrival,
      departured,
	  work_start,
    } = this.state;

    let poData = [
      {name: 'Grounding', value: grounding, required: false},
      {name: 'IT', value: it, required: false},
      {name: 'Voltage', value: voltage, required: false},
      {name: 'AC', value: ac, required: false},
      {name: 'UPS', value: ups, required: false},
      {name: 'Notes', value: notes, required: false},
      {name: 'Arrival', value: arrival, required: true},
      {name: 'Departured', value: departured, required: true},
      // {name: 'image_problem', value: image_problem},
    ];

    let emptyField = checkFields(poData);
    const isEmptyFields = size(emptyField) === 0;

    if (isEmptyFields) {
      this.setState({
        spinner: true,
      });

      let formData = new FormData();
	  formData.append('work_start', moment(work_start).format('YYYY-MM-DD HH:mm:ss'))
      formData.append('arrival', arrival);
      formData.append('departured', departured);
      formData.append('grounding', grounding);
      formData.append('it', it);
      formData.append('voltage', voltage);
      formData.append('ac', ac);
      formData.append('ups', ups);
      formData.append('notes', notes);
      formData.append('image_problem', image_problem);

      this.postData(formData);
    } else {
      if (isArray(emptyField)) {
        const messageText = emptyField.map((itemField) => {
          return `Field ${itemField.name} wajib di isi.\n`;
        });
        alert(messageText.join(''));
      }
    }
  };

  render() {
    const {
      displayPhotoModal,
      togglePhotoModal,
      document,
      previewPhoto,
      AppraisalData,
      dataWorkStart,
    } = this.props;

    const {
      spinner,
      grounding,
      it,
      voltage,
      ac,
      ups,
      departured,
      arrival,
      notes,
      statusLocationUser,
    } = this.state;

    let images = document.imageUrl;
    let data = _.find(AppraisalData.data, {objectField: {name: document.name}});

    // console.log("aa", data)
    return (
      <Modal
        supportedOrientations={['portrait', 'landscape']}
        onBackdropPress={() => togglePhotoModal()}
        isVisible={displayPhotoModal}
        style={styles.photoModal}
        onModalShow={() => console.log('ini', document)}>
        <Spinner
          visible={spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <View
          style={
            Dimensions.get('window').height <= 360 ? styles.scrollableModal : {}
          }>
          <View style={styles.modalContainer}>
            <View style={styles.modalHeader}>
              <TouchableOpacity onPress={() => togglePhotoModal()}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: widthPercentageToDP('8%'),
                    fontFamily: 'Roboto',
                  }}>
                  x
                </Text>
              </TouchableOpacity>
            </View>
            <ScrollView
              ref={(ref) => (this.scrollViewRef = ref)}
              onScroll={this.handleOnScroll}
              scrollEventThrottle={16}>
              <View style={styles.modalContent}>
                <Text style={styles.modalPhotoName}>Work Start</Text>
                <View style={styles.containerAccordian}>
                  <TicketInfo dataWorkStart={dataWorkStart} />
                </View>
                <View style={styles.containerAccordian}>
                  <ProblemInfo dataWorkStart={dataWorkStart} />
                </View>
                <View style={styles.containerAccordian}>
                  <EngineerInfo dataWorkStart={dataWorkStart} />
                </View>
                <View style={styles.containerAccordian}>
                  <AtmEngineerInfo dataWorkStart={dataWorkStart} />
                </View>
                <View style={styles.containerAccordian}>
                  <TimeStamp
                    onChangeText={this.onChangeText}
                    dataWorkStart={dataWorkStart}
                    departured={departured}
                    arrival={arrival}
                  />
                </View>
                <View style={styles.containerAccordian}>
                  <HealthCheck
                    grounding={grounding}
                    it={it}
                    voltage={voltage}
                    ac={ac}
                    ups={ups}
                    onChangeText={this.onChangeText}
                  />
                </View>
                <View style={styles.containerAccordian}>
                  <View style={styles.headerAccordian}>
                    <Text style={[styles.titleAccordian, styles.fontAccordian]}>
                      PART REMOVAL & REPLACEMENT
                    </Text>
                  </View>
                </View>
                <View style={styles.containerAccordian}>
                  <RemovalAndReplacementPartOne />
                </View>
                <View style={styles.containerAccordian}>
                  <RemovalAndReplacementPartTwo />
                </View>
                <View style={styles.containerAccordian}>
                  <RemovalAndReplacementPartThree />
                </View>
                <View style={styles.containerAccordian}>
                  <RemovalAndReplacementPartFour />
                </View>
                <View style={styles.containerAccordian}>
                  <RemovalAndReplacementPartFive />
                </View>
                <View style={styles.containerAccordian}>
                  <ProblemAndAction
                    notes={notes}
                    onChangeText={this.onChangeText}
                    dataWorkStart={dataWorkStart}
                  />
                </View>
                <View style={styles.containerAccordian}>
                  <Miscellaneous dataWorkStart={dataWorkStart} />
                </View>
                <View style={styles.containerAccordian}>
                  <Geotaging
                    captureDistance={this.captureDistance}
                    dataWorkStart={dataWorkStart}
                  />
                </View>

                <ScrollView horizontal={true} nestedScrollEnabled={true}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-around',
                      width: '100%',
                      height: heightPercentageToDP('23%'),
                    }}>
                    {this.checkingImage()}
                    <TouchableOpacity
                      onPress={() => this._takePicture('image_problem')}>
                      <View style={styles.modalImagePicker}>
                        <Icon name="camera" size={50} />
                      </View>
                    </TouchableOpacity>
                  </View>
                </ScrollView>
                {/* <TextInput
                  onChangeText={(text) => onChangeText(text, document.name)}
                  multiline={true}
                  numberOfLines={5}
                  style={styles.modalNotesInput}
                  value={document.notes}
                  placeholder="Tulis keterangan di sini..."
                  placeholderTextColor={colors.light_grayish_blue}
                /> */}

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    marginTop: 20,
                    marginBottom: 30,
                  }}>
                  <TouchableOpacity
                    style={[
                      styles.viewPhotoButton,
                      styles.viewPhotoButtonActive,
                    ]}
                    onPress={() => togglePhotoModal()}>
                    <Icon
                      name="close"
                      size={widthPercentageToDP('6%')}
                      color="white"
                    />
                    <Text style={styles.buttonText}>Batalkan</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={[
                      styles.uploadPhotoButton,
                      statusLocationUser
                        ? styles.uploadPhotoButtonInactive
                        : styles.uploadPhotoButtonActive,
                    ]}
                    disabled={statusLocationUser ? true : false}
                    onPress={() => this.onPressUpload()}>
                    <Icon
                      name="save"
                      size={widthPercentageToDP('6%')}
                      color="white"
                    />
                    <Text style={styles.buttonText}>Simpan</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  containerModal: {
    flex: 1,
  },
  modalContainer: {
    height: heightPercentageToDP('90%'),
  },
  modalHeader: {
    height: heightPercentageToDP('7%'),
    backgroundColor: 'transparent',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    padding: 5,
  },
  modalContent: {
    paddingHorizontal: widthPercentageToDP('5%'),
    paddingVertical: heightPercentageToDP('1.5%'),
    backgroundColor: '#fff',
    borderRadius: 6,
  },
  modalPhotoName: {
    fontSize: widthPercentageToDP('6%'),
    fontFamily: 'Roboto',
    color: colors.dope_blue,
  },
  modalImagePicker: {
    height: widthPercentageToDP('25.3%'),
    width: widthPercentageToDP('25.3%'),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#778899',
    marginRight: widthPercentageToDP('2%'),
    borderRadius: 6,
  },
  modalNotesInput: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    textAlignVertical: 'top',
    fontSize: widthPercentageToDP('4.5%'),
    borderRadius: 5,
  },
  scrollableModal: {
    height: 250,
  },
  photoModal: {
    paddingBottom: 20,
  },
  scrollableModalContent1: {
    height: 200,
    backgroundColor: 'orange',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollableModalContent2: {
    height: 200,
    backgroundColor: 'lightgreen',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Roboto',
    fontSize: widthPercentageToDP('4%'),
    fontWeight: 'bold',
    marginLeft: 10,
  },
  viewPhotoButton: {
    width: widthPercentageToDP('37.5%'),
    borderRadius: 5,
    backgroundColor: colors.bright_red,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%'),
    justifyContent: 'center',
    flexDirection: 'row',
  },
  viewPhotoButtonActive: {
    backgroundColor: colors.strong_grey,
  },
  viewPhotoButtonInactive: {
    backgroundColor: colors.very_soft_red,
  },
  uploadPhotoButton: {
    width: widthPercentageToDP('37.5%'),
    borderRadius: 5,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%'),
    justifyContent: 'center',
    flexDirection: 'row',
  },
  uploadPhotoButtonActive: {
    backgroundColor: colors.strong_blue,
  },
  uploadPhotoButtonInactive: {
    backgroundColor: colors.very_soft_blue,
  },
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
  },
  containerAccordian: {
    flex: 1,
    paddingTop: 0,
    backgroundColor: 'white',
    borderColor: colors.light_sapphire_bluish_gray,
    borderWidth: 2,
    borderRadius: 10,
    paddingHorizontal: 16,
    marginTop: 16,
  },
  titleAccordian: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  headerAccordian: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
});

const mapStateToProps = ({AppraisalData}) => ({AppraisalData});

export default connect(mapStateToProps)(UploadModalTicketWorkStart);
