import React, {Component} from 'react';
import Modal from 'react-native-modal';
import _ from 'lodash';
import {
  View,
  Text,
  Button,
  TouchableOpacity,
  TextInput,
  Image,
  ScrollView,
  Dimensions,
  StyleSheet,
  AsyncStorage,
  Picker,
  LayoutAnimation,
  UIManager,
} from 'react-native';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import {widthPercentageToDP, heightPercentageToDP} from '../../utils';
import colors from '../../utils/colors';
import Spinner from 'react-native-loading-spinner-overlay';
import CleaningInfo from './CleaningDetailAccordion/CleaningInfo';
import CleanedMechine from './CleaningDetailAccordion/CleanedMechine';
import Geotaging from './CleaningDetailAccordion/Geotaging';
import TimeStamp from './CleaningDetailAccordion/TimeStamp';
import ToCheck from './CleaningDetailAccordion/ToCheck';
import ToDo from './CleaningDetailAccordion/ToDo';
import {API_URL} from 'react-native-dotenv';

class UploadModalCleaningDetail extends Component {
  constructor(props) {
    super(props);
    this.onChangeText = this.onChangeText.bind(this);
    this.state = {
      name: '',
      email: '',
      no_telp: '',
      updated_at: '',
      username: '',
      api_token: '',
      img_user: '',
      img_user: '',
      password: '',
      spinner: false,
      expanded: false,
    };
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  async componentDidMount() {
    // [TEST] SPINNER
    // setInterval(() => {
    //   this.setState({
    //     spinner: !this.state.spinner,
    //   });
    // }, 3000);
    try {
      let userData = await AsyncStorage.getItem('userData');
      if (userData) {
        let data = JSON.parse(userData);
        // console.log('userData', data);
        this.setState({
          api_token: data.api_token.api_token,
        });
      }
    } catch (err) {
      // console.log(err);
      // console.log('Failed local login');
    }
  }

  onChangeText(text, name) {
    this.setState(
      {
        [name]: text,
      },
    );
  }

  _takePicture(field) {
    const options = {
      title: `Upload foto ${field} anda`,
      storageOptions: {
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response upload photo = ', response);
      if (response.didCancel) {
        // console.log('Pick image cancelled');
      } else if (response.error) {
        console.error('Error pick image', response.error);
      } else {
        // if(this.state[field]) {
        //   let temp = {
        //     uri: response.uri,
        //     type: response.type,
        //     name: response.fileName,
        //   }

        //   let tempField = this.state[field]
        //   tempField.push(temp)

        //   this.setState({
        //     [field]: tempField
        //   });

        // } else {
        this.setState({
          [field]: {
            uri: response.uri,
            type: 'image/jpeg',
            name: response.fileName,
            path: response.path,
          },
        });
        // }
      }
    });
  }

  handleOnScroll = (event) => {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y,
    });
  };

  handleScrollTo = (p) => {
    if (this.scrollViewRef) {
      this.scrollViewRef.scrollTo(p);
    }
  };

  checkingImage = () => {
    let {img_user} = this.state;
    let {document, AppraisalData, dataTicketDetail} = this.props;
    let data = _.find(AppraisalData.data, {objectField: {name: img_user.name}});
    console.log('dataTicketDetail.image_clean', dataTicketDetail.image_clean);
    // console.log("--",document)
    if (img_user || dataTicketDetail.image_clean) {
      return (
        <Image
          source={{
            uri: dataTicketDetail.image_clean
              ? `https://demo-kota.com/insan5/public/storage/public/cleaning/${dataTicketDetail.image_clean}`
              : img_user.uri,
          }}
          style={styles.modalImagePicker}
        />
      );
    } else {
      return null;
    }
  };

  onPressUpload = async () => {
    const {
      api_token,
      name,
      email,
      no_telp,
      username,
      img_user,
      password,
    } = this.state;

    // console.log(api_token, name, email, no_telp, username, img_user);

    let poData = [
      {name: 'name', value: name},
      {name: 'email', value: email},
      {name: 'no_telp', value: no_telp},
      {name: 'username', value: username},
      {name: 'img_user', value: img_user},
      {name: 'password', value: password},
    ];

    let emptyField = poData.find((field) => field.value === '');

    if (!emptyField) {
      this.setState({
        spinner: true,
      });

      let headers = {
        Authorization: `Bearer ${api_token}`,
        'Access-Control-Allow-Origin': '*',
        Accept: 'multipart/form-data',
        'content-type': 'multipart/form-data',
      };

      let formData = new FormData();
      formData.append('name', name);
      formData.append('email', email);
      formData.append('no_telp', no_telp);
      formData.append('username', username);
      formData.append('password', password);
      formData.append('img_user', img_user);

      fetch(`${API_URL}/user/edit`, {
        method: 'POST',
        headers,
        body: formData,
      })
        .then((response) => console.log(response.text()))
        .then(async (responseData) => {
          console.log(responseData);
          this.setState({
            spinner: false,
          });
          alert('Edit profil Berhasil');
        });

      this.setState({
        displayPhotoModal: !this.props.displayPhotoModal,
      });
    } else {
      alert(`field ${emptyField.name} wajib di isi`);
    }
  };

  render() {
    const {
      displayPhotoModal,
      togglePhotoModal,
      document,
      previewPhoto,
      AppraisalData,
      dataTicketDetail,
      dataTicketDetailLebihDetail,
    } = this.props;

    const {
      name,
      email,
      no_telp,
      updated_at,
      img_user,
      username,
      spinner,
    } = this.state;

    let images = document.imageUrl;
    let data = _.find(AppraisalData.data, {objectField: {name: document.name}});

    console.log('dataTicketDetailLebihDetail', dataTicketDetailLebihDetail);
    return (
      <Modal
        supportedOrientations={['portrait', 'landscape']}
        onBackdropPress={() => togglePhotoModal()}
        isVisible={displayPhotoModal}
        style={styles.photoModal}
        onModalShow={() => console.log('ini', document)}>
        <Spinner
          visible={spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <View
          style={
            Dimensions.get('window').height <= 360 ? styles.scrollableModal : {}
          }>
          <View style={styles.modalContainer}>
            <View style={styles.modalHeader}>
              <TouchableOpacity onPress={() => togglePhotoModal()}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: widthPercentageToDP('8%'),
                    fontFamily: 'Roboto',
                  }}>
                  x
                </Text>
              </TouchableOpacity>
            </View>
            <ScrollView
              ref={(ref) => (this.scrollViewRef = ref)}
              onScroll={this.handleOnScroll}
              scrollEventThrottle={16}>
              <View style={styles.modalContent}>
                <Text style={styles.modalPhotoName}>Cleaning Detail</Text>
                <View style={styles.containerAccordian}>
                  <CleaningInfo dataTicketDetail={dataTicketDetail} />
                </View>
                <View style={styles.containerAccordian}>
                  <CleanedMechine dataTicketDetail={dataTicketDetail} />
                </View>
                <View style={styles.containerAccordian}>
                  <TimeStamp dataTicketDetail={dataTicketDetail} />
                </View>
                <View style={styles.containerAccordian}>
                  <ToCheck
                    dataTicketDetailLebihDetail={dataTicketDetailLebihDetail}
                    dataTicketDetail={dataTicketDetail}
                  />
                </View>
                <View style={styles.containerAccordian}>
                  <ToDo
                    dataTicketDetailLebihDetail={dataTicketDetailLebihDetail}
                    dataTicketDetail={dataTicketDetail}
                  />
                </View>
                <View style={styles.containerAccordian}>
                  <Geotaging dataTicketDetail={dataTicketDetail} />
                </View>

                <ScrollView horizontal={true} nestedScrollEnabled={true}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-around',
                      width: '100%',
                      height: heightPercentageToDP('43%'),
                    }}>
                    {this.checkingImage()}
                    {/* <TouchableOpacity
                      onPress={() => this._takePicture('img_user')}>
                      <View style={styles.modalImagePicker}>
                        <Icon name="camera" size={50} />
                      </View>
                    </TouchableOpacity> */}
                  </View>
                </ScrollView>
                {/* <TextInput
                  onChangeText={(text) => onChangeText(text, document.name)}
                  multiline={true}
                  numberOfLines={5}
                  style={styles.modalNotesInput}
                  value={document.notes}
                  placeholder="Tulis keterangan di sini..."
                  placeholderTextColor={colors.light_grayish_blue}
                /> */}

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    marginTop: 20,
                    marginBottom: 30,
                  }}>
                  <TouchableOpacity
                    style={[
                      styles.viewPhotoButton,
                      styles.viewPhotoButtonActive,
                    ]}
                    onPress={() => togglePhotoModal()}>
                    <Text style={styles.buttonText}>Keluar</Text>
                  </TouchableOpacity>

                  {/* <TouchableOpacity
                    style={[
                      styles.uploadPhotoButton,
                      styles.uploadPhotoButtonActive,
                    ]}
                    onPress={() => this.onPressUpload()}>
                    <Text style={styles.buttonText}>Simpan</Text>
                  </TouchableOpacity> */}
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  containerModal: {
    flex: 1,
  },
  modalContainer: {
    height: heightPercentageToDP('90%'),
  },
  modalHeader: {
    height: heightPercentageToDP('7%'),
    backgroundColor: 'transparent',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    padding: 5,
  },
  modalContent: {
    paddingHorizontal: widthPercentageToDP('5%'),
    paddingVertical: heightPercentageToDP('1.5%'),
    backgroundColor: '#fff',
    borderRadius: 6,
  },
  modalPhotoName: {
    fontSize: widthPercentageToDP('6%'),
    fontFamily: 'Roboto',
    color: colors.dope_blue,
  },
  modalImagePicker: {
    height: widthPercentageToDP('70%'),
    width: widthPercentageToDP('70%'),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#778899',
    borderRadius: 6,
  },
  modalNotesInput: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    textAlignVertical: 'top',
    fontSize: widthPercentageToDP('4.5%'),
    borderRadius: 5,
  },
  scrollableModal: {
    height: 250,
  },
  photoModal: {
    paddingBottom: 20,
  },
  scrollableModalContent1: {
    height: 200,
    backgroundColor: 'orange',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollableModalContent2: {
    height: 200,
    backgroundColor: 'lightgreen',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Roboto',
    fontSize: widthPercentageToDP('4%'),
    fontWeight: 'bold',
  },
  viewPhotoButton: {
    width: widthPercentageToDP('37.5%'),
    borderRadius: 5,
    backgroundColor: colors.bright_red,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%'),
  },
  viewPhotoButtonActive: {
    backgroundColor: colors.strong_grey,
  },
  viewPhotoButtonInactive: {
    backgroundColor: colors.very_soft_red,
  },
  uploadPhotoButton: {
    width: widthPercentageToDP('37.5%'),
    borderRadius: 5,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%'),
  },
  uploadPhotoButtonActive: {
    backgroundColor: colors.strong_blue,
  },
  uploadPhotoButtonInactive: {
    backgroundColor: colors.very_soft_blue,
  },
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
  },
  containerAccordian: {
    flex: 1,
    paddingTop: 0,
    backgroundColor: 'white',
    borderColor: colors.light_sapphire_bluish_gray,
    borderWidth: 2,
    borderRadius: 10,
    paddingHorizontal: 16,
    marginTop: 16,
  },
  titleAccordian: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  headerAccordian: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
});

const mapStateToProps = ({AppraisalData}) => ({AppraisalData});

export default connect(mapStateToProps)(UploadModalCleaningDetail);
