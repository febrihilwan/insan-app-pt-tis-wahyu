import axios from 'axios';
import store from '../store';
import {API_URL} from 'react-native-dotenv';

// const API_URL = 'http://demo-kota.com/insan/public/api';

const selectAuthToken = (state) => state.Auth.token;

let instance = axios.create({
  baseURL: API_URL,
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json',
  },
});

store.subscribe(() => {
  token = selectAuthToken(store.getState());
  instance.defaults.headers.common['Authorization'] = token;
});

export default instance;
