import React from 'react';
import {Platform, UIManager} from 'react-native';

function useEnableExperimental() {
  React.useEffect(() => {
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  });
  return null;
}

export default useEnableExperimental;
