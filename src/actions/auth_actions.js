import axios from 'axios';
import {AsyncStorage} from 'react-native';
import {API_URL} from 'react-native-dotenv';

const doAuth = (reqBody) => async (dispatch) => {
  dispatch(authOnRequest());
  try {
    const login_url = `${API_URL}/login`;
    const request = await axios.post(login_url, reqBody);
    // console.log('request login', request);
    const stringResult = JSON.stringify(request.data);
    const store = await AsyncStorage.setItem('userData', stringResult);
    return dispatch(authRequestSuccess(request.data));
  } catch (err) {
    console.log(err);
    // console.log(JSON.stringify(err));
    return dispatch(authRequestFailed(err));
  }
};

const authLocalJWT = (loginData) => async (dispatch) => {
  dispatch(authOnRequest());
  try {
    console.log('loginData.token', loginData.api_token.api_token);
    if (loginData.api_token.api_token === undefined) {
      throw new Error('No token in local storage');
    }
    // const request = await axios.get(Config.API_URL + "/user/verify", {
    //   headers: { authorization: loginData.token },
    // });
    // if (request.data.success) {
    return dispatch(authRequestSuccess(loginData));
    // }
    // throw new Error("Invalid token");
  } catch (err) {
    return dispatch(authRequestFailed(err));
  }
};

const authLogout = () => {
  return {type: 'AUTH_LOGOUT'};
};

const authOnRequest = () => {
  return {type: 'AUTH_REQUEST'};
};

const authRequestFailed = (err) => ({
  type: 'AUTH_REQUEST_FAILED',
  payload: err,
});

const authRequestSuccess = (payload) => ({
  type: 'AUTH_REQUEST_SUCCESS',
  payload,
});

export {doAuth, authLocalJWT, authLogout};
