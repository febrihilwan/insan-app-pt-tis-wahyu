import axios from '../services/axios';
// [START] REQUEST LIST PDI
const onRequestListPDI = () => ({
  type: 'LIST_PDI_REQUEST',
});

const finishRequestListPDI = (payload) => ({
  type: 'LIST_PDI_REQUEST_SUCCESS',
  payload,
});

const failedRequestListPDI = (error) => ({
  type: 'LIST_PDI_REQUEST_FAILED',
  error,
});

const fetchAllPDI = (userid) => async (dispatch) => {
  dispatch(onRequestListPDI());
  try {
    let request = await axios.get(`/pdi/${userid}/list`);
    return dispatch(finishRequestListPDI(request.data.carInspections));
  } catch (err) {
    return dispatch(failedRequestListPDI(err));
  }
};
// [END] LIST PDI

// [START] REQUEST DETAIL PDI
const onRequestDetailPDI = () => ({
  type: 'DETAIL_PDI_REQUEST',
});

const finishRequestDetailPDI = (payload) => ({
  type: 'DETAIL_PDI_REQUEST_SUCCESS',
  payload,
});

const failedRequestDetailPDI = (error) => ({
  type: 'DETAIL_PDI_REQUEST_FAILED',
  error,
});

const fetchDetailPDI = (id) => async (dispatch) => {
  dispatch(onRequestDetailPDI());
  try {
    let request = await axios.get(`/pdi/${id}`);

    return dispatch(finishRequestDetailPDI(request.data.carInspection));
  } catch (err) {
    // console.log(JSON.stringify(err.message));
    return dispatch(failedRequestDetailPDI(err));
  }
};
// [END] DETAIL PDI

// [START] Update PDI
const onRequestUpdatePDI = () => ({
  type: 'PDI_UPDATE_REQUEST',
});

const finishRequestUpdatePDI = (payload, updateApa) => ({
  type: 'PDI_UPDATE_REQUEST_SUCCESS',
  payload,
  updateApa,
});

const failedRequestUpdatePDI = () => ({
  type: 'PDI_UPDATE_REQUEST_FAILED',
});

const requestUpdatePDI = (payload, type) => async (dispatch) => {
  dispatch(onRequestUpdatePDI());
  try {
    let request = await axios.put(`/pdi/${payload.id}`, payload);
    return dispatch(finishRequestUpdatePDI(request.data.carInspection, type));
  } catch (err) {
    return dispatch(failedRequestUpdatePDI());
  }
};
// [END] Update PDI

// [START] Upload PDI
const onRequestUploadPhotoPDI = () => ({
  type: 'PDI_UPLOAD_REQUEST',
});

const finishRequestUploadPhotoPDI = (payload) => ({
  type: 'PDI_UPLOAD_REQUEST_SUCCESS',
  payload,
});

const failedRequestUploadPhotoPDI = () => ({
  type: 'PDI_UPLOAD_REQUEST_FAILED',
});

const uploadPhotoProgress = (payload) => ({
  type: 'UPLOAD_PHOTO_PROGRESS',
  payload,
});

const requestUploadPhotoPDI = (payload) => (dispatch) => {
  const {Auth} = store.getState();

  dispatch(onRequestUploadPhotoPDI());
};
// [END] Upload PDI

const updatePDIData = (partName, index, data) => {
  return {
    type: 'PDI_UPDATE_DATA',
    payload: {
      partName: partName,
      index: index,
      data: data,
    },
  };
};

// [START] Finished PDI
const onRequestFinishPDI = () => ({
  type: 'PDI_FINISH_REQUEST',
});

const finishPDI = (payload) => ({
  type: 'PDI_FINISH_REQUEST_SUCCESS',
  payload,
});

const failedFinishPDI = () => ({
  type: 'PDI_FINISH_REQUEST_FAILED',
});

const requestFinishPDI = (payload) => async (dispatch) => {
  dispatch(onRequestFinishPDI());
  try {
    let request = await axios.put(`/pdi/${payload.id}/finished`, payload);
    return dispatch(finishPDI(request.data.carInspection));
  } catch (err) {
    return dispatch(failedFinishPDI());
  }
};
//END Finished PDI

export {
  fetchAllPDI,
  fetchDetailPDI,
  requestUpdatePDI,
  requestFinishPDI,
  updatePDIData,
  requestUploadPhotoPDI,
  uploadPhotoProgress,
  finishRequestUploadPhotoPDI,
  failedRequestUploadPhotoPDI,
};
