import axios from '../services/axios';
import store from '../store';
import _ from 'lodash';
import RNFetchBlob from 'rn-fetch-blob';

const onRequestDetailAppraisal = () => ({
  type: 'APPRAISAL_DETAIL_REQUEST',
});

const finishRequestDetailAppraisal = (payload) => ({
  type: 'APPRAILSAL_DETAIL_REQUEST_SUCCESS',
  payload,
});

const deleteSuccessDetailAppraisal = () => ({
  type: 'APRPAISAL_DETAIL_REQUEST_FINISH',
});

const failedRequestDetailAppraisal = () => ({
  type: 'APRAISAL_DETAIL_REQUEST_FAILED',
});

const requestAppraisalDetail = (id) => async (dispatch) => {
  dispatch(onRequestDetailAppraisal());
  try {
    let request = await axios.get(`/appraisal/${id}`);
    return dispatch(finishRequestDetailAppraisal(request.data.appraisal));
  } catch (err) {
    // console.log(JSON.stringify(err));
    return dispatch(failedRequestDetailAppraisal());
  }
};

const onRequestUpdateAppraisal = () => ({
  type: 'APPRAISAL_UPDATE_REQUEST',
});

const finishRequestUpdateAppraisal = (payload, updateApa) => ({
  type: 'APPRAISAL_UPDATE_REQUEST_SUCCESS',
  payload,
  updateApa,
});

const failedRequestUpdateAppraisal = () => ({
  type: 'APPRAISAL_UPDATE_REQUEST_FAILED',
});

const requestUpdateAppraisal = (payload, type) => async (dispatch) => {
  dispatch(onRequestUpdateAppraisal());
  try {
    let request = await axios.put(`/appraisal/${payload.id}`, payload);
    return dispatch(finishRequestUpdateAppraisal(request.data.appraisal, type));
  } catch (err) {
    return dispatch(failedRequestUpdateAppraisal());
  }
};

const onRequestUploadPhotoAppraisal = () => ({
  type: 'APPRAISAL_UPLOAD_REQUEST',
});

const finishRequestUploadPhotoAppraisal = (payload) => ({
  type: 'APPRAISAL_UPLOAD_REQUEST_SUCCESS',
  payload,
});

const failedRequestUploadPhotoAppraisal = () => ({
  type: 'APPRAISAL_UPLOAD_REQUEST_FAILED',
});

const uploadPhotoProgress = (payload) => ({
  type: 'UPLOAD_PHOTO_PROGRESS',
  payload,
});

const requestUploadPhotoAppraisal = (payload) => (dispatch) => {
  const {Auth} = store.getState();

  dispatch(onRequestUploadPhotoAppraisal());

  // RNFetchBlob.fetch(
  //   'POST',
  //   `https://api.autotrust.id/api/v1/appraisal/${payload.id}/upload`,
  //   {
  //     Authorization : `Bearer ${Auth.token}`,
  //     'Content-Type' : 'multipart/form-data',
  //   },
  //   payload.formData
  // ).uploadProgress((written, total) => {
  //   let uploadProgress = Math.round((written * 100) / total);
  //   dispatch(uploadPhotoProgress({ uploadProgress }));
  // }).then(result => {
  //     result = result.json()
  //     const {AppraisalDetail} = store.getState()
  //     let docs
  //     if(result.field == "photoDocument"){
  //       docs = _.find(AppraisalDetail.photoDocument, {name: result.objName})
  //     } else if(result.field == "photoFrame"){
  //       docs = _.find(AppraisalDetail.photoFrame, {name: result.objName})
  //     } else if(result.field == "photoEngine"){
  //       docs = _.find(AppraisalDetail.photoEngine, {name: result.objName})
  //     } else if(result.field == "photoInterior"){
  //       docs = _.find(AppraisalDetail.photoInterior, {name: result.objName})
  //     } else if(result.field == "photoExterior"){
  //       docs = _.find(AppraisalDetail.photoExterior, {name: result.objName})
  //     }

  //     docs.imageUrl = result.imageUrl
  //     return dispatch(finishRequestUploadPhotoAppraisal(result.appraisal));
  //   })
  //   .catch(err => {
  //     console.log("ERR ", err);
  //     dispatch(failedRequestUploadPhotoAppraisal());
  //   });
  // console.log("PAYLOAD NYAA ", Auth.token)
  // axios({
  //   method: "POST",
  //   url: `/appraisal/${payload.id}/upload`,
  //   timeout: 60 * 3 * 1000,
  //   data: payload.formData,
  //   headers: {
  //     'Content-Type': 'multipart/form-data',
  //   },
  //   onUploadProgress: progressEvent => {
  //     let uploadProgress = Math.round((progressEvent.loaded * 100) / progressEvent.total);
  //     dispatch(uploadPhotoProgress({ uploadProgress }));
  //   },
  // })
  //   .then(result => {
  //     const {AppraisalDetail} = store.getState()
  //     let docs
  //     if(result.data.field == "photoDocument"){
  //       docs = _.find(AppraisalDetail.photoDocument, {name: result.data.objName})
  //     } else if(result.data.field == "photoFrame"){
  //       docs = _.find(AppraisalDetail.photoFrame, {name: result.data.objName})
  //     } else if(result.data.field == "photoEngine"){
  //       docs = _.find(AppraisalDetail.photoEngine, {name: result.data.objName})
  //     } else if(result.data.field == "photoInterior"){
  //       docs = _.find(AppraisalDetail.photoInterior, {name: result.data.objName})
  //     } else if(result.data.field == "photoExterior"){
  //       docs = _.find(AppraisalDetail.photoExterior, {name: result.data.objName})
  //     }

  //     docs.imageUrl = result.data.imageUrl
  //     return dispatch(finishRequestUploadPhotoAppraisal(result.data.appraisal));
  //   })
  //   .catch(err => {
  //     console.log("ERR ", err.request);
  //     dispatch(failedRequestUploadPhotoAppraisal());
  //   });
};

const updateAppraisalData = (partName, index, data) => {
  return {
    type: 'APPRAISAL_UPDATE_DATA',
    payload: {
      partName: partName,
      index: index,
      data: data,
    },
  };
};

const onRequestFinishAppraisal = () => ({
  type: 'APPRAISAL_FINISH_REQUEST',
});

const finishAppraisal = (payload) => ({
  type: 'APPRAISAL_FINISH_REQUEST_SUCCESS',
  payload,
});

const failedFinishAppraisal = () => ({
  type: 'APPRAISAL_FINISH_REQUEST_FAILED',
});

const requestFinishAppraisal = (payload) => async (dispatch) => {
  dispatch(onRequestFinishAppraisal());
  try {
    let request = await axios.put(`/appraisal/${payload.id}/finished`, payload);
    return dispatch(finishAppraisal(request.data.appraisal));
  } catch (err) {
    return dispatch(failedFinishAppraisal());
  }
};

export {
  requestAppraisalDetail,
  requestUpdateAppraisal,
  requestUploadPhotoAppraisal,
  updateAppraisalData,
  requestFinishAppraisal,
  deleteSuccessDetailAppraisal,
  uploadPhotoProgress,
  finishRequestUploadPhotoAppraisal,
  failedRequestUploadPhotoAppraisal,
};
