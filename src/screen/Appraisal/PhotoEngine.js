import React, { Component } from "react";
import _ from 'lodash'
import { connect } from "react-redux";
import { setApprasialData, getData } from '../../reducers/ApprasialData'
import { View, FlatList, ToastAndroid, StyleSheet } from "react-native";
import { requestUploadPhotoAppraisal } from "../../actions/appraisal_actions";
import Spinner from "react-native-loading-spinner-overlay";

import UploadModal from "../../component/Modal/UploadModal";
import PreviewImage from "../../component/Modal/PreviewImage";
import { widthPercentageToDP, heightPercentageToDP } from "../../utils";
import InputEngine from "../../component/InputEngine";

class PhotoEngine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewImage: false,
      photoData: [],
      displayPhotoModal: false,
      document: {},
    };
    this._onPressPhoto = this._onPressPhoto.bind(this);
    this._togglePhotoModal = this._togglePhotoModal.bind(this);
    this._onChangeText = this._onChangeText.bind(this);
    this._onPressUpload = this._onPressUpload.bind(this);
    this._showImage = this._showImage.bind(this);
    this._closeImage = this._closeImage.bind(this);
  }

  _onPressPhoto(docData) {
    this.setState({
      displayPhotoModal: true,
      document: docData,
    });
  }

  _togglePhotoModal() {
    this.setState({
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _showImage(imageUrl) {
    console.log("open image", imageUrl);
    this.setState({
      imageLink: imageUrl,
      viewImage: !this.state.viewImage,
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _closeImage() {
    console.log("close image");
    this.setState({
      viewImage: !this.state.viewImage,
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _onPressUpload(imageData) {
    const { AppraisalDetail, requestUploadPhoto, AppraisalData } = this.props;
    let formData = new FormData();
    formData.append("image", imageData);
    formData.append("field", "photoEngine");
    formData.append("objectField", JSON.stringify(this.state.document));
    console.log(formData);
    let {data} = AppraisalData
    let isExistData = _.find(data, {objectField: {name: this.state.document.name}})

    if(isExistData){
      let idxExistData = _.find(data, {objectField: {name: this.state.document.name}})
      data.splice(idxExistData, 1)
    }
    let newData = {
      image: imageData,
      field: "photoEngine",
      objectField: this.state.document
    }
    data.push(newData)
    setApprasialData(data);
    let docs = _.find(AppraisalDetail.photoEngine, {name: this.state.document.name})
    docs.imageUrl = imageData.uri
    // requestUploadPhoto({ id: AppraisalDetail.id, formData });

    this.setState({
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _onChangeText(text, name) {
    const { photoData, document } = this.state;
    this.setState({
      photoData: photoData.map(photo => (photo.name === name ? { ...photo, notes: text } : photo)),
      document: { ...document, notes: text },
    });
  }

  componentDidMount() {
    this.setState({
      photoData: this.props.AppraisalDetail.photoEngine,
    });
  }

  componentDidUpdate(prevProps) {
    const { AppraisalDetail } = this.props;
    if (!prevProps.AppraisalDetail.message && !!AppraisalDetail.message) {
      ToastAndroid.showWithGravity(`${AppraisalDetail.message}`, 3000, ToastAndroid.CENTER);
    }
  }

  render() {
    const { displayPhotoModal, document } = this.state;
    const { AppraisalDetail } = this.props;
    return (
      <View>
        <View style={styles.containerModal}>
          <UploadModal
            document={document}
            togglePhotoModal={this._togglePhotoModal}
            displayPhotoModal={displayPhotoModal}
            onChangeText={this._onChangeText}
            onPressUpload={this._onPressUpload}
            previewPhoto={this._showImage}
          />
        </View>

        <View style={styles.containerModal}>
          <PreviewImage
            closeImage={this._closeImage}
            viewImage={this.state.viewImage}
            imageLink={this.state.imageLink}
          />
        </View>

        <Spinner
          visible={AppraisalDetail.uploadProgress == 100 ? false : AppraisalDetail.spinner}
          textContent={`Uploading... ${AppraisalDetail.uploadProgress}  %`}
          textStyle={{ color: "white" }}
        />

        <FlatList
          renderItem={({ item, index }) => (
            <InputEngine onPressPhoto={() => this._onPressPhoto(item)} key={index} photo={item} index={index} />
          )}
          data={this.props.AppraisalDetail.photoEngine}
          keyExtractor={item => item.name}
          extraData={this.state}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  containerModal: {
    flex: 1,
  },
  modalContainer: {
    // height: heightPercentageToDP("50%"),
    backgroundColor: "#fff",
  },
  modalHeader: {
    height: heightPercentageToDP("7%"),
    backgroundColor: "#4682B4",
    justifyContent: "center",
    padding: 10,
  },
  modalContent: {
    paddingHorizontal: widthPercentageToDP("5%"),
    paddingVertical: heightPercentageToDP("1.5%"),
  },
  modalPhotoName: {
    fontSize: 24,
    fontWeight: "600",
  },
  modalImagePicker: {
    height: heightPercentageToDP("20%"),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#778899",
  },
  modalNotesInput: {
    borderWidth: 1,
    borderColor: "black",
    textAlignVertical: "top",
  },
});

const mapStateToProps = ({ AppraisalDetail, AppraisalData }) => ({ AppraisalDetail, AppraisalData });
const mapDispatchToProps = dispatch => {
  return {
    requestUploadPhoto: payload => {
      dispatch(requestUploadPhotoAppraisal(payload));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PhotoEngine);
