import React from "react";
import { Text, TouchableOpacity } from "react-native";
import {createAppContainer} from 'react-navigation'
import {createMaterialTopTabNavigator} from 'react-navigation-tabs'
import {createStackNavigator} from 'react-navigation-stack'

import ButtonSave from "./ButtonSave";
import Colors from '../../utils/colors'
import { widthPercentageToDP } from '../../utils'

import PhotoDoc from "./PhotoDoc";
import PhotoEngine from "./PhotoEngine";
import PhotoFrame from "./PhotoFrame";
import PhotoExterior from "./PhotoExterior";
import PhotoInterior from "./PhotoInterior";
import Summary from "./Summary";
import InputCarDetail from './InputCarDetail'

const PhotoAppraisal = createMaterialTopTabNavigator(
  {
    Document: PhotoDoc,
    Frame: PhotoFrame,
    Engine: PhotoEngine,
    Interior: PhotoInterior,
    Exterior: PhotoExterior,
    Summary: Summary
  },
  {
    lazy: true,
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: Colors.strong_blue,
      inactiveTintColor: Colors.dope_blue,
      style: {
          backgroundColor: 'white'
      },
      indicatorStyle: {
          backgroundColor: Colors.strong_blue,
          height: 4
      },
      labelStyle:{
        fontSize: widthPercentageToDP('3.5%')
      },
      scrollEnabled: true,
      tabStyle: {
        paddingHorizontal: 0,
        width: widthPercentageToDP('25%')
      }
  }
  },
);

PhotoAppraisal.navigationOptions = ({ navigation }) => ({
  title: " Photo Appraisal",
  headerRight: <ButtonSave navigation={navigation} />,
  headerStyle: {
    backgroundColor: Colors.strong_blue
  },
  headerTintColor: 'white'
});

const Appraisal = createStackNavigator(
  {
    CarDetail: {
      screen: InputCarDetail,
      navigationOptions: ({ navigation }) => ({
        header: null
      })
    },
    PhotoAppraisal: {
      screen: PhotoAppraisal
    },
  },
  {
    initialRouteName: "PhotoAppraisal",
  },
);

export default Appraisal;
