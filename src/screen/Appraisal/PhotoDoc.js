import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, FlatList, ToastAndroid, AsyncStorage} from 'react-native';
import _ from 'lodash';
import {requestUploadPhotoAppraisal} from '../../actions/appraisal_actions';
import {setApprasialData, getData} from '../../reducers/ApprasialData';
import Spinner from 'react-native-loading-spinner-overlay';
import RNFetchBlob from 'rn-fetch-blob';

import UploadModal from '../../component/Modal/UploadModal';
import PreviewImage from '../../component/Modal/PreviewImage';
import {widthPercentageToDP, heightPercentageToDP} from '../../utils';
import InputPhoto from '../../component/InputPhoto';

class PhotoDoc extends Component {
  constructor(props) {
    super(props);
    this._onPressPhoto = this._onPressPhoto.bind(this);
    this.state = {
      viewImage: false,
      photoData: [],
      displayPhotoModal: false,
      document: {},
      modalVisible: false,
    };
    this._togglePhotoModal = this._togglePhotoModal.bind(this);
    this._onChangeText = this._onChangeText.bind(this);
    this._onPressUpload = this._onPressUpload.bind(this);
    this._showImage = this._showImage.bind(this);
    this._closeImage = this._closeImage.bind(this);
  }

  _onPressPhoto(docData) {
    console.log('itaem', docData);

    this.setState({
      displayPhotoModal: true,
      document: docData,
    });
  }

  _togglePhotoModal() {
    this.setState({
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _showImage(imageUrl) {
    console.log('open image', imageUrl);
    this.setState({
      imageLink: imageUrl,
      viewImage: !this.state.viewImage,
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _closeImage() {
    console.log('close image');
    this.setState({
      viewImage: !this.state.viewImage,
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _onPressUpload(imageData) {
    const {
      AppraisalDetail,
      Auth,
      requestUploadPhoto,
      AppraisalData,
      dispatch,
    } = this.props;
    // imageData.uri = RNFetchBlob.wrap(imageData.uri)
    // console.log("IMAGE", imageData)
    // return
    let formData = [];
    // let formData = new FormData();
    formData.push({
      name: 'image',
      filename: imageData.name,
      type: imageData.type,
      data: RNFetchBlob.wrap(imageData.path),
    });
    formData.push({name: 'field', data: 'photoDocument'});
    formData.push({
      name: 'objectField',
      data: JSON.stringify(this.state.document),
    });
    console.log('DATANYA FORM ', formData);

    let {data} = AppraisalData;
    let isExistData = _.find(data, {
      objectField: {name: this.state.document.name},
    });

    if (isExistData) {
      let idxExistData = _.find(data, {
        objectField: {name: this.state.document.name},
      });
      data.splice(idxExistData, 1);
    }
    let newData = {
      image: imageData,
      field: 'photoDocument',
      objectField: this.state.document,
    };
    data.push(newData);
    setApprasialData(data);
    let docs = _.find(AppraisalDetail.photoDocument, {
      name: this.state.document.name,
    });
    docs.imageUrl = imageData.uri;

    // requestUploadPhoto({ id: AppraisalDetail.id, formData });

    this.setState({
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _onChangeText(text, name) {
    const {photoData, document} = this.state;
    this.setState({
      photoData: photoData.map((photo) =>
        photo.name === name ? {...photo, notes: text} : photo,
      ),
      document: {...document, notes: text},
    });
  }

  componentDidMount() {
    this.setState({
      photoData: this.props.AppraisalDetail.photoDocument,
    });
    console.log('++');
    console.log('Data', this.props.AppraisalData);
  }

  componentDidUpdate(prevProps) {
    console.log('====================================');
    console.log(this.state.document);
    console.log('Data', this.props.AppraisalData);
    console.log('====================================');
    const {AppraisalDetail} = this.props;
    if (!prevProps.AppraisalDetail.message && !!AppraisalDetail.message) {
      ToastAndroid.showWithGravity(
        `${AppraisalDetail.message}`,
        3000,
        ToastAndroid.CENTER,
      );
    }
  }

  render() {
    const {displayPhotoModal, document, photoData} = this.state;
    const {AppraisalDetail, AppraisalData} = this.props;
    console.log('detail', AppraisalDetail);
    return (
      <View>
        <View style={styles.containerModal}>
          {displayPhotoModal ? (
            <UploadModal
              document={document}
              togglePhotoModal={this._togglePhotoModal}
              displayPhotoModal={displayPhotoModal}
              onChangeText={this._onChangeText}
              onPressUpload={this._onPressUpload}
              previewPhoto={this._showImage}
            />
          ) : (
            false
          )}
        </View>

        <View style={styles.containerModal}>
          <PreviewImage
            closeImage={this._closeImage}
            viewImage={this.state.viewImage}
            imageLink={this.state.imageLink}
          />
        </View>

        <Spinner
          visible={
            AppraisalDetail.uploadProgress == 100
              ? false
              : AppraisalDetail.spinner
          }
          textContent={`Uploading... ${AppraisalDetail.uploadProgress}  %`}
          textStyle={{color: 'white'}}
        />

        <View
          style={{
            paddingLeft: widthPercentageToDP('4%'),
            backgroundColor: 'white',
          }}>
          <FlatList
            renderItem={({item, index}) => {
              return (
                <InputPhoto
                  onPressPhoto={() => this._onPressPhoto(item)}
                  key={index}
                  photo={item}
                />
              );
            }}
            data={AppraisalDetail.photoDocument}
            keyExtractor={(item) => item.name}
            extraData={this.state}
          />
        </View>
      </View>
    );
  }
}

const styles = {
  flex1: {
    flex: 1,
  },
  containerModal: {
    flex: 1,
  },
  modalContainer: {
    // height: heightPercentageToDP("50%"),
    backgroundColor: '#fff',
  },
  modalHeader: {
    height: heightPercentageToDP('7%'),
    backgroundColor: '#4682B4',
    justifyContent: 'center',
    padding: 10,
  },
  modalContent: {
    paddingHorizontal: widthPercentageToDP('5%'),
    paddingVertical: heightPercentageToDP('1.5%'),
  },
  modalPhotoName: {
    fontSize: 24,
    fontWeight: '600',
  },
  modalImagePicker: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#778899',
  },
  modalNotesInput: {
    borderWidth: 1,
    borderColor: 'black',
    textAlignVertical: 'top',
  },
  closeBtn: {
    alignSelf: 'flex-end',
    marginBottom: 10,
    padding: 10,
  },
};

const mapStateToProps = ({AppraisalDetail, Auth, AppraisalData}) => ({
  Auth,
  AppraisalDetail,
  AppraisalData,
});
const mapDispatchToProps = (dispatch) => {
  return {
    requestUploadPhoto: (payload) => {
      dispatch(requestUploadPhotoAppraisal(payload));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PhotoDoc);
