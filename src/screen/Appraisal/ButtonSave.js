import React, {Component} from 'react';
import {Text, TouchableOpacity, Alert, ToastAndroid} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';
import {requestUpdateAppraisal, requestFinishAppraisal} from '../../actions';
import {
  requestUploadPhotoAppraisal,
  uploadPhotoProgress,
  finishRequestUploadPhotoAppraisal,
  failedRequestUploadPhotoAppraisal,
} from '../../actions/appraisal_actions';
import {bindActionCreators} from 'redux';
import {
  resetApprasialData,
  setApprasialData,
} from '../../reducers/ApprasialData';
import RNFetchBlob from 'rn-fetch-blob';
import store from '../../store';

const requiredDetail = [
  'Nomor Polisi',
  'Pemilik Kendaraan',
  'No. HP Customer',
  'Nomor Mesin',
  'Nomor Rangka',
  'Jarak Tempuh',
  'STNK 5 Tahunan',
  'Tahun STNK',
  'Tahun',
];

const requiredSummary = ['Transmisi', 'Kategori Warna', 'Nilai STNK'];

class ButtonSave extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  uploadImages = (id, formData) =>
    new Promise((resolve, reject) => {
      const {Auth} = store.getState();
      RNFetchBlob.fetch(
        'POST',
        `https://api.autotrust.id/api/v1/appraisal/${id}/upload`,
        {
          Authorization: `Bearer ${Auth.token}`,
          'Content-Type': 'multipart/form-data',
        },
        formData,
      )
        .uploadProgress((written, total) => {
          let uploadProgress = Math.round((written * 100) / total);
          this.props.uploadPhotoProgress({uploadProgress});
        })
        .then((result) => {
          result = result.json();
          const {AppraisalDetail} = store.getState();
          let docs;
          if (result.field == 'photoDocument') {
            docs = _.find(AppraisalDetail.photoDocument, {
              name: result.objName,
            });
          } else if (result.field == 'photoFrame') {
            docs = _.find(AppraisalDetail.photoFrame, {name: result.objName});
          } else if (result.field == 'photoEngine') {
            docs = _.find(AppraisalDetail.photoEngine, {name: result.objName});
          } else if (result.field == 'photoInterior') {
            docs = _.find(AppraisalDetail.photoInterior, {
              name: result.objName,
            });
          } else if (result.field == 'photoExterior') {
            docs = _.find(AppraisalDetail.photoExterior, {
              name: result.objName,
            });
          }

          docs.imageUrl = result.imageUrl;
          this.props.finishRequestUploadPhotoAppraisal(result.appraisal);
          resolve();
        })
        .catch((err) => {
          // console.log("ERR ", err);
          reject();
          this.props.failedRequestUploadPhotoAppraisal();
        });
    });

  async onSaveToDraft() {
    const {AppraisalData} = this.props;

    if (AppraisalData) {
      if (AppraisalData.data.length > 0) {
        let promises = [];
        await AppraisalData.data.map((item, idx) => {
          let formData = [];

          formData.push({
            name: 'image',
            filename: item.image.name,
            type: item.image.type,
            data: RNFetchBlob.wrap(item.image.path),
          });
          formData.push({name: 'field', data: item.field});
          formData.push({
            name: 'objectField',
            data: JSON.stringify(item.objectField),
          });

          promises.push(
            this.uploadImages(this.props.AppraisalDetail.id, formData),
          );
        });
        Promise.all(promises)
          .then((result) => {
            let {data} = AppraisalData;
            while (data.length > 0) {
              data.pop();
            }

            this.props.requestUpdateAppraisal(
              this.props.AppraisalDetail,
              'saveDraft',
            );
            this.props.navigation.navigate('AppraisalList');
          })
          .catch((e) => {
            console.log('ERROR PROMISE', e);
          });
      } else {
        this.props.requestUpdateAppraisal(
          this.props.AppraisalDetail,
          'saveDraft',
        );
        this.props.navigation.navigate('AppraisalList');
      }
    } else {
      this.props.requestUpdateAppraisal(
        this.props.AppraisalDetail,
        'saveDraft',
      );
      this.props.navigation.navigate('AppraisalList');
    }
  }

  async onFinishedAppraisal() {
    const {AppraisalData, AppraisalDetail} = this.props;

    let validDetail = false;
    let validSummary = false;

    await Promise.all(
      AppraisalDetail.carDetail.some(function (item, i) {
        if (requiredDetail.includes(item.fieldName)) {
          // console.log("Ada",item)
          if (item.value == '') {
            alert(`Harap isi data ${item.fieldName}`);
            validDetail = false;
            return true;
          }

          validDetail = true;
          return false;
        }
      }),
    );

    if (validDetail) {
      let valBrand = '';
      await Promise.all(
        AppraisalDetail.carSummary.some(function (item, i) {
          if (item.fieldName == 'Brand') {
            valBrand = item.value;
          }
          if (requiredSummary.includes(item.fieldName)) {
            if (item.value == '') {
              alert(`Harap isi data ${item.fieldName}`);
              validSummary = false;
              return true;
            }

            validSummary = true;
            return false;
          } else if (item.fieldName == 'Model' || item.fieldName == 'Tipe') {
            if (valBrand != '') {
              if (item.value == '') {
                alert(`Harap isi data ${item.fieldName}`);
                validSummary = false;
                return true;
              }
            }
          } else if (item.fieldName == 'Other') {
            if (valBrand == '') {
              if (item.value == '') {
                alert(`Harap isi data ${item.fieldName}`);
                validSummary = false;
                return true;
              }
            }
          }
        }),
      );
    }

    if (validSummary) {
      // alert("YESS")
      if (AppraisalData) {
        if (AppraisalData.data.length > 0) {
          let promises = [];
          await AppraisalData.data.map((item, idx) => {
            let formData = [];

            formData.push({
              name: 'image',
              filename: item.image.name,
              type: item.image.type,
              data: RNFetchBlob.wrap(item.image.path),
            });
            formData.push({name: 'field', data: item.field});
            formData.push({
              name: 'objectField',
              data: JSON.stringify(item.objectField),
            });

            promises.push(
              this.uploadImages(this.props.AppraisalDetail.id, formData),
            );
          });

          Promise.all(promises)
            .then((result) => {
              let {data} = AppraisalData;
              while (data.length > 0) {
                data.pop();
              }

              this.props.requestFinishAppraisal(this.props.AppraisalDetail);
              this.props.navigation.navigate('AppraisalList');
              ToastAndroid.showWithGravity(
                'Berhasil menyelesaikan appraisal',
                2000,
                ToastAndroid.CENTER,
              );
            })
            .catch((e) => {
              console.log('ERROR PROMISE', e);
            });
        } else {
          this.props.requestFinishAppraisal(this.props.AppraisalDetail);
          this.props.navigation.navigate('AppraisalList');
          ToastAndroid.showWithGravity(
            'Berhasil menyelesaikan appraisal',
            2000,
            ToastAndroid.CENTER,
          );
        }
      } else {
        this.props.requestFinishAppraisal(this.props.AppraisalDetail);
        this.props.navigation.navigate('AppraisalList');
        ToastAndroid.showWithGravity(
          'Berhasil menyelesaikan appraisal',
          2000,
          ToastAndroid.CENTER,
        );
      }
    }
  }

  SaveToDraft = (navigation) => {
    console.log('PROP', this.props.AppraisalDetail);
    Alert.alert(
      'Save Data',
      'Successfully Save to Draft',
      [
        {text: 'Cancel', onPress: () => console.log('cancel save data')},
        {text: 'Save to draft', onPress: () => this.onSaveToDraft()},
        {text: 'Finish Appraisal', onPress: () => this.onFinishedAppraisal()},
      ],
      {cancelable: false},
    );
  };

  render() {
    return (
      <TouchableOpacity
        style={{marginRight: 30}}
        onPress={() => this.SaveToDraft()}>
        <Text style={{fontSize: 18, fontWeight: 'bold', color: 'white'}}>
          Simpan
        </Text>
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = ({AppraisalDetail, AppraisalData}) => ({
  AppraisalDetail,
  AppraisalData,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      requestFinishAppraisal,
      requestUpdateAppraisal,
      requestUploadPhotoAppraisal,
      uploadPhotoProgress,
      finishRequestUploadPhotoAppraisal,
      failedRequestUploadPhotoAppraisal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(ButtonSave);
