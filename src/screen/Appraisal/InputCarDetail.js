import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {HeaderBackButton} from 'react-navigation-stack'
import Datepicker from "react-native-datepicker";
import { Text, View, Button, ScrollView, TextInput, ToastAndroid, Modal, StyleSheet, TouchableOpacity } from "react-native";
import { requestUpdateAppraisal } from "../../actions";
import { widthPercentageToDP, heightPercentageToDP } from "../../utils";
import Colors from '../../utils/colors'
import colors from "../../utils/colors";

class InputCarDetail extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: () => <HeaderBackButton onPress={() => navigation.navigate("AppraisalList")} />,
    title: "Input Detail Mobil",
  });

  state = {
    carDetail: [],
  };

  componentDidMount() {
    this.setState({
      carDetail: this.props.AppraisalDetail.carDetail,
    });
  }

  onChangeField(index, text) {
    this.setState({
      carDetail: this.state.carDetail.map((detail, idx) => {
        if (index !== idx) {
          return detail;
        } else {
          detail.value = text;
          return {
            ...detail,
            value: text,
          };
        }
      }),
    });
  }

  onPressSubmit() {
    const { AppraisalDetail } = this.props;
    const { carDetail } = this.state;
    const requiredField = carDetail.filter(
      field => field.fieldName !== "E-mail Customer",
    );

    const emptyValue = requiredField.find(field => field.value === "");

    if (!emptyValue) {
      this.props.requestUpdateAppraisal({ ...AppraisalDetail, carDetail: carDetail }, "inputCar");
    } else {
      ToastAndroid.showWithGravity(`${emptyValue.fieldName} wajib diisi`, 2000, ToastAndroid.CENTER);
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.AppraisalDetail.onRequestUpdateAppraisal) {
      if (this.props.AppraisalDetail.updateApa === "inputCar") {
        this.props.navigation.navigate("PhotoAppraisal");
      } else if (this.props.AppraisalDetail.failedInputCarDetail) {
        console.log(this.props.AppraisalDetail);
        ToastAndroid.showWithGravity("Gagal input detail mobil", 2000, ToastAndroid.BOTTOM);
      }
    }
  }

  render() {
    const { carDetail } = this.state;
    return (
      <React.Fragment>
        <View style={styles.headerContainer}>
          <HeaderBackButton tintColor='white' onPress={() => this.props.navigation.navigate("AppraisalList")} />
          <Text style={styles.headerTitle}>Input Detail Mobil</Text>
          <TouchableOpacity onPress={() => this.onPressSubmit()}>
            <Text style={{...styles.headerTitle, fontSize: widthPercentageToDP('4%'), marginRight: 10}}>Simpan</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.subheader}>
          <Text style={{color: 'white', fontSize: widthPercentageToDP('4%'), fontFamily: 'Roboto', fontWeight: 'bold'}}>Bagian Bertanda ( * ) Wajib Diisi</Text>
        </View>
        <ScrollView>
          <View style={styles.container}>
            {carDetail.map((detail, index) => (
              <View style={styles.inputGroup} key={index}>
                {detail.fieldName === "E-mail Customer" ? (
                  <Text style={styles.inputLabel}>{`${detail.fieldName} (Opsional)`}</Text>
                ) : (
                  <Text style={styles.inputLabel}>{detail.fieldName} *</Text>
                )}
                {["Tahun STNK", "STNK 5 Tahunan"].includes(detail.fieldName) ? (
                  <Datepicker
                    style={styles.inputForm}
                    mode="date"
                    date={detail.value}
                    placeholder=""
                    androidMode="spinner"
                    confirmBtnText="Oke"
                    cancelBtnText="Batal"
                    onDateChange={date => this.onChangeField(index, date)}
                    format="YYYY-MM-DD"
                    showIcon={false}
                    customStyles={{
                      dateInput: {
                        borderWidth: 0,
                        textAlign: "left",
                        alignItems: "flex-start",
                        paddingLeft: 3,
                      },
                    }}
                  />
                ) : (
                  <View>
                    <TextInput
                      autoCapitalize="words"
                      style={styles.inputForm}
                      keyboardType={["Tahun", "Jarak Tempuh"].includes(detail.fieldName) ? "number-pad" : "default"}
                      onChangeText={text => this.onChangeField(index, text)}
                      defaultValue={carDetail[index]["value"]}
                      returnKeyType="next"
                    />
                    {!!this.state.nameError && <Text style={{ color: red }}>{this.state.nameError}</Text>}
                  </View>
                )}
              </View>
            ))}
            {/* <Button title="Save Car Detail" onPress={() => this.onPressSubmit()} /> */}
          
        </View></ScrollView>
      </React.Fragment>
      
    );
  }
}

const styles = StyleSheet.create ({
  headerContainer: {
    paddingVertical: widthPercentageToDP('0.6%'),
    width: widthPercentageToDP('100%'),
    paddingHorizontal: widthPercentageToDP('1.5%'),
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: Colors.strong_blue,
    alignItems: 'center'
  },
  headerTitle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: widthPercentageToDP('4.5%'),
    fontFamily: 'Roboto',
  },
  subheader: {
    backgroundColor: colors.bright_red,
    paddingVertical: widthPercentageToDP('3%'),
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    paddingHorizontal: widthPercentageToDP('5%'),
    paddingVertical: widthPercentageToDP('5%'),
    backgroundColor: "#fff",
  },
  title: {
    fontSize: 20,
    fontWeight: "600",
    color: "#000",
    fontFamily: 'Roboto',
  },
  inputGroup: {
    marginBottom: 5,
  },
  inputForm: {
    width: widthPercentageToDP('90%'),
    borderWidth: 1.5,
    borderColor: Colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%')
  },
  inputLabel: {
    color: Colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: widthPercentageToDP('1%'),
    fontFamily: 'Roboto',
  }
});

const mapStateToProps = ({ Auth, AppraisalDetail }) => ({ Auth, AppraisalDetail });
const mapDispatchToProps = dispatch => bindActionCreators({ requestUpdateAppraisal }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InputCarDetail);
