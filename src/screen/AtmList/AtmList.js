import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  View,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  ToastAndroid,
  Linking,
  StyleSheet,
  Button,
  SafeAreaView,
  RefreshControl,
} from 'react-native';
import moment from 'moment';
import 'moment/locale/id';
import {
  resetApprasialData,
  setApprasialData,
} from '../../reducers/ApprasialData';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../utils/colors';
import html_script from '../../utils/html_script';

import {heightPercentageToDP, widthPercentageToDP} from '../../utils';
// import {requestAppraisalDetail} from '../../actions';
import axios from '../../services/axios';
import logo from '../../assets/image/logo-trust.png';
moment.locale('id');
import {WebView} from 'react-native-webview';
import SearchInput, {createFilter} from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['bank.name_bank', 'name_address'];
import Spinner from 'react-native-loading-spinner-overlay';

const BookingList = [
  {
    Booking: {
      noBooking: 'TR-092018-42',
      carType: 'AGYA',
      bookingTime: moment(),
    },
    status: 'Terjadwal',
    action: 'Go',
  },
  {
    Booking: {
      noBooking: 'TR-092018-43',
      carType: 'Alphard',
      bookingTime: moment(),
    },
    status: 'Terjadwal',
    action: 'Go',
  },
  {
    Booking: {
      noBooking: 'TR-092018-44',
      carType: 'Avanza',
      bookingTime: moment(),
    },
    status: 'Terjadwal',
    action: 'Go',
  },
  {
    Booking: {
      noBooking: 'TR-092018-45',
      carType: 'Fortuner',
      bookingTime: moment(),
    },
    status: 'Terjadwal',
    action: 'Go',
  },
];

class AtmList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ticketList: [],
      searchValue: '',
      refreshing: false,
    };
    // this.navigationWillFocusListener = props.navigation.addListener(
    //   'willFocus',
    //   () => {
    //     this.fetchAppraisalList();
    //   },
    // );
  }

  // componentWillUnmount() {
  //   this.navigationWillFocusListener.remove();
  //   setApprasialData([]);
  // }

  componentDidMount() {
    // console.log('DATA ==', this.props.AppraisalData);
    this.fetchTicketList();
  }

  componentDidUpdate(prevProps) {
    // const {AppraisalDetail} = this.props;
    // if (prevProps.AppraisalDetail.onRequestDetailAppraisal) {
    //   if (AppraisalDetail.successDetailAppraisal) {
    //     this.props.navigation.navigate('Appraisal');
    //   } else if (AppraisalDetail.errorDetailAppraisal) {
    //     ToastAndroid.showWithGravity(
    //       'Gagal menampilkan detail appraisal',
    //       2000,
    //       ToastAndroid.BOTTOM,
    //     );
    //   } else {
    //     console.log('Something else ?', AppraisalDetail);
    //   }
    // }
    // this.fetchTicketList();
  }

  fetchTicketList() {
    this.setState({
      spinner: true,
    });
    // console.log('this.props.Auth', this.props.Auth.api_token);
    const {api_token} = this.props.Auth.api_token;

    axios
      .get(`/machine`, {
        headers: {
          Authorization: `Bearer ${api_token}`,
        },
      })
      .then((result) => {
        console.log('atm list', result.data);
        // let filterResult = result.data.appraisalList.filter(
        //   (data) => data.status !== 'FINISHED',
        // );
        // console.log(filterResult);
        this.setState({
          ticketList: result.data,
          spinner: false,
        });
      })
      .catch((err) => {
        console.log('ini error', err.response);
        // ToastAndroid.showWithGravity(
        //   'Gagal menampilkan list ticket',
        //   2000,
        //   ToastAndroid.BOTTOM,
        // );
        this.setState({
          spinner: false,
        });
      });
  }

  searchUpdated(value) {
    this.setState({searchValue: value});
  }

  confirmWhatsapp = (appraisalId, appraisalMisc) => {
    appraisalMisc.whatsappConfirmation.status = true;
    appraisalMisc.whatsappConfirmation.timestamp = new Date();

    console.log(appraisalMisc);
    axios
      .post(`appraisal/${appraisalId}/whatsapp-confirmation`, appraisalMisc)
      .then((appraisal) => {
        console.log(appraisal.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  _goToMyPosition = (lat, lon) => {
    // console.log('lat long', lat, lon);
    this.refs['Map_Ref'].injectJavaScript(`
      mymap.setView([${lat}, ${lon}], 10)
      L.marker([${lat}, ${lon}]).addTo(mymap)
    `);
  };

  openWhatsapp = (
    appraisalId,
    appraisalMisc,
    salesman,
    date,
    time,
    car,
    location,
    appraiser,
  ) => {
    let salesNum = '';
    if (
      salesman.whatsapp[0] === '0' ||
      (salesman.whatsapp[0] === '6' && salesman.whatsapp[1] === '2')
    ) {
      salesNum = salesNum + '+62';
    }
    if (
      (salesman.whatsapp.indexOf('-') !== -1 && salesman.whatsapp[0] === '0') ||
      (salesman.whatsapp.indexOf(' ') !== -1 && salesman.whatsapp[0] === '0')
    ) {
      for (let i = 1; i < salesman.whatsapp.length; i++) {
        if (salesman.whatsapp[i] !== '-' || salesman.whatsapp[i] !== ' ') {
          salesNum = salesNum + salesman.whatsapp[i];
        }
      }
    } else if (
      (salesman.whatsapp.indexOf('-') !== -1 && salesman.whatsapp[0] !== '0') ||
      (salesman.whatsapp.indexOf(' ') !== -1 && salesman.whatsapp[0] !== '0')
    ) {
      for (let i = 2; i < salesman.whatsapp.length; i++) {
        if (salesman.whatsapp[i] !== '-' || salesman.whatsapp[i] !== ' ') {
          salesNum = salesNum + salesman.whatsapp[i];
        }
      }
    } else if (salesman.whatsapp[0] !== '0') {
      salesNum = salesNum + salesman.whatsapp.slice(2);
    } else {
      salesNum = salesNum + salesman.whatsapp.slice(1);
    }

    this.confirmWhatsapp(appraisalId, appraisalMisc);
    // this.fetchAppraisalList();

    Linking.openURL(
      `https://wa.me/${salesNum}?text=Bpk/Ibu ${salesman.name}\n
      Terima kasih sudah booking appraisal di Toyota Trust. Saya hendak mengkonfirmasi ulang appraisal berikut ini:

      Hari/Tanggal: ${date}
      Jam: ${time}
      Mobil: ${car}
      Detail Lokasi: ${location}

      Regards,
      ${appraiser}
      Appraiser Toyota Trust`,
    );
  };

  _onRefresh() {
    this.setState({refreshing: true});

    // console.log('this.props.Auth', this.props.Auth.api_token);
    const {api_token} = this.props.Auth.api_token;

    axios
      .get(`/machine`, {
        headers: {
          Authorization: `Bearer ${api_token}`,
        },
      })
      .then((result) => {
        console.log('fetchTicketList', result.data);
        // let filterResult = result.data.appraisalList.filter(
        //   (data) => data.status !== 'FINISHED',
        // );
        // console.log(filterResult);
        this.setState({
          ticketList: result.data,
          refreshing: false,
        });
      })
      .catch((err) => {
        console.log('ini error', err.response);
        // ToastAndroid.showWithGravity(
        //   'Gagal menampilkan list ticket',
        //   2000,
        //   ToastAndroid.BOTTOM,
        // );
        this.setState({
          refreshing: false,
        });
      });
  }

  render() {
    const {ticketList, searchValue, spinner, refreshing} = this.state;
    const {user} = this.props.Auth.api_token;
    const filteredValueSearch = ticketList.filter(
      createFilter(searchValue, KEYS_TO_FILTERS),
    );

    return (
      <React.Fragment>
        <Spinner
          visible={spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        {/* <View style={styles.badgeBox}>
          <Image source={require('../../assets/image/badge.png')} style={{width: widthPercentageToDP('9%'), height: widthPercentageToDP('9%') }}/>
        </View> */}

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this._onRefresh.bind(this)}
            />
          }>
          {/* <View style={styles.infoContainer}>
            <View style={styles.infoBox}>
              <Text style={styles.infoTitle}>2</Text>
              <Text style={styles.infoSubHead}>CARD BULAN INI</Text>
            </View>

            <View style={styles.infoBox}>
              <Text style={styles.infoTitle}>3</Text>
              <Text style={styles.infoSubHead}>CARD BULAN SELESAI</Text>
            </View>
          </View> */}

          <View style={styles.listContainer}>
            {/* <View style={styles.containerSearch}>
              <SearchInput
                onChangeText={(value) => {
                  this.searchUpdated(value);
                }}
                style={styles.searchInput}
                placeholder="Type a to search"
              />
              <ScrollView>
                {filteredValueSearch.map((value) => {
                  value.map((data) => {
                    console.log('data', data);
                    return (
                      <TouchableOpacity
                        onPress={() => alert(data.bank.name_bank)}
                        key={data.id}
                        style={styles.valueItem}>
                        <View>
                          <Text>{data.bank.name_bank}</Text>
                          <Text style={styles.valueSubject}>
                            {data.name_address}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    );
                  });
                })}
              </ScrollView>
            </View> */}
            {console.log('atm', ticketList)}
            {ticketList.length !== 0 ? (
              ticketList.map((ticket, index) => (
                <View key={index}>
                  {ticket.map((data, index) => (
                    <View style={styles.bookingItem} key={data.id}>
                      <TouchableOpacity
                      // onPress={() =>
                      //   this.props.requestAppraisalDetail(data.id)
                      // }
                      >
                        <View style={styles.noOrderBox}>
                          <Text style={styles.noOrder}>
                            {data.machine_id} ({data.type.name_type}{' '}
                            {data.machine_sn})
                          </Text>

                          <Text
                            style={{
                              fontWeight: 'bold',
                              fontSize: widthPercentageToDP('3%'),
                              color:
                                data.status === 'Tidak Aktif'
                                  ? colors.red_insan
                                  : colors.green_insan,
                            }}>
                            {data.status}
                          </Text>
                        </View>
                        <View style={styles.detailBook}>
                          <Text style={styles.date}>{data.bank.name_bank}</Text>
                          <Text style={styles.brandText}>
                            <Text>{data.name_address}</Text>
                          </Text>
                        </View>
                      </TouchableOpacity>
                      {/* --------- [START MAP VIEW] ----------- */}
                      {/* <View style={styles.salesProfile}>
                      <View
                        style={{
                          width: widthPercentageToDP('40%'),
                          justifyContent: 'center',
                        }}>
                        <Text style={styles.brandText}>
                          <Text>Cabang: </Text>
                        </Text>
                        <Text style={styles.date}>
                          {ticket.branch.name_branch}
                        </Text>
                        <Text style={styles.brandText}>
                          <Text>Type Mechine: </Text>
                        </Text>
                        <Text style={styles.date}>{ticket.type.name_type}</Text>
                        <Text style={styles.brandText}>
                          <Text>Update at: </Text>
                        </Text>
                        <Text style={styles.date}>
                          {moment(ticket.updated_at, 'YYYY-MM-DDTHH:mm').format(
                            'llll',
                          )}
                        </Text>
                        <Text style={styles.brandText}>
                          <Text>Map Location at: </Text>
                        </Text>
                        <SafeAreaView
                          style={{
                            marginTop: widthPercentageToDP('4%'),
                            width: widthPercentageToDP('80%'),
                            height: widthPercentageToDP('80%'),
                            padding: 10,
                            backgroundColor: 'grey',
                          }}>
                          <WebView ref={'Map_Ref'} source={{html: html_script}} />
                        </SafeAreaView>
                        <TouchableOpacity
                          onPress={() =>
                            this._goToMyPosition(
                              ticket.latitude,
                              ticket.longitude,
                            )
                          }
                          style={styles.findLocationATMButton}>
                          <Text style={styles.logoutTextFindLocationButton}>
                            Location ATM
                          </Text>
                        </TouchableOpacity>
                      </View>

                      <TouchableOpacity
                        style={[
                          styles.WAbutton,
                          !appraisal.misc.whatsappConfirmation.status
                            ? styles.WAactive
                            : styles.WAinactive,
                        ]}
                        style={[styles.WAbutton, styles.WAactive]}
                        disabled={
                          !appraisal.misc.whatsappConfirmation.status
                            ? false
                            : true
                        }
                        onPress={() =>
                          this.openWhatsapp(
                            appraisal.id,
                            appraisal.misc,
                            appraisal.Booking.SalesProfile,
                            moment(
                              appraisal.Booking.bookingTime,
                              'YYYY-MM-DD',
                            ).format('ll'),
                            moment(
                              appraisal.Booking.bookingTime,
                              'YYYY-MM-DDTHH:mm',
                            ).format('LTS'),
                            appraisal.Booking.carType,
                            appraisal.Booking.notes,
                            appraisal.Booking.Appraiser.name,
                          )
                        }>
                        <Icon
                          name="whatsapp"
                          size={widthPercentageToDP('6%')}
                          color="white"
                        />
                        <Text style={styles.WAtext}>Whatsapp</Text>
                      </TouchableOpacity>
                      </View> */}
                      {/* --------- [END MAP VIEW] ----------- */}
                    </View>
                  ))}
                </View>
              ))
            ) : (
              <View
                style={{
                  width: widthPercentageToDP('90%'),
                  height: heightPercentageToDP('50%'),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: colors.dope_blue,
                    fontSize: widthPercentageToDP('5%'),
                  }}>
                  Empty
                </Text>
              </View>
            )}
          </View>
        </ScrollView>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoContainer: {
    paddingHorizontal: widthPercentageToDP('5%'),
    paddingVertical: widthPercentageToDP('5%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  infoBox: {
    width: widthPercentageToDP('43.5%'),
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('2%'),
    backgroundColor: 'white',
    borderRadius: 5,
  },
  infoTitle: {
    color: colors.strong_blue,
    fontWeight: 'bold',
    fontSize: widthPercentageToDP('10%'),
  },
  infoSubHead: {
    fontSize: widthPercentageToDP('3.5%'),
  },
  listContainer: {
    marginTop: widthPercentageToDP('4%'),
    paddingHorizontal: widthPercentageToDP('5%'),
  },
  bookingItem: {
    padding: widthPercentageToDP('4%'),
    backgroundColor: 'white',
    elevation: 4,
    shadowOffset: {width: 7, height: 5},
    shadowColor: 'grey',
    shadowOpacity: 0.5,
    shadowRadius: 18,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('5%'),
  },
  noOrderBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: widthPercentageToDP('2%'),
  },
  noOrder: {
    fontSize: widthPercentageToDP('3%'),
    color: colors.strong_blue,
    fontWeight: 'bold',
  },
  brandText: {
    fontSize: widthPercentageToDP('4.35%'),
    fontWeight: 'bold',
    color: colors.dope_blue,
  },
  date: {
    paddingBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4.3%'),
  },
  detailBook: {
    paddingBottom: widthPercentageToDP('2%'),
    borderColor: colors.light_sapphire_bluish_gray,
    // borderBottomWidth: 1.5,
  },
  salesProfile: {
    paddingVertical: widthPercentageToDP('2%'),
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  WAbutton: {
    width: widthPercentageToDP('40%'),
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('2.2%'),
    // backgroundColor: colors.whatsapp,
    flexDirection: 'row',
    borderRadius: 5,
  },
  WAtext: {
    color: 'white',
    fontSize: widthPercentageToDP('4.5%'),
    fontWeight: 'bold',
    marginLeft: 10,
  },
  WAactive: {
    backgroundColor: colors.whatsapp,
  },
  WAinactive: {
    backgroundColor: colors.super_light_grayish_blue,
  },
  badgeBox: {
    position: 'absolute',
    top: 0,
    bottom: 100,
    zIndex: 100,
  },
  ButtonArea: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  Button: {
    width: 80,
    padding: 10,
    borderRadius: 10,
    backgroundColor: 'black',
    alignItems: 'center',
  },
  ButtonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14,
  },
  findLocationATMButton: {
    backgroundColor: colors.strong_blue,
    marginTop: widthPercentageToDP('4%'),
    borderRadius: 6,
    borderColor: colors.strong_blue,
    borderWidth: 2,
    width: '100%',
    paddingVertical: widthPercentageToDP('2%'),
    alignItems: 'center',
  },
  logoutTextFindLocationButton: {
    color: '#fff',
    fontWeight: '800',
    textAlign: 'center',
    fontSize: widthPercentageToDP('5%'),
  },
  searchInput: {
    padding: 10,
    borderColor: '#CCC',
    borderWidth: 1,
  },
  containerSearch: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    marginBottom: widthPercentageToDP('5%'),
  },
  valueItem: {
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.3)',
    padding: 10,
  },
  valueSubject: {
    color: 'rgba(0,0,0,0.5)',
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
});

const mapStateToProps = ({Auth, AppraisalDetail, AppraisalData}) => ({
  Auth,
  AppraisalDetail,
  AppraisalData,
});
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      // requestAppraisalDetail
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AtmList);
