import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  View,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  ToastAndroid,
  Linking,
  StyleSheet,
  SafeAreaView,
  RefreshControl,
} from 'react-native';
import moment from 'moment';
import 'moment/locale/id';
import {
  resetApprasialData,
  setApprasialData,
} from '../../reducers/ApprasialData';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../utils/colors';
import {WebView} from 'react-native-webview';
import html_script from '../../utils/html_script';

import {heightPercentageToDP, widthPercentageToDP} from '../../utils';
// import {requestAppraisalDetail} from '../../actions';
import axios from '../../services/axios';
import logo from '../../assets/image/logo-trust.png';
moment.locale('id');
import UploadModalTicketPending from '../../component/Modal/UploadModalTicketPending';
import UploadModalTicketConfirm from '../../component/Modal/UploadModalTicketConfirm';
import UploadModalTicketWorkStart from '../../component/Modal/UploadModalTicketWorkStart';
import SearchInput, {createFilter} from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['problem', 'problem_status'];
import Spinner from 'react-native-loading-spinner-overlay';

const arrayStatus = ['WORK START', 'OPEN', 'CONFIRM', 'PENDING', 'CLOSE'];

class AppraisalList extends React.Component {
  constructor(props) {
    super(props);
    this._onPressTicketPending = this._onPressTicketPending.bind(this);
    this._toggleTicketPendingModal = this._toggleTicketPendingModal.bind(this);
    this._onPressTicketConfirm = this._onPressTicketConfirm.bind(this);
    this._toggleTicketConfirmModal = this._toggleTicketConfirmModal.bind(this);
    this._onPressTicketWorkStart = this._onPressTicketWorkStart.bind(this);
    this._toggleTicketWorkStartModal = this._toggleTicketWorkStartModal.bind(
      this,
    );
    this.findListSearch = this.findListSearch.bind(this);
    this._closeImage = this._closeImage.bind(this);
    this._showImage = this._showImage.bind(this);
    this.state = {
      ticketList: [],
      valueListSearch: [],
      displayTicketPendingModal: false,
      displayTicketConfirmModal: false,
      displayTicketWorkStartModal: false,
      refreshing: false,
      listSearchOnpress: false,
      document: {},
      dataConfirm: {},
      dataWorkStart: {},
      searchValue: '',
      spinner: false,
    };
  }

  // componentWillUnmount() {
  //   this.navigationWillFocusListener.remove();
  //   setApprasialData([]);
  // }

  componentDidMount() {
    this.fetchTicketList();
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      displayTicketConfirmModal,
      displayTicketWorkStartModal,
      displayTicketPendingModal,
    } = this.state;
    // console.log('prevProps', prevProps);
    // console.log('prevState', prevState);
    if (displayTicketConfirmModal !== prevState.displayTicketConfirmModal) {
      this.fetchTicketList();
    }
    if (displayTicketWorkStartModal !== prevState.displayTicketWorkStartModal) {
      this.fetchTicketList();
    }
    if (displayTicketPendingModal !== prevState.displayTicketPendingModal) {
      this.fetchTicketList();
    }
  }

  fetchTicketList() {
    this.setState({
      spinner: true,
    });
    // console.log('this.props.Auth', this.props.Auth.api_token);
    const {api_token} = this.props.Auth.api_token;

    axios
      .get('/problem', {
        headers: {
          Authorization: `Bearer ${api_token}`,
        },
      })
      .then((result) => {
        // console.log('fetchTicketList', result.data);
        // let filterResult = result.data.appraisalList.filter(
        //   (data) => data.status !== 'FINISHED',
        // );
        // console.log(filterResult);
        this.setState({
          ticketList: result.data,
          spinner: false,
        });
      })
      .catch((err) => {
        // console.log('ini error', err.response);
        ToastAndroid.showWithGravity(
          'Gagal menampilkan list ticket',
          2000,
          ToastAndroid.BOTTOM,
        );
        this.setState({
          spinner: false,
        });
      });
  }

  searchUpdated(value) {
    console.log('value', value);
    this.setState({searchValue: value});
  }

  _goToMyPosition = (lat, lon) => {
    // console.log('tike atm lokasi', lat, lon);
    this.refs.Map_Ref.injectJavaScript(`
      mymap.setView([${lat}, ${lon}], 10)
      L.marker([${lat}, ${lon}]).addTo(mymap)
    `);
  };

  _onPressTicketPending(ticket) {
    // console.log('_onPressTicketPending', ticket);
    this.setState({
      displayTicketPendingModal: true,
      dataWorkStart: ticket,
    });
  }

  _toggleTicketPendingModal() {
    this.setState({
      displayTicketPendingModal: !this.state.displayTicketPendingModal,
    });
  }

  _toggleTicketConfirmModal() {
    this.setState({
      displayTicketConfirmModal: !this.state.displayTicketConfirmModal,
    });
  }

  _onPressTicketConfirm(ticket) {
    this.setState({
      displayTicketConfirmModal: true,
      dataConfirm: ticket,
    });
  }

  _toggleTicketWorkStartModal() {
    this.setState({
      displayTicketWorkStartModal: !this.state.displayTicketWorkStartModal,
    });
  }

  _onPressTicketWorkStart(ticket) {
    this.setState({
      displayTicketWorkStartModal: true,
      dataWorkStart: ticket,
    });
  }

  _showImage(imageUrl) {
    // console.log('open image', imageUrl);
    this.setState({
      imageLink: imageUrl,
      viewImage: !this.state.viewImage,
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _closeImage() {
    // console.log('close image');
    this.setState({
      viewImage: !this.state.viewImage,
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _onRefresh() {
    this.fetchTicketList();
  }

  findListSearch(value) {
    const {ticketList} = this.state;
    let tempFind = ticketList[0]?.filter((data) => {
      return data.problem.toLowerCase().includes(value.toLowerCase());
    });

    this.setState({
      valueListSearch: tempFind,
      listSearchOnpress: true,
      searchValue: '',
      ticketList: [],
    });
  }

  render() {
    const {
      ticketList,
      displayTicketPendingModal,
      displayTicketConfirmModal,
      displayTicketWorkStartModal,
      document,
      searchValue,
      dataConfirm,
      dataWorkStart,
      refreshing,
      listSearchOnpress,
      valueListSearch,
      spinner,
    } = this.state;
    const {user} = this.props.Auth.api_token;
    const filteredValueSearch = ticketList.filter(
      createFilter(searchValue, KEYS_TO_FILTERS),
    );

    return (
      <React.Fragment>
        <Spinner
          visible={spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        {/* <View style={styles.badgeBox}>
          <Image source={require('../../assets/image/badge.png')} style={{width: widthPercentageToDP('9%'), height: widthPercentageToDP('9%') }}/>
        </View> */}

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this._onRefresh.bind(this)}
            />
          }>
          {/* <View style={styles.infoContainer}>
            <View style={styles.infoBox}>
              <Text style={styles.infoTitle}>2</Text>
              <Text style={styles.infoSubHead}>CARD BULAN INI</Text>
            </View>

            <View style={styles.infoBox}>
              <Text style={styles.infoTitle}>3</Text>
              <Text style={styles.infoSubHead}>CARD BULAN SELESAI</Text>
            </View>
          </View> */}

          <View style={styles.listContainer}>
            <View style={styles.containerSearch}>
              <SearchInput
                onChangeText={(value) => {
                  this.searchUpdated(value);
                }}
                style={styles.searchInput}
                placeholder="Type a to search"
              />
              {searchValue ? (
                <ScrollView>
                  {filteredValueSearch[0]?.length > 0 &&
                    filteredValueSearch[0]?.map((value) => {
                      return (
                        <TouchableOpacity
                          // onPress={() => alert(value.problem)}
                          onPress={() => this.findListSearch(value.problem)}
                          key={value.id}
                          style={styles.valueItem}>
                          <View>
                            <Text>{value.problem}</Text>
                            <Text style={styles.valueSubject}>
                              {value.problem_status}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      );
                    })}
                </ScrollView>
              ) : null}
            </View>
            {/* {console.log('ticket', ticketList)} */}
            {ticketList.length !== 0 ? (
              ticketList.map((value, index) => (
                <View key={index}>
                  {value.map((ticket, index) => (
                    <View style={styles.bookingItem} key={ticket.id}>
                      <View style={styles.noOrderBox}>
                        <Text style={styles.noOrder}>
                          {ticket.ticket_id}/
                          {ticket.no_problem
                            ? no_problem
                            : ' Empty No. Problem'}
                        </Text>

                        <Text
                          style={{
                            fontWeight: 'bold',
                            fontSize: widthPercentageToDP('3%'),
                            color:
                              ticket.problem_status === 'WORK START'
                                ? colors.blue_insan
                                : ticket.problem_status === 'OPEN'
                                ? colors.red_insan
                                : ticket.problem_status === 'CONFIRM'
                                ? colors.orange_insan
                                : ticket.problem_status === 'PENDING'
                                ? colors.blue_insan
                                : ticket.problem_status === 'CLOSE'
                                ? colors.green_insan
                                : null,
                          }}>
                          {ticket.problem_status}{' '}
                          {ticket.kelanjutan_problem
                            ? `(${ticket.kelanjutan_problem})`
                            : null}
                        </Text>
                      </View>
                      <View style={styles.detailBook}>
                        <Text style={styles.date}>
                          {ticket.ticket.machine.machine_id}(
                          {ticket.ticket.maintance_type}
                          {ticket.ticket.machine.machine_sn})
                        </Text>
                        <Text style={styles.brandText}>
                          <Text>{ticket.problem}</Text>
                        </Text>

                        {/* <Text style={styles.date}>
                            {moment('17 Jun 2020', 'YYYY-MM-DDTHH:mm').format(
                              'llll',
                            )}
                          </Text> */}
                      </View>
                      <View style={styles.salesProfile}>
                        {/* --------- [START MAP VIEW] ----------- */}
                        {/* <View
                          style={{
                            width: widthPercentageToDP('40%'),
                            justifyContent: 'center',
                          }}>
                          <Text style={styles.brandText}>
                            <Text>ATM Location map: </Text>
                          </Text>
                          <SafeAreaView
                            style={{
                              marginTop: widthPercentageToDP('4%'),
                              width: widthPercentageToDP('80%'),
                              height: widthPercentageToDP('80%'),
                              padding: 6,
                              backgroundColor: colors.strong_grey,
                            }}>
                            {console.log('html_script', html_script)}
                            <WebView
                              ref={'Map_Ref'}
                              source={{html: html_script}}
                            />
                          </SafeAreaView>
                          <TouchableOpacity
                            onPress={() =>
                              this._goToMyPosition(
                                ticket.atm.latitude,
                                ticket.atm.longitude,
                              )
                            }
                            style={styles.findLocationATMButton}>
                            <Text style={styles.textFindLocationButton}>
                              Location ATM
                            </Text>
                          </TouchableOpacity>
                        </View> */}
                        {/* --------- [END MAP VIEW] ----------- */}

                        <TouchableOpacity
                          style={[
                            styles.ButtonContainerStyle,
                            ticket.problem_status === 'OPEN'
                              ? styles.ButtonOrangeStyle
                              : styles.ButtonInactive,
                          ]}
                          disabled={
                            ticket.problem_status === 'OPEN' ? false : true
                          }
                          // onPress={() =>
                          //   this.props.requestAppraisalDetail(ticket.id)
                          // }
                          onPress={() => this._onPressTicketConfirm(ticket)}
                          // onPress={() =>
                          //   this.openWhatsapp(
                          //     appraisal.id,
                          //     appraisal.misc,
                          //     appraisal.Booking.SalesProfile,
                          //     moment(
                          //       appraisal.Booking.bookingTime,
                          //       'YYYY-MM-DD',
                          //     ).format('ll'),
                          //     moment(
                          //       appraisal.Booking.bookingTime,
                          //       'YYYY-MM-DDTHH:mm',
                          //     ).format('LTS'),
                          //     appraisal.Booking.carType,
                          //     appraisal.Booking.notes,
                          //     appraisal.Booking.Appraiser.name,
                          //   )
                          // }
                        >
                          <Icon
                            name="check"
                            size={widthPercentageToDP('6%')}
                            color="white"
                          />
                          <Text style={styles.ButtonTextStyle}>Confirm</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[
                            styles.ButtonContainerStyle,
                            ticket.problem_status === 'CONFIRM'
                              ? styles.ButtonBlueColorStyle
                              : styles.ButtonInactive,
                          ]}
                          disabled={
                            ticket.problem_status === 'CONFIRM' ? false : true
                          }
                          onPress={() => this._onPressTicketWorkStart(ticket)}
                          // onPress={() =>
                          //   this.openWhatsapp(
                          //     appraisal.id,
                          //     appraisal.misc,
                          //     appraisal.Booking.SalesProfile,
                          //     moment(
                          //       appraisal.Booking.bookingTime,
                          //       'YYYY-MM-DD',
                          //     ).format('ll'),
                          //     moment(
                          //       appraisal.Booking.bookingTime,
                          //       'YYYY-MM-DDTHH:mm',
                          //     ).format('LTS'),
                          //     appraisal.Booking.carType,
                          //     appraisal.Booking.notes,
                          //     appraisal.Booking.Appraiser.name,
                          //   )
                          // }
                        >
                          <Icon
                            name="clock-o"
                            size={widthPercentageToDP('6%')}
                            color="white"
                          />
                          <Text style={styles.ButtonTextStyle}>Work Start</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[
                            styles.ButtonContainerStyle,
                            ticket.problem_status === 'WORK START'
                              ? styles.ButtonBlueColorStyle
                              : styles.ButtonInactive,
                          ]}
                          disabled={
                            ticket.problem_status === 'WORK START'
                              ? false
                              : true
                          }
                          onPress={() => this._onPressTicketPending(ticket)}
                          // onPress={() =>
                          //   this.openWhatsapp(
                          //     appraisal.id,
                          //     appraisal.misc,
                          //     appraisal.Booking.SalesProfile,
                          //     moment(
                          //       appraisal.Booking.bookingTime,
                          //       'YYYY-MM-DD',
                          //     ).format('ll'),
                          //     moment(
                          //       appraisal.Booking.bookingTime,
                          //       'YYYY-MM-DDTHH:mm',
                          //     ).format('LTS'),
                          //     appraisal.Booking.carType,
                          //     appraisal.Booking.notes,
                          //     appraisal.Booking.Appraiser.name,
                          //   )
                          // }
                        >
                          <Icon
                            name="gears"
                            size={widthPercentageToDP('6%')}
                            color="white"
                          />
                          <Text style={styles.ButtonTextStyle}>Pending</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  ))}
                </View>
              ))
            ) : listSearchOnpress ? (
              valueListSearch.map((ticket, index) => (
                <View style={styles.bookingItem} key={ticket.id}>
                  <View style={styles.noOrderBox}>
                    <Text style={styles.noOrder}>
                      {ticket.ticket_id}/
                      {ticket.no_problem ? no_problem : ' Empty No. Problem'}
                    </Text>

                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: widthPercentageToDP('3%'),
                        color:
                          ticket.problem_status === 'WORK START'
                            ? colors.blue_insan
                            : ticket.problem_status === 'OPEN'
                            ? colors.red_insan
                            : ticket.problem_status === 'CONFIRM'
                            ? colors.orange_insan
                            : ticket.problem_status === 'PENDING'
                            ? colors.blue_insan
                            : ticket.problem_status === 'CLOSE'
                            ? colors.green_insan
                            : null,
                      }}>
                      {ticket.problem_status}{' '}
                      {ticket.kelanjutan_problem
                        ? `(${ticket.kelanjutan_problem})`
                        : null}
                    </Text>
                  </View>
                  <View style={styles.detailBook}>
                    <Text style={styles.date}>
                      {ticket.ticket.machine.machine_id}(
                      {ticket.ticket.maintance_type}
                      {ticket.ticket.machine.machine_sn})
                    </Text>
                    <Text style={styles.brandText}>
                      <Text>{ticket.problem}</Text>
                    </Text>

                    {/* <Text style={styles.date}>
                            {moment('17 Jun 2020', 'YYYY-MM-DDTHH:mm').format(
                              'llll',
                            )}
                          </Text> */}
                  </View>
                  <View style={styles.salesProfile}>
                    {/* --------- [START MAP VIEW] ----------- */}
                    {/* <View
                          style={{
                            width: widthPercentageToDP('40%'),
                            justifyContent: 'center',
                          }}>
                          <Text style={styles.brandText}>
                            <Text>ATM Location map: </Text>
                          </Text>
                          <SafeAreaView
                            style={{
                              marginTop: widthPercentageToDP('4%'),
                              width: widthPercentageToDP('80%'),
                              height: widthPercentageToDP('80%'),
                              padding: 6,
                              backgroundColor: colors.strong_grey,
                            }}>
                            {console.log('html_script', html_script)}
                            <WebView
                              ref={'Map_Ref'}
                              source={{html: html_script}}
                            />
                          </SafeAreaView>
                          <TouchableOpacity
                            onPress={() =>
                              this._goToMyPosition(
                                ticket.atm.latitude,
                                ticket.atm.longitude,
                              )
                            }
                            style={styles.findLocationATMButton}>
                            <Text style={styles.textFindLocationButton}>
                              Location ATM
                            </Text>
                          </TouchableOpacity>
                        </View> */}
                    {/* --------- [END MAP VIEW] ----------- */}

                    <TouchableOpacity
                      style={[
                        styles.ButtonContainerStyle,
                        ticket.problem_status === 'OPEN'
                          ? styles.ButtonOrangeStyle
                          : styles.ButtonInactive,
                      ]}
                      disabled={ticket.problem_status === 'OPEN' ? false : true}
                      // onPress={() =>
                      //   this.props.requestAppraisalDetail(ticket.id)
                      // }
                      onPress={() => this._onPressTicketConfirm(ticket)}
                      // onPress={() =>
                      //   this.openWhatsapp(
                      //     appraisal.id,
                      //     appraisal.misc,
                      //     appraisal.Booking.SalesProfile,
                      //     moment(
                      //       appraisal.Booking.bookingTime,
                      //       'YYYY-MM-DD',
                      //     ).format('ll'),
                      //     moment(
                      //       appraisal.Booking.bookingTime,
                      //       'YYYY-MM-DDTHH:mm',
                      //     ).format('LTS'),
                      //     appraisal.Booking.carType,
                      //     appraisal.Booking.notes,
                      //     appraisal.Booking.Appraiser.name,
                      //   )
                      // }
                    >
                      <Icon
                        name="check"
                        size={widthPercentageToDP('6%')}
                        color="white"
                      />
                      <Text style={styles.ButtonTextStyle}>Confirm</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[
                        styles.ButtonContainerStyle,
                        ticket.problem_status === 'CONFIRM'
                          ? styles.ButtonBlueColorStyle
                          : styles.ButtonInactive,
                      ]}
                      disabled={
                        ticket.problem_status === 'CONFIRM' ? false : true
                      }
                      onPress={() => this._onPressTicketWorkStart(ticket)}
                      // onPress={() =>
                      //   this.openWhatsapp(
                      //     appraisal.id,
                      //     appraisal.misc,
                      //     appraisal.Booking.SalesProfile,
                      //     moment(
                      //       appraisal.Booking.bookingTime,
                      //       'YYYY-MM-DD',
                      //     ).format('ll'),
                      //     moment(
                      //       appraisal.Booking.bookingTime,
                      //       'YYYY-MM-DDTHH:mm',
                      //     ).format('LTS'),
                      //     appraisal.Booking.carType,
                      //     appraisal.Booking.notes,
                      //     appraisal.Booking.Appraiser.name,
                      //   )
                      // }
                    >
                      <Icon
                        name="clock-o"
                        size={widthPercentageToDP('6%')}
                        color="white"
                      />
                      <Text style={styles.ButtonTextStyle}>Work Start</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[
                        styles.ButtonContainerStyle,
                        ticket.problem_status === 'WORK START'
                          ? styles.ButtonBlueColorStyle
                          : styles.ButtonInactive,
                      ]}
                      disabled={
                        ticket.problem_status === 'WORK START' ? false : true
                      }
                      onPress={() => this._onPressTicketPending(ticket)}
                      // onPress={() =>
                      //   this.openWhatsapp(
                      //     appraisal.id,
                      //     appraisal.misc,
                      //     appraisal.Booking.SalesProfile,
                      //     moment(
                      //       appraisal.Booking.bookingTime,
                      //       'YYYY-MM-DD',
                      //     ).format('ll'),
                      //     moment(
                      //       appraisal.Booking.bookingTime,
                      //       'YYYY-MM-DDTHH:mm',
                      //     ).format('LTS'),
                      //     appraisal.Booking.carType,
                      //     appraisal.Booking.notes,
                      //     appraisal.Booking.Appraiser.name,
                      //   )
                      // }
                    >
                      <Icon
                        name="gears"
                        size={widthPercentageToDP('6%')}
                        color="white"
                      />
                      <Text style={styles.ButtonTextStyle}>Pending</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              ))
            ) : (
              <View
                style={{
                  width: widthPercentageToDP('90%'),
                  height: heightPercentageToDP('50%'),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: colors.dope_blue,
                    fontSize: widthPercentageToDP('5%'),
                  }}>
                  Empty
                </Text>
              </View>
            )}
          </View>
          {/* ---------- [START MODAL TICKET CONFIRM] ------------ */}
          <View style={styles.containerModal}>
            {displayTicketConfirmModal ? (
              <UploadModalTicketConfirm
                document={document}
                dataConfirm={dataConfirm}
                togglePhotoModal={this._toggleTicketConfirmModal}
                displayPhotoModal={displayTicketConfirmModal}
              />
            ) : (
              false
            )}
          </View>
          {/* ---------- [START MODAL TICKET CONFIRM] ------------ */}
          {/* ---------- [START MODAL TICKET WORK START] ------------ */}
          <View style={styles.containerModal}>
            {displayTicketWorkStartModal ? (
              <UploadModalTicketWorkStart
                document={document}
                dataWorkStart={dataWorkStart}
                togglePhotoModal={this._toggleTicketWorkStartModal}
                displayPhotoModal={displayTicketWorkStartModal}
                previewPhoto={this._showImage}
              />
            ) : (
              false
            )}
          </View>
          {/* ---------- [START MODAL TICKET WORK START] ------------ */}
          {/* ---------- [START MODAL TICKET PENDING] ------------ */}
          <View style={styles.containerModal}>
            {displayTicketPendingModal ? (
              <UploadModalTicketPending
                document={document}
                dataWorkStart={dataWorkStart}
                togglePhotoModal={this._toggleTicketPendingModal}
                displayPhotoModal={displayTicketPendingModal}
                previewPhoto={this._showImage}
              />
            ) : (
              false
            )}
          </View>
          {/* ---------- [START MODAL TICKET PENDING] ------------ */}
        </ScrollView>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoContainer: {
    paddingHorizontal: widthPercentageToDP('5%'),
    paddingVertical: widthPercentageToDP('5%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  infoBox: {
    width: widthPercentageToDP('43.5%'),
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('2%'),
    backgroundColor: 'white',
    borderRadius: 5,
  },
  infoTitle: {
    color: colors.strong_blue,
    fontWeight: 'bold',
    fontSize: widthPercentageToDP('10%'),
  },
  infoSubHead: {
    fontSize: widthPercentageToDP('3.5%'),
  },
  listContainer: {
    marginTop: widthPercentageToDP('4%'),
    paddingHorizontal: widthPercentageToDP('5%'),
  },
  bookingItem: {
    padding: widthPercentageToDP('4%'),
    backgroundColor: 'white',
    elevation: 4,
    shadowOffset: {width: 7, height: 5},
    shadowColor: 'grey',
    shadowOpacity: 0.5,
    shadowRadius: 18,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('5%'),
  },
  noOrderBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: widthPercentageToDP('2%'),
  },
  noOrder: {
    fontSize: widthPercentageToDP('3%'),
    color: colors.strong_blue,
    fontWeight: 'bold',
  },
  brandText: {
    fontSize: widthPercentageToDP('4.35%'),
    fontWeight: 'bold',
    color: colors.dope_blue,
  },
  date: {
    paddingBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4.3%'),
  },
  detailBook: {
    paddingBottom: widthPercentageToDP('2%'),
    borderColor: colors.light_sapphire_bluish_gray,
    borderBottomWidth: 1.5,
  },
  salesProfile: {
    paddingVertical: widthPercentageToDP('2%'),
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  ButtonContainerStyle: {
    width: widthPercentageToDP('25%'),
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('2.2%'),
    // backgroundColor: colors.whatsapp,
    flexDirection: 'row',
    borderRadius: 5,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  ButtonTextStyle: {
    color: 'white',
    fontSize: widthPercentageToDP('3.5%'),
    fontWeight: 'bold',
    marginLeft: 3,
  },
  WAactive: {
    backgroundColor: colors.whatsapp,
  },
  ButtonInactive: {
    backgroundColor: colors.super_light_grayish_blue,
  },
  ButtonOrangeStyle: {
    backgroundColor: colors.orange_insan,
  },
  ButtonBlueColorStyle: {
    backgroundColor: colors.blue_insan,
  },
  badgeBox: {
    position: 'absolute',
    top: 0,
    bottom: 100,
    zIndex: 100,
  },
  findLocationATMButton: {
    backgroundColor: colors.strong_blue,
    marginTop: widthPercentageToDP('4%'),
    borderRadius: 6,
    borderColor: colors.strong_blue,
    borderWidth: 2,
    width: '100%',
    paddingVertical: widthPercentageToDP('2%'),
    alignItems: 'center',
  },
  textFindLocationButton: {
    color: '#fff',
    fontWeight: '800',
    textAlign: 'center',
    fontSize: widthPercentageToDP('5%'),
  },
  searchInput: {
    padding: 10,
    borderColor: '#CCC',
    borderWidth: 1,
  },
  containerSearch: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    marginBottom: widthPercentageToDP('5%'),
  },
  valueItem: {
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.3)',
    padding: 10,
  },
  valueSubject: {
    color: 'rgba(0,0,0,0.5)',
  },
});

const mapStateToProps = ({Auth, AppraisalDetail, AppraisalData}) => ({
  Auth,
  AppraisalDetail,
  AppraisalData,
});
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      // requestAppraisalDetail
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AppraisalList);
