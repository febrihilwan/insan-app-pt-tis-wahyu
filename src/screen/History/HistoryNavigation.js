import React from 'react';
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator, DrawerItems} from 'react-navigation-drawer';
import {Provider} from 'react-redux';
import {Image, TouchableOpacity, View, StyleSheet} from 'react-native';
import {widthPercentageToDP, heightPercentageToDP} from '../../utils/index';
import colors from '../../utils/colors';

import HistoryAppraisal from './HIstoryAppraisList';
import HistoryHandover from './HistoryHandoverList';

const History = createMaterialTopTabNavigator(
  {
    HistoryAppraisal: {
      screen: HistoryAppraisal,
      navigationOptions: {
        title: 'Appraisal',
      },
    },
    HistoryHandover: {
      screen: HistoryHandover,
      navigationOptions: {
        title: 'Handover',
      },
    },
  },
  {
    lazy: true,
    swipeEnabled: true,
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: colors.dope_blue,
      inactiveTintColor: colors.grayish_blue,
      style: {
        backgroundColor: 'white',
      },
      indicatorStyle: {
        backgroundColor: colors.strong_blue,
        height: 4,
      },
    },
  },
);

const HistoryNavigator = createStackNavigator({
  History: {
    screen: History,
    navigationOptions: ({navigation}) => ({
      title: 'History',
      headerStyle: {
        backgroundColor: colors.strong_blue,
      },
      headerTintColor: 'white',
      headerLeft: () => (
        <TouchableOpacity
          style={{marginLeft: 10}}
          onPress={navigation.openDrawer}>
          <Image
            source={require('../../assets/image/menu.png')}
            style={{
              width: widthPercentageToDP('8%'),
              height: widthPercentageToDP('8%'),
            }}
          />
        </TouchableOpacity>
      ),
      headerTitleStyle: {
        fontSize: widthPercentageToDP('5%'),
      },
    }),
  },
});

export default HistoryNavigator;
