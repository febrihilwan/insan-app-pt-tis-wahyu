import React, { Component } from 'react'
import { TouchableOpacity, View, StyleSheet,ScrollView, Text } from 'react-native'
import {widthPercentageToDP,heightPercentageToDP} from '../../utils/index'
import colors from '../../utils/colors'

class MessageList extends Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <TouchableOpacity style={styles.box}>
                        <View>
                            <Text style={styles.name}>Agus</Text>
                            <Text style={styles.regText}>Toyota Rush</Text>
                        </View>
                        <Text style={styles.date}>Senin 08:12</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.box}>
                        <View>
                            <Text style={styles.name}>Agus</Text>
                            <Text style={styles.regText}>Toyota Rush</Text>
                        </View>
                        <Text style={styles.date}>Senin 08:12</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.box}>
                        <View>
                            <Text style={styles.name}>Agus</Text>
                            <Text style={styles.regText}>Toyota Rush</Text>
                        </View>
                        <Text style={styles.date}>Senin 08:12</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: heightPercentageToDP('100%'),
        paddingLeft: widthPercentageToDP('5%'),
        backgroundColor: 'white'
    },
    box: {
        paddingVertical: widthPercentageToDP('4%'),
        paddingRight: widthPercentageToDP('5%'),
        borderColor: colors.supeer_light_grayish_blue,
        borderBottomWidth: 1.5,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    name: {
        color: colors.dope_blue,
        fontWeight: 'bold',
        fontSize: widthPercentageToDP('4.5%')
    },
    regText: {
        color: colors.dope_blue,
        fontSize: widthPercentageToDP('4.5%')
    },
    date: {
        color: colors.other_grayish_blue,
        fontSize: widthPercentageToDP('3.5%')
    }
})

export default MessageList;