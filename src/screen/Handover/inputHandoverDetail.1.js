import React, { Component, Fragment } from "react";
import { HeaderBackButton } from "react-navigation-stack";
import { Text, View, ScrollView, TextInput, StyleSheet, TouchableOpacity, Picker, Image } from "react-native";
import { widthPercentageToDP, heightPercentageToDP } from "../../utils";
import colors from "../../utils/colors";
import ImagePicker from "react-native-image-picker";
import Icons from 'react-native-vector-icons/MaterialCommunityIcons'

class HandoverDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            form: [
                {
                    fieldName: 'Salesman',
                    type: 'select',
                    value: ''
                },{
                    fieldName: 'Salesman Lainnya',
                    type: 'text',
                    value: ''
                },{
                    fieldName: 'Cabang',
                    type: 'select',
                    value: ''
                },{
                    fieldName: 'PO',
                    type: 'radio',
                    value: ''
                },{
                    fieldName: 'Alokasi Dana',
                    type: 'radio',
                    value: ''
                },{
                    fieldName: 'Surat Pernyataan',
                    type: 'radio',
                    value: ''
                },{
                    fieldName: 'Tanda Terima Surat Kendaraan',
                    type:'radio',
                    value: ''
                },{
                    fieldName: 'Tanda Terima Kendaraan',
                    type: 'radio',
                    value: ''
                },{
                    fieldName: 'Kwintasi Blangko',
                    type: 'radio',
                    value: ''
                },{
                    fieldName: 'SPK',
                    type: 'radio',
                    value: ''
                },{
                    fieldName: 'KTP Kustomer',
                    type: 'radio',
                    value: ''
                }, {
                    fieldName: 'Kartu Nama Cabang',
                    type: 'radio',
                    value: ''
                },{
                    fieldName: 'Buku Rekening Kustomer',
                    type: 'radio',
                    value: ''
                },{
                    fieldName: 'Surat Kuasa',
                    type: 'radio',
                    value: ''
                },{
                    fieldName: 'Foto Dengan Kustomer',
                    type: 'image',
                    value: ''
                },{
                    fieldName: 'Catatan',
                    type: 'freetext',
                    value: ''
                }
            ],

         };
    }

    takePicture = (field, index) => {

        const options = {
          title: `Pilih ${field.fieldName}`,
          storageOptions: {
            path: "images",
          },
        };
        ImagePicker.showImagePicker(options, response => {
          if (response.didCancel) {
            console.log("Pick image cancelled");
          } else if (response.error) {
            console.log("Error pick image", response.error);
          } else {

            let tempForm = this.state.form

            tempForm[index].value = response.uri

            this.setState({
                form: tempForm
            })

          }
        });
      }

    changeField = (value, index, type) => {
        let tempForm = this.state.form

        if(type === 'radio') {
            if( tempForm[index].value !== value ) {

                tempForm[index].value = value
                this.setState({
                    form: tempForm
                })
            } else {

                tempForm[index].value = ''
                this.setState({
                    form: tempForm
                })
            }
        } else {

            tempForm[index].value = value
            this.setState({
                form: tempForm
            })
        }

    }
    render() {
        let { form } = this.state
        return (
            <Fragment>
                <View style={styles.headerContainer}>
                    <HeaderBackButton tintColor='white' onPress={() => this.props.navigation.navigate("handoverList")} />
                    <Text style={styles.headerTitle}>Handover</Text>
                    <TouchableOpacity >
                        <Text style={{...styles.headerTitle, fontSize: widthPercentageToDP('4%'), marginRight: 10}}>Update</Text>
                    </TouchableOpacity>
                </View>
                <ScrollView>
                    {
                        form.map((field, index) => (
                            <View style={styles.container} key={index}>
                                {
                                    field.type === 'select' ? (
                                        <View>
                                            <Text style={styles.label}>{field.fieldName}</Text>
                                            <View style={styles.inputForm}>
                                                <Picker
                                                    mode="dropdown"
                                                    selectedValue={field.value}
                                                    onValueChange={value => this.changeField(value, index, field.type)}
                                                >
                                                    <Picker.Item label={`Pilih ${field.fieldName}`} value="0" />
                                                </Picker>
                                            </View>
                                        </View>
                                    ) : null
                                }
                                {
                                    field.type === 'radio' ? (
                                        <View>
                                            <Text style={styles.label}>{field.fieldName !== 'Buku Rekening Kustomer' && field.fieldName !== 'Surat Kuasa' ? field.fieldName : `${field.fieldName} (Opsional)`}</Text>
                                            <View style={styles.radioButtonCont}>
                                                <TouchableOpacity style={styles.radioButtonBox} onPress={() => this.changeField('Ada', index, field.type)}>
                                                    <Icons name={ field.value !== 'Ada' ? 'radiobox-blank' : 'radiobox-marked'} color={colors.dope_blue} size={widthPercentageToDP('6.5%')}/>
                                                    <Text style={styles.radioButtonText}>Ada</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity style={styles.radioButtonBox} onPress={() => this.changeField('Tidak Ada', index, field.type)}>
                                                    <Icons name={ field.value !== 'Tidak Ada' ? 'radiobox-blank' : 'radiobox-marked'} color={colors.dope_blue} size={widthPercentageToDP('6.5%')}/>
                                                    <Text style={styles.radioButtonText}>Tidak Ada</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    ) : null
                                }
                                {
                                    field.type === 'image' ? (
                                        <View>
                                            <Text style={styles.label}>{field.fieldName}</Text>
                                            {
                                                field.value === '' ? (
                                                    <TouchableOpacity style={styles.cameraButton} onPress={() => this.takePicture(field, index)}>
                                                        <Icons name='camera' color={colors.light_sapphire_bluish_gray} size={widthPercentageToDP('20%')}/>
                                                        <Text style={{color: colors.light_sapphire_bluish_gray, fontWeight: 'bold'}}>Upload Foto</Text>
                                                    </TouchableOpacity>
                                                ) : (
                                                    <Image source={{uri: field.value}} style={styles.cameraButton}/>
                                                )
                                            }

                                        </View>
                                    ) : null
                                }
                                {
                                    field.type === 'freetext' ? (
                                        <View>
                                            <Text style={styles.label}>{field.fieldName}</Text>
                                            <TextInput
                                                onChangeText={text => this.changeField(text, index, field.type)}
                                                multiline={true}
                                                numberOfLines={4}
                                                style={styles.textArea}
                                                value={field.value}
                                            />
                                        </View>
                                    ) : null
                                }
                            </View>
                        ))
                    }


                </ScrollView>

            </Fragment>
        );
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        paddingVertical: widthPercentageToDP('0.6%'),
        width: widthPercentageToDP('100%'),
        paddingHorizontal: widthPercentageToDP('1.5%'),
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: colors.strong_blue,
        alignItems: 'center'
    },
    headerTitle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: widthPercentageToDP('4.9%'),
        fontFamily: 'Roboto',
    },
    container: {
        paddingVertical: widthPercentageToDP('5%'),
        paddingHorizontal: widthPercentageToDP('5%'),
        backgroundColor: 'white'
    },
    label: {
        color: colors.dope_blue,
        fontFamily: 'Roboto',
        fontSize: widthPercentageToDP('4.6%'),
        marginBottom: widthPercentageToDP('2%')
    },
    inputForm: {
        borderWidth: 1.5,
        borderColor: colors.light_sapphire_bluish_gray,
        borderRadius: 5,
        marginBottom: widthPercentageToDP('0.1%'),
        color: colors.dope_blue
    },
    radioButtonCont: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: widthPercentageToDP('55%')
    },
    radioButtonBox: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    radioButtonText: {
        fontSize: widthPercentageToDP('4.5%'),
        color: colors.dope_blue,
        fontFamily: 'Roboto',
        marginLeft: widthPercentageToDP('1.5%')
    },
    cameraButton: {
        width: widthPercentageToDP('53%'),
        height: widthPercentageToDP('53%'),
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: colors.light_sapphire_bluish_gray,
        borderWidth: 2,
        borderRadius: 5
    },
    textArea: {
        borderColor: colors.light_sapphire_bluish_gray,
        borderWidth: 2,
        borderRadius: 5
    }
})

export default HandoverDetail;