import React, { Component, Fragment } from "react";
import { HeaderBackButton } from "react-navigation-stack";
import { Text, View, ScrollView, TextInput, StyleSheet, TouchableOpacity, Image, ToastAndroid , Alert} from "react-native";
import { widthPercentageToDP } from "../../utils";
import colors from "../../utils/colors";
import ImagePicker from "react-native-image-picker";
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from "../../services/axios";
import DatePicker from "react-native-datepicker";
import moment from "moment";

class HandoverDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			handover:{},
			photo: ""
		};
	}

	componentDidMount() {
		const { navigation } = this.props;
		let bookingId = navigation.getParam("bookingId", "0");
		axios.get(`handover/${bookingId}`)
			.then( res => {
				if(res.data.handover.handoverTime === null) {
					res.data.handover.handoverTime = new Date()
					console.log("kosong")
				} else {
					res.data.handover.handoverTime = moment(res.data.handover.handoverTime)
					console.log(res.data.handover.handoverTime, "udah ada")
				}
				this.setState({ ...res.data})
			})
	}

	takePicture = (field, index) => {
		const { id } = this.state.handover;
		const options = {
			title: `Pilih Foto Dengan Kustomer`,
			storageOptions: {
				path: "images",
			},
		};
		ImagePicker.showImagePicker(options, response => {
			if (response.didCancel) {
				console.log("Pick image cancelled");
			} else if (response.error) {
				console.log("Error pick image", response.error);
			} else {
				this.setState({
					photo: response.uri
				})
				let temp = {
					uri: response.uri,
					type: response.type,
					name: response.fileName,
				}
				let formData = new FormData();
				formData.append("image", temp);
				formData.append("status", "handover")
				axios({
					method: "put",
					url: `handover/${id}/upload`,
					timeout: 60 * 3 * 1000,
					data: formData,
					Headers:{
						'Content-Type': 'multipart/form-data'
					}
				})
					.then(res => {
						this.setState({
							handover: res.data.handover
						})
					})
			}
		});
	}

	changeField = (value, index, type) => {
		let tempForm = this.state.handover.misc;

		if(type === 'radio') {
			tempForm[index].value = value
			this.setState({
					form: tempForm
			})
		} else {
			let handover = this.state.handover;
			handover[type] = value;
			this.setState({
				handover
			})
		}
	}


	onUpdate = () => {
		const { handover } = this.state;
		let payload = {
			handoverTime : handover.handoverTime,
			misc: handover.misc,
			location: handover.location,
			driverNotes: handover.driverNotes
		}

		console.log(payload, "handover update")

		Alert.alert(
			"Finish Handover",
			"Selesaikan Handover",
			[
				{ text: "Cancel", onPress: () => console.log("cancel save data") },
				{ text: "Deal", onPress: () => this.finishedHandover(handover.id, {...payload, deal: true}) },
				{ text: "No Deal", onPress: () => this.finishedHandover(handover.id, {...payload, deal: false}) },
			],
			{ cancelable: false },
		);
	}

	finishedHandover = async (handoverId, payload) => {
		try {
			let res = await axios.put(`/handover/${handoverId}/finished`, payload);
			this.props.navigation.navigate("handoverList");
			ToastAndroid.showWithGravity("Anda telah menyelesaikan handover", 2000, ToastAndroid.CENTER);
		} catch (e) {
			console.log(e)
			ToastAndroid.showWithGravity(e.message, 2000, ToastAndroid.CENTER)
		}
	}

	render() {
		let { handover } = this.state
		return (
			<Fragment>
				<View style={styles.headerContainer}>
					<HeaderBackButton tintColor='white' onPress={() => this.props.navigation.navigate("handoverList")} />
					<Text style={styles.headerTitle}>Handover</Text>
					<TouchableOpacity onPress={() => this.onUpdate()}>
						<Text style={{...styles.headerTitle, fontSize: widthPercentageToDP('4%'), marginRight: 10}}>Update</Text>
					</TouchableOpacity>
				</View>
				{handover.id ? (
					<ScrollView>
						<View style={styles.container}>
							<Text style={styles.label}>Salesman</Text>
							<Text style={{fontSize: 18, color: colors.dope_blue, fontWeight: "bold", marginLeft: 10}}>{handover.Booking.SalesProfile.name}</Text>
						</View>
						<View style={styles.container}>
							<Text style={styles.label}>Cabang</Text>
							<Text style={{fontSize: 18, color: colors.dope_blue, fontWeight: "bold", marginLeft: 10}}>{handover.Booking.SalesProfile.branch}</Text>
						</View>

						<View style={styles.container}>
							<Text style={styles.label}>Hari/Tanggal Handover </Text>
							<View style={styles.inputForm}>
								<DatePicker
										date={handover.handoverTime}
										style={{ width: "100%" }}
										locale="id"
										mode="datetime"
										androidMode="spinner"
										placeholder="Pilih tanggal"
										format="dddd, DD-MM-YYYY HH:mm"
										confirmBtnText="Oke"
										cancelBtnText="Batal"
										minuteInterval={15}
										onDateChange={date => {
											handover["handoverTime"] = moment(date, "dddd, DD-MM-YYYY HH:mm");
											this.setState({ handover })
										}}
										customStyles={{
											dateInput: {
												borderWidth: 0,
											},
											placeholderText: {
												color: "#CAD9E4",
											},
											dateText: {
												color: "#172344",
												fontSize: 14,
											},
										}}
									/>
							</View>
						</View>

						<View style={styles.container}>
							<Text style={styles.label}>Lokasi</Text>
							<TextInput
									onChangeText={text => this.changeField(text, "", "location")}
									style={styles.textArea}
									multiline={true}
									numberOfLines={2}
									value={handover.location}
								/>
						</View>

						{handover.misc.map((misc, index) => (
							<View style={styles.container} key={index}>
								<Text style={styles.label}>{misc.fieldname !== 'Buku Rekening Kustomer' && misc.fieldname !== 'Surat Kuasa' ? misc.fieldname : `${misc.fieldname} (Opsional)`}</Text>
								<View style={styles.radioButtonCont}>
									<TouchableOpacity style={styles.radioButtonBox} onPress={() => this.changeField('yes', index, "radio")}>
										<Icons name={ misc.value !== 'yes' ? 'radiobox-blank' : 'radiobox-marked'} color={colors.dope_blue} size={widthPercentageToDP('6.5%')}/>
										<Text style={styles.radioButtonText}>Ada</Text>
									</TouchableOpacity>

									<TouchableOpacity style={styles.radioButtonBox} onPress={() => this.changeField('no', index, "radio")}>
										<Icons name={ misc.value !== 'no' ? 'radiobox-blank' : 'radiobox-marked'} color={colors.dope_blue} size={widthPercentageToDP('6.5%')}/>
										<Text style={styles.radioButtonText}>Tidak Ada</Text>
									</TouchableOpacity>
								</View>
							</View>
						))}

						<View style={styles.container}>
							<Text style={styles.label}>Foto Dengan Kustomer</Text>
							{
								this.state.photo !== "" ? (
									<Fragment>
										<Image source={{uri: this.state.photo}} style={styles.cameraButton}/>
										<TouchableOpacity style={styles.changePhoto} onPress={() => this.takePicture()}>
											<Text style={{textAlign: "center", color: "white", fontWeight: "bold", fontSize: 16}}>Ubah Foto</Text>
										</TouchableOpacity>
									</Fragment>
								) :handover.photo === null || handover.photo === '' ? (
									<TouchableOpacity style={styles.cameraButton} onPress={() => this.takePicture()}>
										<Icons name='camera' color={colors.light_sapphire_bluish_gray} size={widthPercentageToDP('20%')}/>
										<Text style={{color: colors.light_sapphire_bluish_gray, fontWeight: 'bold'}}>Upload Foto</Text>
									</TouchableOpacity>
								) : (
									<Fragment>
										<Image source={{uri: handover.photo}} style={styles.cameraButton}/>
										<TouchableOpacity style={styles.changePhoto} onPress={() => this.takePicture()}>
											<Text style={{textAlign: "center", color: "white", fontWeight: "bold", fontSize: 16}}>Ubah Foto</Text>
										</TouchableOpacity>
									</Fragment>
								)
							}
						</View>

						<View style={styles.container}>
							<Text style={styles.label}>Catatan</Text>
							<TextInput
									onChangeText={text => this.changeField(text, "", "driverNotes")}
									multiline={true}
									numberOfLines={4}
									style={styles.textArea}
									value={handover.driverNotes}
								/>
						</View>
					</ScrollView>
				) : null}
			</Fragment>
		);
	}
}

const styles = StyleSheet.create({
    headerContainer: {
        paddingVertical: widthPercentageToDP('0.6%'),
        width: widthPercentageToDP('100%'),
        paddingHorizontal: widthPercentageToDP('1.5%'),
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: colors.strong_blue,
        alignItems: 'center'
    },
    headerTitle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: widthPercentageToDP('4.9%'),
        fontFamily: 'Roboto',
    },
    container: {
        paddingVertical: widthPercentageToDP('5%'),
        paddingHorizontal: widthPercentageToDP('5%'),
        backgroundColor: 'white'
    },
    label: {
        color: colors.dope_blue,
        fontFamily: 'Roboto',
        fontSize: widthPercentageToDP('4.6%'),
        marginBottom: widthPercentageToDP('2%')
    },
    inputForm: {
        borderWidth: 1.5,
        borderColor: colors.light_sapphire_bluish_gray,
        borderRadius: 5,
        marginBottom: widthPercentageToDP('0.1%'),
        color: colors.dope_blue
    },
    radioButtonCont: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: widthPercentageToDP('55%')
    },
    radioButtonBox: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    radioButtonText: {
        fontSize: widthPercentageToDP('4.5%'),
        color: colors.dope_blue,
        fontFamily: 'Roboto',
        marginLeft: widthPercentageToDP('1.5%')
    },
    cameraButton: {
        width: widthPercentageToDP('53%'),
        height: widthPercentageToDP('53%'),
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: colors.light_sapphire_bluish_gray,
        borderWidth: 2,
        borderRadius: 5
    },
    textArea: {
        borderColor: colors.light_sapphire_bluish_gray,
        borderWidth: 2,
        borderRadius: 5
		},
		changePhoto: {
			marginTop: 10,
			backgroundColor: colors.dope_blue,
			width: widthPercentageToDP('53%'),
			padding: 10,
			borderRadius: 5
		}
})

export default HandoverDetail;