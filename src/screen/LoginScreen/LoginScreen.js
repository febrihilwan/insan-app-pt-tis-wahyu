/* eslint-disable no-alert */
import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import _ from 'lodash';
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  ToastAndroid,
  ActivityIndicator,
} from 'react-native';

import {doAuth, authLocalJWT} from '../../actions';
import {widthPercentageToDP, heightPercentageToDP} from '../../utils';
import Colors from '../../utils/colors';

import logo from '../../assets/image/logo-insan.png';
import Spinner from 'react-native-loading-spinner-overlay';
import {get} from 'lodash';
import checkFields from '../../utils/checkFields';

class LoginScreen extends React.Component {
  state = {
    activeTab: 'signin',
    username: '',
    password: '',
    spinner: false,
  };
  onChangeInput(text, name) {
    this.setState({
      [name]: text,
    });
  }

  onClickLogin() {
    const {username, password} = this.state;
    const arryForm = [
      {name: 'Username', value: username, required: true},
      {name: 'Password', value: password, required: true},
    ];

    const checkForm = checkFields(arryForm);
    const isEmptyForm = _.size(checkForm) === 0;

    if (isEmptyForm) {
      this.props.doAuth({username, password});
    } else {
      const messageText = checkForm.map((itemField) => {
        return `Field ${itemField.name} wajib di isi.\n`;
      });
      alert(messageText.join(''));
    }
  }

  async componentDidMount() {
    // ------ [START FOR AUTOMATE LOGIN] -------
    // try {
    //   let userData = await AsyncStorage.getItem('userData');
    //   console.log('userData', userData);
    //   if (userData) {
    //     userData = JSON.parse(userData);
    //     this.props.authLocalJWT(userData);
    //   }
    // } catch (err) {
    //   console.log(err);
    //   console.log('Failed local login');
    //   ToastAndroid.showWithGravity('Gagal Login', 2000, ToastAndroid.BOTTOM);
    // }
    // ------ [END FOR AUTOMATE LOGIN] -------
  }

  componentDidUpdate(prevProps) {
    const {Auth} = this.props;
    if (prevProps.Auth.authRequest) {
      if (Auth.isAuthenticated) {
        const role = get(Auth, 'api_token.role');

        if (role === 'Teknisi') {
          this.props.navigation.navigate('App');
        } else {
          this.props.navigation.navigate('CS');
        }
      }

      if (Auth.error) {
        ToastAndroid.showWithGravity('Gagal Login', 2000, ToastAndroid.BOTTOM);
        // this.setState({
        //   spinner: false,
        // });
      }
    }
  }

  render() {
    const {Auth} = this.props;
    const {isload} = Auth;

    return (
      <ScrollView>
        <View style={styles.container}>
          <View>
            <Image source={logo} style={styles.logo} />
          </View>

          <View style={styles.loginContainer}>
            <Text style={styles.label}>Username</Text>
            <TextInput
              value={this.state.username}
              autoCapitalize="none"
              onChangeText={(text) => this.onChangeInput(text, 'username')}
              style={styles.loginInput}
            />

            <Text style={styles.label}>Password</Text>
            <TextInput
              value={this.state.password}
              autoCapitalize="none"
              onChangeText={(text) => this.onChangeInput(text, 'password')}
              style={styles.loginInput}
              secureTextEntry
            />

            <TouchableOpacity
              onPress={() => this.onClickLogin()}
              style={styles.loginButton}>
              {isload && (
                <ActivityIndicator
                  color="#fff"
                  animating={isload}
                  style={{marginRight: 10}}
                />
              )}
              <Text style={styles.loginText}>Login</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = {
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    height: heightPercentageToDP('100%'),
  },
  logo: {
    width: widthPercentageToDP('60%'),
    height: heightPercentageToDP('10%'),
    marginBottom: heightPercentageToDP('10%'),
  },
  header: {
    color: Colors.strong_blue,
    fontFamily: 'Roboto',
    fontSize: widthPercentageToDP('7%'),
    marginBottom: heightPercentageToDP('2%'),
  },
  subHeader: {
    color: Colors.dope_blue,
    fontFamily: 'Roboto',
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('5%'),
  },
  loginContainer: {
    paddingHorizontal: widthPercentageToDP('5%'),
    borderWidth: 0,
    width: widthPercentageToDP('100%'),
  },
  label: {
    color: Colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  loginInput: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: Colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  loginButton: {
    backgroundColor: Colors.strong_orange,
    marginTop: widthPercentageToDP('4%'),
    borderRadius: 6,
    borderColor: Colors.strong_orange,
    borderWidth: 2,
    width: '100%',
    paddingVertical: widthPercentageToDP('2%'),
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  loginText: {
    color: '#fff',
    fontWeight: '800',
    textAlign: 'center',
    fontSize: widthPercentageToDP('5%'),
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
};

const mapStateToProps = ({Auth}) => ({Auth});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({doAuth, authLocalJWT}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
