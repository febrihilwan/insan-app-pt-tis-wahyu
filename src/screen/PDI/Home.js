import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {View, Text, StyleSheet, FlatList, ToastAndroid, ScrollView} from 'react-native';
import axios from '../../services/axios';
import colors from '../../utils/colors';
import Button from '../../component/Button';
import Icon from '../../component/Icon';
import {fetchAllPDI, fetchDetailPDI} from '../../actions';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      PDIList:[],
      dataReady: false,
      type: null,
      edited: 0,
      nonEdited: 0
    }
    this.navigationWillFocusListener = props.navigation.addListener(
      'willFocus',
      async () => {
        const {user} = await this.props.Auth;
        await this.props.fetchAllPDI(user.id)
        await this.fetchCount()
      },
    );
  }

  componentWillUnmount() {
    const {user} = this.props.Auth;
    this.props.fetchAllPDI(user.id)
    this.fetchPDIList()
  }

  componentDidMount() {
    const {user} = this.props.Auth;
    this.props.fetchAllPDI(user.id)
    this.fetchPDIList()
    this.fetchCount()
  }

  componentDidUpdate(prevProps) {
    const {PDI} = this.props;
    console.log("PDI ", PDI)
    if (prevProps.PDI.onRequestDetailPDI) {
      console.log(prevProps.PDI.onRequestDetailPDI)
      if (PDI.successRequestDetailDI && !PDI.onRequestDetailPDI) {
        if(this.state.type == "edit"){
          this.props.navigation.navigate('EditMenu');
        } else if(this.state.type == "preview"){
          this.props.navigation.navigate('PreviewMenu');
        }
      } else if (PDI.errorRequestDetailPDI) {
        ToastAndroid.showWithGravity(
          'Gagal menampilkan detail PDI',
          2000,
          ToastAndroid.BOTTOM,
        );
      } else {
        console.log('Something else ?', PDI);
      }
    }
  }

  fetchPDIList() {
    const {user} = this.props.Auth;
    axios
      .get(`/pdi/${user.id}/list`)
      .then(result => {
        this.setState({
          PDIList: result.data.carInspections,
          dataReady: true
        });
      })
      .catch(err => {
        ToastAndroid.showWithGravity(
          'Gagal menampilkan list PDI',
          2000,
          ToastAndroid.BOTTOM,
        );
      });
  }

  fetchCount(){
    const {user} = this.props.Auth;

    axios
      .get(`/pdi/${user.id}/status_counter`)
      .then(result => {
        console.log("DATA ", result)
        this.setState({
          edited: result.data.inspectionsEdited.count,
          nonEdited : result.data.inspectionsUnEdited.count
        })
      })
  }

  _renderCarList = ({item, index}) => {
    const {navigation} = this.props;
    return (
      <View style={styles.carListContainer}>
        <View style={styles.carListHeader}>
          <Text style={{color: colors.strong_blue}}>
            {item.Booking.noBooking}
          </Text>
        </View>
        <View style={styles.carListContent}>
          <Text style={{fontSize: 16, fontWeight: 'bold'}}>{item.brand} {item.model} {item.type}</Text>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <View style={{width: '70%'}}>
              <Text style={{fontSize: 14, marginBottom: 5, fontWeight: 'bold'}}>
                {item.carDetail[7].value}
              </Text>
              <Text style={{fontSize: 12, color: colors.grayish_blue}}>
                {item.createdAt}
              </Text>
            </View>
            <View
              style={{
                width: '30%',
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Button onPress={() => 
                {
                  this.setState({
                    type: "preview"
                  })
                  this.props.fetchDetailPDI(item.id)
                }
              }>
                <Icon name="eye" />
              </Button>
              <Button
                onPress={() => 
                {
                  this.setState({
                    type: "edit"
                  })
                  this.props.fetchDetailPDI(item.id)
                }
              }>
                <Icon name="pencil" />
              </Button>
            </View>
          </View>
        </View>
      </View>
    );
  };

  render() {
    const {dataReady, PDIList, edited, nonEdited} = this.state
    const {PDI} = this.props
    return (
      <View style={styles.content}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            margin: 15,
          }}>
          <Board quantity={nonEdited} description="BELUM DICEK" />
          <Board quantity={edited} description="SUDAH DICEK" />
        </View>
        <View style={{marginTop: 10}}>
          {
            PDI.ListPDI 
            ? (
              <ScrollView>
                {
                  PDI.ListPDI.map((pdi, index) => this._renderCarList({item: pdi, index}))
                }
              </ScrollView>
            )
          :null
          }
          
        </View>
      </View>
    );
  }

 
}

const Board = ({quantity, description}) => {
  return (
    <View style={styles.boardContainer}>
      <Text style={styles.boardQuantity}>{quantity}</Text>
      <Text style={styles.boardDescription}>{description}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    // padding: 15,
  },
  boardContainer: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
    padding: 15,
    width: ' 48%',
  },
  boardQuantity: {
    fontSize: 20,
    color: colors.strong_blue,
    fontWeight: 'bold',
  },
  boardDescription: {
    fontSize: 12,
    // color: Colors.primary,
    fontWeight: 'bold',
  },
  carListContainer: {
    backgroundColor: 'white',
    borderRadius: 3,
    marginHorizontal: 15,
    marginBottom: 10,
    overflow: 'hidden',
    elevation: 3,
    borderColor: 'darkgrey',
    borderBottomWidth: 0,
    shadowColor: 'darkgrey',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.1,
    shadowRadius: 1,
  },
  carListHeader: {
    paddingVertical: 5,
    paddingHorizontal: 10,
    backgroundColor: '#EBF3FF',
  },
  carListContent: {
    padding: 10,
  },
});

const mapStateToProps = ({Auth, PDI}) => ({
  Auth,
  PDI
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({fetchAllPDI, fetchDetailPDI}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);