import React from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Spinner from "react-native-loading-spinner-overlay";
import RNFetchBlob from 'rn-fetch-blob'
import _ from 'lodash';
import colors from '../../../utils/colors';
import data from '../../../../dummy-pdi.json';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { setPDIData, getData } from '../../../reducers/PDIData'
import Button from '../../../component/Button';
import {widthPercentageToDP} from '../../../utils';
import {updatePDIData} from '../../../actions/pdi_actions';
import UploadModal from "../../../component/Modal/UploadModalPreview";
import PreviewImage from "../../../component/Modal/PreviewImage";
import InputPhoto from "../../../component/PDI/InputPhoto";

class Document extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      viewImage: false,
      photoData: [],
      displayPhotoModal: false,
      document: {},
      modalVisible: false,
    };
    this._onPressPhoto = this._onPressPhoto.bind(this);
    this._togglePhotoModal = this._togglePhotoModal.bind(this);
    this._onChangeText = this._onChangeText.bind(this);
    this._onPressUpload = this._onPressUpload.bind(this);
    this._showImage = this._showImage.bind(this);
    this._closeImage = this._closeImage.bind(this);
    
  }

  componentDidMount() {
    this.setState({
      photoData: this.props.PDI.DetailPDI.photoDocument,
    });
  }

  _onPressPhoto(docData) {
    console.log('itaem', docData);
    
    this.setState({
      displayPhotoModal: true,
      document: docData,
    });
  }

  _togglePhotoModal() {
    this.setState({
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _showImage(imageUrl) {
    console.log("open image", imageUrl);
    this.setState({
      imageLink: imageUrl,
      viewImage: !this.state.viewImage,
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _closeImage() {
    console.log("close image");
    this.setState({
      viewImage: !this.state.viewImage,
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _onPressUpload(imageData) {
    const { PDI, PDIData } = this.props;
    let formData = []
    formData.push({name: "image", filename: imageData.name, type: imageData.type, data: RNFetchBlob.wrap(imageData.path)});
    formData.push({name: "field", data: "photoDocument"});
    formData.push({name: "objectField", data: JSON.stringify(this.state.document)});
    console.log("DATANYA FORM ", formData)


    let {data} = PDIData
    let isExistData = _.find(data, {objectField: {name: this.state.document.name}})

    if(isExistData){
      let idxExistData = _.find(data, {objectField: {name: this.state.document.name}})
      data.splice(idxExistData, 1)
    }
    let newData = {
      image: imageData,
      field: "photoDocument",
      objectField: this.state.document
    }
    data.push(newData)
    setPDIData(data);
    let docs = _.find(PDI.DetailPDI.photoDocument, {name: this.state.document.name})
    docs.imageUrl = imageData.uri

    this.setState({
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _onChangeText(text, name) {
    const { photoData, document } = this.state;
    this.setState({
      photoData: photoData.map(photo => (photo.name === name ? { ...photo, notes: text } : photo)),
      document: { ...document, notes: text },
    });
  }

  

  render() {
    const { displayPhotoModal, document, photoData } = this.state;
    const { PDI } = this.props;
    console.log("::DETAIL REDUX Doc",this.props.PDI.DetailPDI)
    console.log("::PDI DATA REDUX Doc",this.props.PDIData)
    return (
      <View style={{flex: 1}}>
        {displayPhotoModal ? (
          <UploadModal
            document={document}
            togglePhotoModal={this._togglePhotoModal}
            displayPhotoModal={displayPhotoModal}
            onChangeText={this._onChangeText}
            onPressUpload={this._onPressUpload}
            previewPhoto={this._showImage}
          />
        ) : (
          false
        )}

        <PreviewImage
          closeImage={this._closeImage}
          viewImage={this.state.viewImage}
          imageLink={this.state.imageLink}
        />

        <View style={styles.container}>
          <FlatList
              renderItem={({ item, index }) => {
                return (
                  <InputPhoto onPressPhoto={() => this._onPressPhoto(item)} key={index} photo={item} />
                )}
              }
              data={this.props.PDI.DetailPDI.photoDocument}
              keyExtractor={item => item.name}
              extraData={this.state}
            />
        </View>
      </View>
      
    );
  }

  _renderItems = ({item, index}) => {
    return (
      <View style={styles.cardList}>
        <View>
          <Text style={styles.textTitle}>{item.name}</Text>
        </View>
        <View>
          <Button
            style={styles.button}
            color={colors.strong_blue}
            onLayout={event => {
              var {x, y, width, height} = event.nativeEvent.layout;
              if (index == 0) {
                this.setState({
                  buttonWidth: width,
                  buttonHeight: height,
                });
              }
            }}>
            <Icon name="camera" size={20} color="white" />
          </Button>
        </View>
      </View>
    );
  };
}

const mapStateToProps = ({Auth, PDI, PDIData}) => ({
  Auth,
  PDI,
  PDIData
});
const mapDispatchToProps = dispatch => bindActionCreators({ updatePDIData }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Document);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingLeft: widthPercentageToDP('3%'),
  },
  containerModal: {
    flex: 1,
  },
  cardList: {
    paddingVertical: 10,
    borderBottomColor: '#eee',
    borderBottomWidth: 2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: 10,
    alignItems: 'center',
  },
  textTitle: {
    // fontSize: 14,
    fontWeight: 'bold',
  },
  button: {
    width: 40,
    height: 40,
    // alignItems: "flex-start",
    justifyContent: 'center',
    alignItems: 'center',
  },
});
