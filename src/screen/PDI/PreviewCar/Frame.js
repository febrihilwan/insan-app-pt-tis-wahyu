import React from 'react';
import {View, Text, StyleSheet, FlatList, ToastAndroid} from 'react-native';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {bindActionCreators} from 'redux';
import Spinner from "react-native-loading-spinner-overlay";
import RNFetchBlob from 'rn-fetch-blob';
import _ from 'lodash';

import { setPDIData, getData } from '../../../reducers/PDIData';
import { updatePDIData } from '../../../reducers/PDI';
import UploadModal from "../../../component/Modal/UploadModalPreview";
import PreviewImage from "../../../component/Modal/PreviewImage";
import colors from '../../../utils/colors';
import data from '../../../../dummy-pdi.json';
import Button from '../../../component/Button';
import {widthPercentageToDP} from '../../../utils';
import InputFrame from "../../../component/PDI/Frame/InputFramePreview";
class Frame extends React.Component {
  constructor(props) {
    super(props);
    this._onPressPhoto = this._onPressPhoto.bind(this);
    this.state = {
      viewImage: false,
      photoData: [],
      displayPhotoModal: false,
      photoFrame: this.props.PDI.DetailPDI,
      document: {},
    };
    this._togglePhotoModal = this._togglePhotoModal.bind(this);
    this._onChangeText = this._onChangeText.bind(this);
    this._onPressUpload = this._onPressUpload.bind(this);
    this._showImage = this._showImage.bind(this);
    this._closeImage = this._closeImage.bind(this);
    this.updateData = this.updateData.bind(this);
  }

  componentDidMount() {
    this.setState({
      photoData: this.props.PDI.DetailPDI.photoDocument,
    });
  }

  componentDidUpdate(prevProps) {
    const { PDI } = this.props;
    if (!prevProps.PDI.message && !!PDI.message) {
      ToastAndroid.showWithGravity(`${PDI.message}`, 3000, ToastAndroid.CENTER);
    }
  }


  _onPressPhoto(dataFrame) {

    console.log('item', typeof (dataFrame.imageUrl));
    
    this.setState({
      displayPhotoModal: true,
      document: dataFrame,
    });
  }

  _showImage(imageUrl) {
    console.log("open image", imageUrl);
    this.setState({
      imageLink: imageUrl,
      viewImage: !this.state.viewImage,
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _closeImage() {
    console.log("close image");
    this.setState({
      viewImage: !this.state.viewImage,
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _takePicture(field) {
    const options = {
      title: `Pilih ${field}`,
      storageOptiond: {
        path: "images",
      },
    };

    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log("Pick image cancelled");
      } else {
        this.setState({
          [field]: {
            uri: response.uri,
            type: response.type,
            name: field,
          },
        });
      }
    });
  }

  _onChangeText(text, name) {
    const { photoData, document } = this.state;
    this.setState({
      photoData: photoData.map(photo => (photo.name === name ? { ...photo, notes: text } : photo)),
      document: { ...document, notes: text },
    });
  }

  _togglePhotoModal() {
    this.setState({
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _onPressUpload(imageData) {
    const { PDI, PDIData } = this.props;
    let formData = new FormData();
    formData.append("image", imageData);
    formData.append("field", "photoFrame");
    formData.append("objectField", JSON.stringify(this.state.document));

    let {data} = PDIData;
    let isExistData = _.find(data, {objectField: {name: this.state.document.name}})

    if(isExistData){
      let idxExistData = _.find(data, {objectField: {name: this.state.document.name}})
      data.splice(idxExistData, 1)
    }
    let newData = {
      image: imageData,
      field: "photoFrame",
      objectField: this.state.document
    }
    data.push(newData)
    setPDIData(data);
    let docs = _.find(PDI.DetailPDI.photoFrame, {name: this.state.document.name})
    docs.imageUrl = imageData.uri
    this.setState({
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  updateData(field, fieldObjectIndex, fieldObject) {
    this.props.updatePDIData(field, fieldObjectIndex, fieldObject);
  }

  render() {
    const { displayPhotoModal, document } = this.state;
    const { PDI } = this.props
    return (
      <View style={styles.container}>
        {displayPhotoModal ? (
          <UploadModal
            document={document}
            togglePhotoModal={this._togglePhotoModal}
            displayPhotoModal={displayPhotoModal}
            onChangeText={this._onChangeText}
            onPressUpload={this._onPressUpload}
            previewPhoto={this._showImage}
          />
        ) : (
          false
        )}

        <PreviewImage
          closeImage={this._closeImage}
          viewImage={this.state.viewImage}
          imageLink={this.state.imageLink}
        />
        <FlatList
          renderItem={({ item, index }) => (
            <InputFrame
              onPressPhoto={() => this._onPressPhoto(item)}
              index={index}
              updateData={this.updateData}
              key={index}
              photo={item}
            />
          )}
          data={this.props.PDI.DetailPDI.photoFrame}
          keyExtractor={item => item.name}
          extraData={this.state}
        />
      </View>
    );
  }
}


const mapStateToProps = ({Auth, PDI, PDIData}) => ({
  Auth,
  PDI,
  PDIData
});
const mapDispatchToProps = dispatch => bindActionCreators({ updatePDIData }, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Frame);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingLeft: widthPercentageToDP('3%'),
  },
  cardList: {
    paddingVertical: 10,
    borderBottomColor: '#eee',
    borderBottomWidth: 2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: 10,
    alignItems: 'center',
  },
  textTitle: {
    // fontSize: 14,
    // fontWeight: 'bold',
  },
  button: {
    width: 40,
    height: 40,
    // alignItems: "flex-start",
    justifyContent: 'center',
    alignItems: 'center',
  },
});
