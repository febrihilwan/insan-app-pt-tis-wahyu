import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {HeaderBackButton} from 'react-navigation-stack'
import {View, Text, ScrollView, StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import _ from 'lodash';
import { widthPercentageToDP, heightPercentageToDP } from "../../../utils";
import Label from '../../../component/Label';
import FormSelect from '../../../component/FormSelect';
import axios from "../../../services/axios";
import Colors from '../../../utils/colors';
import data from '../data';
import {updatePDIData} from '../../../actions/pdi_actions'

class EditSummary extends React.Component {
  constructor() {
    super();
    this.state = {
      Brand: [{ brand: "Brand tidak ada di list", id: "" }],
      Model: [{ model: "model tidak ada di list", id: "" }],
      Tipe: [{ tipe: "tipe tidak ada di list", id: "" }],
      KategoriWarna: [],
      selectedBrand: null,
      selectedBrandId: null,
      selectedModel: null,
      selectedModelId: null,
      selectedType: null,
      selectedTypeId: null,
      selectedTahun: null,
      selectedWarna: null,
      selectedSTNKHidup: null,
      NamaSTNK: null,
      NilaiSTNK: null,
      selectedBanjir: null,
      selectedKecelakaan: null,
      selectedWheel: null,
      selectedPaint: null,
      selectedSTNK: null,
      selectedInterior: null,
      selectedNIK: null,
      selectedFaktur: null,
      selectedKunci: null,
      KunciSerep: null,
      carDetail:{},
      

    };
  }

  async componentDidMount() {
    await this.fetchBrand();
    await this.fetchColor();
    await this.setState({
      carDetail: this.props.PDI.DetailPDI,
    });

    console.log("DETAIL ", this.props.PDI.DetailPDI)

    await this.setValue();

  }

  async setValue(){
    const { PDI } = this.props
    const { Brand, KategoriWarna } = this.state
    const summary = PDI.DetailPDI.carSummary
    const selectedBrand = _.find(Brand, {id: summary[0].value})

    if(selectedBrand){
      this.setState({
        selectedBrandId: selectedBrand.id,
        selectedBrand: selectedBrand.brand
      })

      this.fetchType(selectedBrand.id, true);
    }

    const nilai = _.find(summary, { fieldName: "Nilai STNK" })
    const nama = _.find(summary, { fieldName: "Nama STNK" })
    const color = _.find(summary, { fieldName: "Kategori Warna" })

    this.setState({
      selectedTahun: this.getValue(data.Tahun, 'Tahun'),
      selectedSTNKHidup: this.getValue(data.STNK_hidup, 'STNK Hidup'),
      selectedBanjir: this.getValue(data.Banjir, 'Banjir'),
      selectedKecelakaan: this.getValue(data.Kecelakaan, 'Kecelakaan'),
      selectedWheel: this.getValue(data.Wheel, 'Wheel'),
      selectedPaint: this.getValue(data.Paint, 'Paint'),
      selectedSTNK: this.getValue(data.STNK, 'STNK'),
      selectedInterior: this.getValue(data.Kebersihan_interior, 'Kebersihan Interior'),
      selectedNIK: this.getValue(data.NIK, 'NIK/Form A'),
      selectedFaktur: this.getValue(data.Faktur, 'Faktur'),
      selectedKunci: this.getValue(data.Kunci_serep, 'Kunci Serep'),
      selectedWarna: color && color.value,
      NilaiSTNK: nilai && nilai.value,
      NamaSTNK: nama && nama.value,
    })
    

  }

  getValue(data, key){
    const { PDI } = this.props
    const summary = PDI.DetailPDI.carSummary
    let a = _.find(summary, { fieldName: key })
    let b = _.find(data, { id: a.value })

    if(b){
      return b.label
    } else {
      return null
    }
  }

  fetchBrand = async () => {
    await axios
      .get("/value-car/brand")
      .then(result => {
        let carBrandList = result.data.carBrand;
        console.log(carBrandList, "brand")
        this.setState({
          Brand: [...carBrandList],
          Model: [],
          Tipe: [],
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  fetchType = (id, hasParent = false) => {
    this.setState({
      selectedModel: null,
      selectedType: null,
      selectedModelId: null
    })
    axios
      .get(`/value-car/brand/${id}`)
      .then(result => {
        console.log(result.data.carType, "model")
        this.setState({
          Model: [...result.data.carType, { carType: "Pilih Model", id: ""}],
          Tipe: [],
        });
        if(hasParent){
          const selectedModel = _.find(this.state.Model, {id: this.state.carDetail.carSummary[1].value})
          if(selectedModel){
            this.setState({
              selectedModelId: selectedModel.id,
              selectedModel: selectedModel.carType
            })
            this.fetchSpec(this.state.carDetail.carSummary[1].value, true)
          }
          
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  fetchSpec = (id, hasParent = false) => {
    this.setState({
      selectedType: null,
      selectedTypeId: null
    })
    axios
      .get(`/value-car/type/${id}`)
      .then(result => {
        this.setState({
          Tipe: [...result.data.carSpec, { carSpec: "Pilih Tipe", id: "" }],
        });

        if(hasParent){
          const selected = _.find(this.state.Tipe, {id: this.state.carDetail.carSummary[2].value})
          console.log("selected ", selected)
          if(selected){
            this.setState({
              selectedTypeId: selected.id,
              selectedType: selected.carSpec
            })
          }
          
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  fetchColor = () => {
    axios
      .get(`/misc/colors`)
      .then(result => {
        this.setState({
          KategoriWarna: result.data.kategori_warna,
        });
        console.log(result, "COLOR")
      })
      .catch(err => {
        console.log(err);
      });
  };

  changeSelected(fieldName, value, text=""){
    let { carDetail } = this.state
    let { updatePDIData } = this.props
    let data = _.find(carDetail.carSummary, { fieldName })
    let idx = _.findIndex(carDetail.carSummary, { fieldName })

    updatePDIData("carSummary", idx, {
      fieldName: fieldName,
      value: value,
      type: "select"
    });

    if(fieldName == "Brand"){
      var fieldDetail = {
        fieldName: "Merk Mobil",
        value: text,
        type: "select"
      }

      this.onChangeFieldDetail(3, fieldDetail, text)
    } else if(fieldName == "Model"){
      var fieldDetail = {
        fieldName: "Model",
        value: text,
        type: "text"
      }

      this.onChangeFieldDetail(4, fieldDetail, text)
    } else if(fieldName == "Tipe"){
      var fieldDetail = {
        fieldName: "Type",
        value: text,
        type: "text"
      }

      this.onChangeFieldDetail(5, fieldDetail, text)
    }

    
    if(data) data.value = value
  }

  onChangeFieldDetail(index, field, value) {
    const { updatePDIData } = this.props;
    if ( field.fieldName === "Nilai STNK" || field.fieldName === "Penawaran Sebelumnya" || field.fieldName === "Ekspektasi Customer") {
      let newValue = value.split(".").join("")
      field.value = Number(newValue).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
    } else {
      field.value = value;
    }
    console.log("oit", field)
    updatePDIData("carDetail", index, field);
    // console.log("oit", value)
  }

  changeText(fieldName, value, type){
    let { carDetail } = this.state
    let idx = _.findIndex(carDetail.carSummary, { fieldName })
    updatePDIData("carSummary", idx, {
      fieldName: fieldName,
      value: value,
      type: type
    });

    let data = _.find(carDetail.carSummary, { fieldName })
    if(data) data.value = value
  }

  render() {
    const {Brand, Model, Tipe, KategoriWarna, carDetail } = this.state
    // console.log("::DETAIL ",carDetail)
    // console.log("::DETAIL REDUX ",this.props.PDI.DetailPDI.carSummary)
    return (
      <React.Fragment>
        <View style={styles.headerContainer}>
          <HeaderBackButton tintColor='white' onPress={() => this.props.navigation.navigate("HomeMenu")} />
          <Text style={styles.headerTitle}>Input Detail Mobil</Text>
          <TouchableOpacity onPress={() => this.props.navigation.navigate("TabDetail")}>
            <Text style={{...styles.headerTitle, fontSize: widthPercentageToDP('4%'), marginRight: 10}}>Simpan</Text>
          </TouchableOpacity>
        </View>
        <ScrollView>
        <View style={styles.content}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="Brand *" />
              <FormSelect
                label='brand'
                selectedValue={this.state.selectedBrand}
                items={Brand}
                onSelectedItem={({text, id}) =>
                  {
                    this.setState({selectedBrand: text}, () => this.fetchType(id))
                    this.changeSelected('Brand', id, text)
                  }
                }
              />
            </View>
            <View
              style={{
                width: '49%',
              }}>
              <Label text="Model *" />
              <FormSelect
                label='carType'
                selectedValue={this.state.selectedModel}
                items={this.state.Model}
                onSelectedItem={({text, id}) =>
                  {
                    this.setState({selectedModel: text}, () => this.fetchSpec(id))
                    this.changeSelected('Model', id, text)
                  }
                }
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="Tipe *" />
              <FormSelect
                label='carSpec'
                selectedValue={this.state.selectedType}
                items={this.state.Tipe}
                onSelectedItem={({text, id}) =>
                  {
                    this.setState({selectedType: text})
                    this.changeSelected('Tipe', id, text)

                  }
                }
              />
            </View>
            <View
              style={{
                width: '49%',
              }}>
              <Label text="Tahun *" />
              <FormSelect
                label="label"
                selectedValue={this.state.selectedTahun}
                items={data.Tahun}
                onSelectedItem={({text, id}) =>
                  {
                    this.setState({selectedTahun: text})
                    this.changeSelected("Tahun", id);
                  }
                }
              />
            </View>
          </View>

          
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="Kategori Warna *" />
              <FormSelect
                label="label"
                selectedValue={this.state.selectedWarna}
                items={KategoriWarna}
                onSelectedItem={({text, id}) =>{
                  this.setState({selectedWarna: text})
                  this.changeSelected("Kategori Warna", id);
                }}
              />
            </View>
            <View
              style={{
                width: '49%',
              }}>
              <Label text="Nama STNK *" />
              <TextInput
                value={this.state.NamaSTNK}
                style={{
                  borderWidth: 0.5,
                  borderRadius: 3,
                  padding: 7,
                  borderColor: 'grey',
                }}
                onChangeText={(NamaSTNK) => {
                  this.setState({NamaSTNK})
                  this.changeText("Nama STNK", NamaSTNK, "text")
                }}
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="Nilai STNK *" />
              <TextInput
                value={this.state.NilaiSTNK}
                keyboardType="number-pad"
                style={{
                  borderWidth: 0.5,
                  borderRadius: 3,
                  padding: 7,
                  borderColor: 'grey',
                }}
                onChangeText={(NilaiSTNK) => {
                  this.setState({NilaiSTNK})
                  this.changeText("Nilai STNK", NilaiSTNK, 'number');
                }}
              />
            </View>
            <View
              style={{
                width: '49%',
              }}>
              <Label text="STNK Hidup *" />
              <FormSelect
                label="label"
                selectedValue={this.state.selectedSTNKHidup}
                items={data.STNK_hidup}
                onSelectedItem={({text, id}) =>
                {
                  this.setState({selectedSTNKHidup: text})
                  this.changeSelected("STNK Hidup", id)
                }
                }
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="Banjir *" />
              <FormSelect
                label="label"
                selectedValue={this.state.selectedBanjir}
                items={data.Banjir}
                onSelectedItem={({text, id}) =>
                {
                  this.setState({selectedBanjir: text})
                  this.changeSelected("Banjir", id)
                }}
              />
            </View>
            <View
              style={{
                width: '49%',
              }}>
              <Label text="Kecelakaan *" />
              <FormSelect
                selectedValue={this.state.selectedKecelakaan}
                label="label"
                items={data.Kecelakaan}
                onSelectedItem={({text, id}) =>{
                  this.setState({selectedKecelakaan: text})
                  this.changeSelected("Kecelakaan", id)
                }}
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="Wheel *" />
              <FormSelect
                selectedValue={this.state.selectedWheel}
                label="label"
                items={data.Wheel}
                onSelectedItem={({text, id}) =>{
                  this.setState({selectedWheel: text})
                  this.changeSelected("Wheel", id)
                }}
              />
            </View>
            <View
              style={{
                width: '49%',
              }}>
              <Label text="Paint *" />
              <FormSelect
                selectedValue={this.state.selectedPaint}
                label="label"
                items={data.Paint}
                onSelectedItem={({text, id}) =>{
                  this.setState({selectedPaint: text})
                  this.changeSelected("Paint", id)
                }}
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="STNK *" />
              <FormSelect
                selectedValue={this.state.selectedSTNK}
                label="label"
                items={data.STNK}
                onSelectedItem={({text, id}) =>{
                  this.setState({selectedSTNK: text})
                  this.changeSelected("STNK",id)
                }}
              />
            </View>
            <View
              style={{
                width: '49%',
              }}>
              <Label text="Kebersihan Interior *" />
              <FormSelect
                selectedValue={this.state.selectedInterior}
                label="label"
                items={data.Kebersihan_interior}
                onSelectedItem={({text, id}) =>{
                  this.setState({selectedInterior: text})
                  this.changeSelected("Kebersihan Interior", id)
                }}
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="Nik/Form A *" />
              <FormSelect
                selectedValue={this.state.selectedNIK}
                label="label"
                items={data.NIK}
                onSelectedItem={({text, id}) =>{
                  this.setState({selectedNIK: text})
                  this.changeSelected("NIK/Form A", id)
                }}
              />
            </View>
            <View
              style={{
                width: '49%',
              }}>
              <Label text="Faktur *" />
              <FormSelect
                selectedValue={this.state.selectedFaktur}
                label="label"
                items={data.Faktur}
                onSelectedItem={({text, id}) =>{
                  this.setState({selectedFaktur: text})
                  this.changeSelected("Faktur", id)
                }}
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="Kunci Serep *" />
              <FormSelect
                selectedValue={this.state.selectedKunci}
                label="label"
                items={data.Kunci_serep}
                onSelectedItem={({text, id}) =>{
                  this.setState({selectedKunci: text})
                  this.changeSelected("Kunci Serep", id)
                }}
              />
            </View>
            <View
              style={{
                width: '49%',
              }}></View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '98%'}}>
              <Label text="Catatan *" />
              <TextInput
                numberOfLines={3}
                style={{
                  borderWidth: 0.5,
                  borderRadius: 3,
                  padding: 7,
                  borderColor: 'grey',
                }}
                value={this.state.KunciSerep}
                onChangeText={(KunciSerep) => {
                  this.setState({KunciSerep})
                }}
              />
            </View>
          </View>
        </View>
      </ScrollView>
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({Auth, PDI}) => ({
  Auth,
  PDI
});
const mapDispatchToProps = dispatch => bindActionCreators({ updatePDIData }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EditSummary);
const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: 'white',
    padding: 15,
  },
  headerContainer: {
    paddingVertical: widthPercentageToDP('0.6%'),
    width: widthPercentageToDP('100%'),
    paddingHorizontal: widthPercentageToDP('1.5%'),
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: Colors.strong_blue,
    alignItems: 'center'
  },
  headerTitle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: widthPercentageToDP('4.5%'),
    fontFamily: 'Roboto',
  },
  subheader: {
    backgroundColor: Colors.bright_red,
    paddingVertical: widthPercentageToDP('3%'),
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    paddingHorizontal: widthPercentageToDP('5%'),
    paddingVertical: widthPercentageToDP('5%'),
    backgroundColor: "#fff",
  },
  title: {
    fontSize: 20,
    fontWeight: "600",
    color: "#000",
    fontFamily: 'Roboto',
  },
  inputGroup: {
    marginBottom: 5,
  },
  inputForm: {
    width: widthPercentageToDP('90%'),
    borderWidth: 1.5,
    borderColor: Colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%')
  },
  inputLabel: {
    color: Colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: widthPercentageToDP('1%'),
    fontFamily: 'Roboto',
  }
});
