import React, { Component } from "react";
import { Text, TouchableOpacity, Alert, ToastAndroid } from "react-native";
import { connect } from "react-redux";
import _ from 'lodash'
import { requestUpdatePDI, requestFinishPDI } from "../../../actions";
import { requestUploadPhotoPDI, uploadPhotoProgress, finishRequestUploadPhotoPDI, failedRequestUploadPhotoPDI, fetchAllPDI } from '../../../actions/pdi_actions'
import { bindActionCreators } from "redux";
import { resetPDIData, setPDIData } from '../../../reducers/PDIData'
import RNFetchBlob from 'rn-fetch-blob'
import store from "../../../store"

const requiredSummary = [
    "Nama STNK", "Kategori Warna", "Nilai STNK", "STNK Hidup", "Banjir", "Kecelakaan", "Wheel", "Paint", "STNK", "Kebersihan Interior"
  ]
class ButtonSave extends Component {
    constructor(props) {
      super(props);
      this.state = {};
    }

    SaveToDraft = navigation => {
        console.log("PROP", this.props.PDI.DetailPDI)
        Alert.alert(
            "Save Data",
            "Please select an action.",
            [
            { text: "Cancel", onPress: () => console.log("cancel save data") },
            { text: "Save to draft", onPress: () => this.onSaveToDraft() },
            { text: "Finish Appraisal", onPress: () => this.onFinishedAppraisal() },
            ],
            { cancelable: false },
        );
    };

    async onSaveToDraft() {
        const {PDIData} = this.props

        if(PDIData){
            if(PDIData.data.length > 0){
                let promises = []
                await PDIData.data.map((item, idx) => {
                    let formData = []

                    formData.push({name: "image", filename: item.image.name, type: item.image.type, data: RNFetchBlob.wrap(item.image.path)});
                    formData.push({name: "field", data: item.field});
                    formData.push({name: "objectField", data: JSON.stringify(item.objectField)});

                    promises.push(this.uploadImages(this.props.PDI.DetailPDI.id, formData))
                })
                Promise.all(
                    promises
                ).then(result => {
                    let {data} = PDIData
                    while(data.length > 0){
                        data.pop();
                    }

                    this.props.requestUpdatePDI(this.props.PDI.DetailPDI, "saveDraft");
                    this.props.navigation.navigate("HomeMenu");
                }).catch(e => {
                    console.log("ERROR PROMISE", e)
                })
            } else {
                this.props.requestUpdatePDI(this.props.PDI.DetailPDI, "saveDraft");
                this.props.navigation.navigate("HomeMenu");
            }
        } else {
            this.props.requestUpdatePDI(this.props.PDI.DetailPDI, "saveDraft");
            this.props.navigation.navigate("HomeMenu");
        }

    }

    async onFinishedAppraisal() {
        const {PDIData, PDI} = this.props
        const {user} = this.props.Auth;
        let validSummary = false

        await Promise.all(
            PDI.DetailPDI.carSummary.some(function(item, i){
                if(requiredSummary.includes(item.fieldName)){
                    // console.log("Ada",item)
                    if(item.value == "" || item.value == null){
                        alert(`Harap isi data ${item.fieldName}`)
                        validDetail = false
                        return true;
                    }
        
                    validSummary = true
                    return false
                }
            })
        )
        
        if(validSummary){
            if(PDIData){
                if(PDIData.data.length > 0){
                    let promises = []
                    await PDIData.data.map((item, idx) => {
                        let formData = []
    
                        formData.push({name: "image", filename: item.image.name, type: item.image.type, data: RNFetchBlob.wrap(item.image.path)});
                        formData.push({name: "field", data: item.field});
                        formData.push({name: "objectField", data: JSON.stringify(item.objectField)});
    
                        promises.push(this.uploadImages(this.props.PDI.DetailPDI.id, formData))
                    })
                    Promise.all(
                        promises
                    ).then(result => {
                        let {data} = PDIData
                        while(data.length > 0){
                            data.pop();
                        }
    
                        this.props.requestFinishPDI(this.props.PDI.DetailPDI);
                        this.props.navigation.navigate("HomeMenu");
                        ToastAndroid.showWithGravity("Berhasil menyelesaikan PDI", 2000, ToastAndroid.CENTER);
                        
                    }).catch(e => {
                        console.log("ERROR PROMISE", e)
                    })
                } else {
                    this.props.requestFinishPDI(this.props.PDI.DetailPDI);
                    this.props.navigation.navigate("HomeMenu");
                    ToastAndroid.showWithGravity("Berhasil menyelesaikan PDI", 2000, ToastAndroid.CENTER);
                }
            } else {
                this.props.requestFinishPDI(this.props.PDI.DetailPDI);
                this.props.navigation.navigate("HomeMenu");
                ToastAndroid.showWithGravity("Berhasil menyelesaikan PDI", 2000, ToastAndroid.CENTER);
                
            }

        } 


    }

    uploadImages = (id, formData) => new Promise((resolve, reject) => {
        const {Auth} = store.getState()
        RNFetchBlob.fetch(
            'POST',
            `https://api.autotrust.id/api/v1/pdi/${id}/upload`,
            {
                Authorization : `Bearer ${Auth.token}`,
                'Content-Type' : 'multipart/form-data',
            },
            formData
        ).uploadProgress((written, total) => {
            let uploadProgress = Math.round((written * 100) / total);
            this.props.uploadPhotoProgress({ uploadProgress });
        }).then(result => {
            result = result.json()
            const {PDI} = store.getState()
            let docs
            if(result.field == "photoDocument"){
                docs = _.find(PDI.DetailPDI.photoDocument, {name: result.objName})
            } else if(result.field == "photoFrame"){
                docs = _.find(PDI.DetailPDI.photoFrame, {name: result.objName})
            } else if(result.field == "photoEngine"){
                docs = _.find(PDI.DetailPDI.photoEngine, {name: result.objName})
            } else if(result.field == "photoInterior"){
                docs = _.find(PDI.DetailPDI.photoInterior, {name: result.objName})
            } else if(result.field == "photoExterior"){
                docs = _.find(PDI.DetailPDI.photoExterior, {name: result.objName})
            }
      
            docs.imageUrl = result.imageUrl
            this.props.finishRequestUploadPhotoPDI(result.appraisal);
            resolve()
        })
        .catch(err => {
            console.log("ERR ", err);
            reject()
            this.props.failedRequestUploadPhotoPDI();
        });
    })

    render() {
        return (
            <TouchableOpacity style={{ marginRight: 30 }} onPress={() => this.SaveToDraft()}>
                <Text style={{ fontSize: 18, fontWeight: "bold", color: 'white'}}>Simpan</Text>
            </TouchableOpacity>
        );
    }
}



const mapStateToProps = ({ PDI, PDIData, Auth }) => ({ PDI, PDIData, Auth });

const mapDispatchToProps = dispatch => bindActionCreators({ requestFinishPDI, requestUpdatePDI, requestUploadPhotoPDI, uploadPhotoProgress, finishRequestUploadPhotoPDI, failedRequestUploadPhotoPDI, fetchAllPDI }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ButtonSave);