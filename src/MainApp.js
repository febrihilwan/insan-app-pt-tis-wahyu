/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import 'react-native-gesture-handler';
import {createDrawerNavigator, DrawerItems} from 'react-navigation-drawer';
import {Provider} from 'react-redux';
import {Image, TouchableOpacity, View, StyleSheet, Text} from 'react-native';
import store from './store';
import {widthPercentageToDP, heightPercentageToDP} from './utils/index';
import colors from './utils/colors';

import LoginScreen from './screen/LoginScreen/LoginScreen';
import AtmListScreen from './screen/AtmList/AtmList';
import ErrorListScreen from './screen/ErrorList/ErrorList';
import AppraisalListScreen from './screen/AppraisalList/AppraisalList';
import AppraisalScreen from './screen/Appraisal/Appraisal';
import HandNav from './screen/Handover/handoverNavigation';
import ProfileScreen from './screen/Profile/profile';
import InputDetailHandover from './screen/Handover/inputHandoverDetail';
import HistoryScreen from './screen/History/HistoryNavigation';
import MessageNav from './screen/Message/MessageNavigation';

// PDI screen
import HomePDIScreen from './screen/PDI/Home';
import EditDocumentPDIScreen from './screen/PDI/EditCar/Document';
import EditFramePDIScreen from './screen/PDI/EditCar/Frame';
import EditEnginePDIScreen from './screen/PDI/EditCar/Engine';
import EditInteriorPDIScreen from './screen/PDI/EditCar/Interior';
import EditEksteriorPDIScreen from './screen/PDI/EditCar/Eksterior';
import EditSummary from './screen/PDI/EditCar/EditSummary';
import PreviewSummary from './screen/PDI/PreviewCar/Summary';
import PreviewDocument from './screen/PDI/PreviewCar/Document';
import PreviewFrame from './screen/PDI/PreviewCar/Frame';
import PreviewEngine from './screen/PDI/PreviewCar/Engine';
import PreviewInterior from './screen/PDI/PreviewCar/Interior';
import PreviewEksterior from './screen/PDI/PreviewCar/Eksterior';
import ButtonSave from './screen/PDI/EditCar/ButtonSave';

// CS Screen
import CleaningListScreen from './screen/CleaningList/CleaningList';

const HomeMenu = createMaterialTopTabNavigator(
  {
    AppraisalList: {
      screen: AppraisalListScreen,
      navigationOptions: {
        title: 'List Problems',
      },
    },
    // untuk menambahkan tab swipe
    // Handover: HandNav,
  },
  {
    lazy: true,
    swipeEnabled: true,
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: colors.dope_blue,
      inactiveTintColor: colors.grayish_blue,
      style: {
        backgroundColor: 'white',
      },
      indicatorStyle: {
        backgroundColor: colors.strong_blue,
        height: 4,
      },
    },
  },
);

const HomeMenuAtm = createMaterialTopTabNavigator(
  {
    AppraisalList: {
      screen: AtmListScreen,
      navigationOptions: {
        title: 'List Machine',
      },
    },
    // untuk menambahkan tab swipe
    // Handover: HandNav,
  },
  {
    lazy: true,
    swipeEnabled: true,
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: colors.dope_blue,
      inactiveTintColor: colors.grayish_blue,
      style: {
        backgroundColor: 'white',
      },
      indicatorStyle: {
        backgroundColor: colors.strong_blue,
        height: 4,
      },
    },
  },
);

const HomeMenuError = createMaterialTopTabNavigator(
  {
    AppraisalList: {
      screen: ErrorListScreen,
      navigationOptions: {
        title: 'List Error',
      },
    },
    // untuk menambahkan tab swipe
    // Handover: HandNav,
  },
  {
    lazy: true,
    swipeEnabled: true,
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: colors.dope_blue,
      inactiveTintColor: colors.grayish_blue,
      style: {
        backgroundColor: 'white',
      },
      indicatorStyle: {
        backgroundColor: colors.strong_blue,
        height: 4,
      },
    },
  },
);

const HomeMenuCleaning = createMaterialTopTabNavigator(
  {
    AppraisalList: {
      screen: CleaningListScreen,
      navigationOptions: {
        title: 'List Cleaning',
      },
    },
    // untuk menambahkan tab swipe
    // Handover: HandNav,
  },
  {
    lazy: true,
    swipeEnabled: true,
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: colors.dope_blue,
      inactiveTintColor: colors.grayish_blue,
      style: {
        backgroundColor: 'white',
      },
      indicatorStyle: {
        backgroundColor: colors.strong_blue,
        height: 4,
      },
    },
  },
);

const HomeNavigator = createStackNavigator(
  {
    HomeMenu: {
      screen: HomeMenu,
      navigationOptions: ({navigation}) => ({
        title: 'Teknisi - List Problems',
        headerStyle: {
          backgroundColor: colors.strong_grey,
        },
        headerTintColor: 'white',
        headerLeft: (
          <TouchableOpacity
            style={{marginLeft: 10}}
            onPress={navigation.openDrawer}>
            <Image
              source={require('./assets/image/menu.png')}
              style={{
                width: widthPercentageToDP('8%'),
                height: widthPercentageToDP('8%'),
              }}
            />
          </TouchableOpacity>
        ),
        headerTitleStyle: {
          fontSize: widthPercentageToDP('5%'),
        },
      }),
    },
    Appraisal: {
      screen: AppraisalScreen,
      navigationOptions: {
        header: null,
      },
    },
    InputDetailHandover: {
      screen: InputDetailHandover,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: 'HomeMenu',
  },
);

const AtmNavigator = createStackNavigator(
  {
    HomeMenu: {
      screen: HomeMenuAtm,
      navigationOptions: ({navigation}) => ({
        title: 'Teknisi - List Machine',
        headerStyle: {
          backgroundColor: colors.strong_grey,
        },
        headerTintColor: 'white',
        headerLeft: (
          <TouchableOpacity
            style={{marginLeft: 10}}
            onPress={navigation.openDrawer}>
            <Image
              source={require('./assets/image/menu.png')}
              style={{
                width: widthPercentageToDP('8%'),
                height: widthPercentageToDP('8%'),
              }}
            />
          </TouchableOpacity>
        ),
        headerTitleStyle: {
          fontSize: widthPercentageToDP('5%'),
        },
      }),
    },
    Appraisal: {
      screen: AppraisalScreen,
      navigationOptions: {
        header: null,
      },
    },
    InputDetailHandover: {
      screen: InputDetailHandover,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: 'HomeMenu',
  },
);

const ErrorNavigator = createStackNavigator(
  {
    HomeMenu: {
      screen: HomeMenuError,
      navigationOptions: ({navigation}) => ({
        title: 'Teknisi - List Error',
        headerStyle: {
          backgroundColor: colors.strong_grey,
        },
        headerTintColor: 'white',
        headerLeft: (
          <TouchableOpacity
            style={{marginLeft: 10}}
            onPress={navigation.openDrawer}>
            <Image
              source={require('./assets/image/menu.png')}
              style={{
                width: widthPercentageToDP('8%'),
                height: widthPercentageToDP('8%'),
              }}
            />
          </TouchableOpacity>
        ),
        headerTitleStyle: {
          fontSize: widthPercentageToDP('5%'),
        },
      }),
    },
    Appraisal: {
      screen: AppraisalScreen,
      navigationOptions: {
        header: null,
      },
    },
    InputDetailHandover: {
      screen: InputDetailHandover,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: 'HomeMenu',
  },
);

const ProfileNav = createStackNavigator({
  Profile: {
    screen: ProfileScreen,
    navigationOptions: ({navigation}) => ({
      title: 'Profile',
      headerStyle: {
        backgroundColor: colors.strong_grey,
      },
      headerTintColor: 'white',
      headerLeft: (
        <TouchableOpacity
          style={{marginLeft: 10}}
          onPress={navigation.openDrawer}>
          <Image
            source={require('./assets/image/menu.png')}
            style={{
              width: widthPercentageToDP('8%'),
              height: widthPercentageToDP('8%'),
            }}
          />
        </TouchableOpacity>
      ),
      headerTitleStyle: {
        fontSize: widthPercentageToDP('5%'),
        fontWeight: '200',
        textAlign: 'center',
        marginLeft: widthPercentageToDP('25%'),
      },
    }),
  },
});

const AppNavigation = createDrawerNavigator(
  {
    Problems: {
      screen: HomeNavigator,
      navigationOptions: ({navigation}) => ({
        drawerIcon: (
          <Image
            source={require('./assets/image/ticket.png')}
            style={{
              width: widthPercentageToDP('8%'),
              height: widthPercentageToDP('8%'),
            }}
          />
        ),
      }),
    },
    // 'Error Code': {
    //   screen: ErrorNavigator,
    //   navigationOptions: ({navigation}) => ({
    //     drawerIcon: (
    //       <Image
    //         source={require('./assets/image/error_code.png')}
    //         style={{
    //           width: widthPercentageToDP('8%'),
    //           height: widthPercentageToDP('8%'),
    //         }}
    //       />
    //     ),
    //   }),
    // },
    Machine: {
      screen: AtmNavigator,
      navigationOptions: ({navigation}) => ({
        drawerIcon: (
          <Image
            source={require('./assets/image/atm.png')}
            style={{
              width: widthPercentageToDP('8%'),
              height: widthPercentageToDP('8%'),
            }}
          />
        ),
      }),
    },

    // 'Error Code': {
    //   screen: MessageNav,
    //   navigationOptions: ({navigation}) => ({
    //     drawerIcon: (
    //       <Image
    //         source={require('./assets/image/error_code.png')}
    //         style={{
    //           width: widthPercentageToDP('8%'),
    //           height: widthPercentageToDP('8%'),
    //         }}
    //       />
    //     ),
    //   }),
    // },
    // Cleaning: {
    //   screen: HistoryScreen,
    //   navigationOptions: ({navigation}) => ({
    //     drawerIcon: (
    //       <Image
    //         source={require('./assets/image/cleaning.png')}
    //         style={{
    //           width: widthPercentageToDP('8%'),
    //           height: widthPercentageToDP('8%'),
    //         }}
    //       />
    //     ),
    //   }),
    // },
    Profile: {
      screen: ProfileNav,
      navigationOptions: ({navigation}) => ({
        drawerIcon: (
          <Image
            source={require('./assets/image/user.png')}
            style={{
              width: widthPercentageToDP('8%'),
              height: widthPercentageToDP('8%'),
            }}
          />
        ),
      }),
    },
  },
  {
    contentOptions: {
      activeTintColor: colors.dope_blue,
      inactiveTintColor: colors.dope_blue,
      activeBackgroundColor: colors.very_light_grayish_blue,
      inactiveBackgroundColor: 'transparent',
      iconContainerStyle: {
        opacity: 1,
      },
    },
    contentComponent: (props) => (
      <View>
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: colors.strong_orange,
            paddingVertical: widthPercentageToDP('11%'),
          }}>
          <Image
            source={require('./assets/image/Insan_white.png')}
            style={{width: '80%', height: heightPercentageToDP('11%')}}
          />
        </View>
        <DrawerItems
          {...props}
          labelStyle={{fontSize: widthPercentageToDP('4%'), fontWeight: '200'}}
        />
      </View>
    ),
  },
);

//PDI navigator
const EditCarTab = createMaterialTopTabNavigator(
  {
    Document: {
      screen: EditDocumentPDIScreen,
      navigationOptions: {
        title: 'Document',
      },
    },
    Frame: {
      screen: EditFramePDIScreen,
      navigationOptions: {
        title: 'Frame',
      },
    },
    Engine: {
      screen: EditEnginePDIScreen,
      navigationOptions: {
        title: 'Engine',
      },
    },
    Interior: {
      screen: EditInteriorPDIScreen,
      navigationOptions: {
        title: 'Interior',
      },
    },
    Eksterior: {
      screen: EditEksteriorPDIScreen,
      navigationOptions: {
        title: 'Eksterior',
      },
    },
  },
  {
    lazy: true,
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: colors.strong_blue,
      inactiveTintColor: colors.dope_blue,
      style: {
        backgroundColor: 'white',
      },
      indicatorStyle: {
        backgroundColor: colors.strong_blue,
        height: 4,
      },
      labelStyle: {
        fontSize: widthPercentageToDP('3.5%'),
      },
      scrollEnabled: true,
      tabStyle: {
        paddingHorizontal: 0,
        width: widthPercentageToDP('25%'),
      },
    },
  },
);

const PreviewCarTab = createMaterialTopTabNavigator(
  {
    Document: {
      screen: PreviewDocument,
      navigationOptions: {
        title: 'Document',
      },
    },
    Frame: {
      screen: PreviewFrame,
      navigationOptions: {
        title: 'Frame',
      },
    },
    Engine: {
      screen: PreviewEngine,
      navigationOptions: {
        title: 'Engine',
      },
    },
    Interior: {
      screen: PreviewInterior,
      navigationOptions: {
        title: 'Interior',
      },
    },
    Eksterior: {
      screen: PreviewEksterior,
      navigationOptions: {
        title: 'Eksterior',
      },
    },
  },
  {
    lazy: true,
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: colors.strong_blue,
      inactiveTintColor: colors.dope_blue,
      style: {
        backgroundColor: 'white',
      },
      indicatorStyle: {
        backgroundColor: colors.strong_blue,
        height: 4,
      },
      labelStyle: {
        fontSize: widthPercentageToDP('3.5%'),
      },
      scrollEnabled: true,
      tabStyle: {
        paddingHorizontal: 0,
        width: widthPercentageToDP('25%'),
      },
    },
  },
);

EditCarTab.navigationOptions = ({navigation}) => ({
  title: 'Edit Mobil',
  headerStyle: {
    backgroundColor: colors.strong_blue,
  },
  headerTintColor: 'white',
  // headerLeft: (
  //   <TouchableOpacity
  //     style={{marginLeft: 10}}
  //     onPress={navigation.openDrawer}>
  //     <Image
  //       source={require('./assets/image/menu.png')}
  //       style={{
  //         width: widthPercentageToDP('8%'),
  //         height: widthPercentageToDP('8%'),
  //       }}
  //     />
  //   </TouchableOpacity>
  // ),
  headerRight: () => <ButtonSave navigation={navigation} />,
  headerTitleStyle: {
    fontSize: widthPercentageToDP('5%'),
  },
});

PreviewCarTab.navigationOptions = ({navigation}) => ({
  title: 'Preview Mobil',
  headerStyle: {
    backgroundColor: colors.strong_blue,
  },
  headerTintColor: 'white',
  // headerLeft: (
  //   <TouchableOpacity
  //     style={{marginLeft: 10}}
  //     onPress={navigation.openDrawer}>
  //     <Image
  //       source={require('./assets/image/menu.png')}
  //       style={{
  //         width: widthPercentageToDP('8%'),
  //         height: widthPercentageToDP('8%'),
  //       }}
  //     />
  //   </TouchableOpacity>
  // ),
  headerTitleStyle: {
    fontSize: widthPercentageToDP('5%'),
  },
});

const EditPDI = createStackNavigator(
  {
    CarDetail: {
      screen: EditSummary,
      navigationOptions: ({navigation}) => ({
        header: null,
      }),
    },
    TabDetail: {
      screen: EditCarTab,
    },
  },
  {
    initialRouteName: 'CarDetail',
  },
);

const PreviewPDI = createStackNavigator(
  {
    CarDetail: {
      screen: PreviewSummary,
      navigationOptions: ({navigation}) => ({
        header: null,
      }),
    },
    TabDetail: {
      screen: PreviewCarTab,
    },
  },
  {
    initialRouteName: 'CarDetail',
  },
);

const HomePDINavigator = createStackNavigator(
  {
    HomeMenu: {
      screen: HomePDIScreen,
      navigationOptions: ({navigation}) => ({
        title: 'Beranda',
        headerStyle: {
          backgroundColor: colors.strong_blue,
        },
        headerTintColor: 'white',
        headerLeft: (
          <TouchableOpacity
            style={{marginLeft: 10}}
            onPress={navigation.openDrawer}>
            <Image
              source={require('./assets/image/menu.png')}
              style={{
                width: widthPercentageToDP('8%'),
                height: widthPercentageToDP('8%'),
              }}
            />
          </TouchableOpacity>
        ),
        headerTitleStyle: {
          fontSize: widthPercentageToDP('5%'),
        },
      }),
    },
    PreviewMenu: {
      screen: PreviewSummary,
      navigationOptions: ({navigation}) => ({
        title: 'Input Detail Mobil',
        headerStyle: {
          backgroundColor: colors.strong_blue,
        },
        headerTintColor: 'white',
        headerTitleStyle: {
          fontSize: widthPercentageToDP('5%'),
          fontWeight: '200',
          textAlign: 'center',
        },
        headerRight: () => (
          <TouchableOpacity style={{marginRight: 10}}>
            <Text style={{color: 'white'}}>Simpan</Text>
          </TouchableOpacity>
        ),
      }),
    },
    EditMenu: {
      screen: EditPDI,
      navigationOptions: ({navigation}) => ({
        header: null,
      }),
    },
    PreviewMenu: {
      screen: PreviewPDI,
      navigationOptions: ({navigation}) => ({
        header: null,
      }),
    },
  },
  {
    initialRouteName: 'HomeMenu',
  },
);

const HomeCSNavigator = createStackNavigator(
  {
    HomeMenu: {
      screen: HomeMenuAtm,
      navigationOptions: ({navigation}) => ({
        title: 'CS - List Machine',
        headerStyle: {
          backgroundColor: colors.strong_grey,
        },
        headerTintColor: 'white',
        headerLeft: (
          <TouchableOpacity
            style={{marginLeft: 10}}
            onPress={navigation.openDrawer}>
            <Image
              source={require('./assets/image/menu.png')}
              style={{
                width: widthPercentageToDP('8%'),
                height: widthPercentageToDP('8%'),
              }}
            />
          </TouchableOpacity>
        ),
        headerTitleStyle: {
          fontSize: widthPercentageToDP('5%'),
        },
      }),
    },
    PreviewMenu: {
      screen: PreviewSummary,
      navigationOptions: ({navigation}) => ({
        title: 'Input Detail Mobil',
        headerStyle: {
          backgroundColor: colors.strong_blue,
        },
        headerTintColor: 'white',
        headerTitleStyle: {
          fontSize: widthPercentageToDP('5%'),
          fontWeight: '200',
          textAlign: 'center',
        },
        headerRight: () => (
          <TouchableOpacity style={{marginRight: 10}}>
            <Text style={{color: 'white'}}>Simpan</Text>
          </TouchableOpacity>
        ),
      }),
    },
    EditMenu: {
      screen: EditPDI,
      navigationOptions: ({navigation}) => ({
        header: null,
      }),
    },
    PreviewMenu: {
      screen: PreviewPDI,
      navigationOptions: ({navigation}) => ({
        header: null,
      }),
    },
  },
  {
    initialRouteName: 'HomeMenu',
  },
);

const HomeCSNavigatorCleaning = createStackNavigator(
  {
    HomeMenu: {
      screen: HomeMenuCleaning,
      navigationOptions: ({navigation}) => ({
        title: 'CS - List Machine',
        headerStyle: {
          backgroundColor: colors.strong_grey,
        },
        headerTintColor: 'white',
        headerLeft: (
          <TouchableOpacity
            style={{marginLeft: 10}}
            onPress={navigation.openDrawer}>
            <Image
              source={require('./assets/image/menu.png')}
              style={{
                width: widthPercentageToDP('8%'),
                height: widthPercentageToDP('8%'),
              }}
            />
          </TouchableOpacity>
        ),
        headerTitleStyle: {
          fontSize: widthPercentageToDP('5%'),
        },
      }),
    },
    PreviewMenu: {
      screen: PreviewSummary,
      navigationOptions: ({navigation}) => ({
        title: 'Input Detail Mobil',
        headerStyle: {
          backgroundColor: colors.strong_blue,
        },
        headerTintColor: 'white',
        headerTitleStyle: {
          fontSize: widthPercentageToDP('5%'),
          fontWeight: '200',
          textAlign: 'center',
        },
        headerRight: () => (
          <TouchableOpacity style={{marginRight: 10}}>
            <Text style={{color: 'white'}}>Simpan</Text>
          </TouchableOpacity>
        ),
      }),
    },
    EditMenu: {
      screen: EditPDI,
      navigationOptions: ({navigation}) => ({
        header: null,
      }),
    },
    PreviewMenu: {
      screen: PreviewPDI,
      navigationOptions: ({navigation}) => ({
        header: null,
      }),
    },
  },
  {
    initialRouteName: 'HomeMenu',
  },
);

const PDINavigator = createDrawerNavigator(
  {
    Home: {
      screen: HomePDINavigator,
      navigationOptions: ({navigation}) => ({
        drawerIcon: (
          <Image
            source={require('./assets/image/home.png')}
            style={{
              width: widthPercentageToDP('8%'),
              height: widthPercentageToDP('8%'),
            }}
          />
        ),
      }),
    },
  },
  {
    contentOptions: {
      activeTintColor: colors.dope_blue,
      inactiveTintColor: colors.dope_blue,
      activeBackgroundColor: colors.very_light_grayish_blue,
      inactiveBackgroundColor: 'transparent',
      iconContainerStyle: {
        opacity: 1,
      },
    },
    contentComponent: (props) => (
      <View>
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: colors.strong_blue,
            paddingVertical: widthPercentageToDP('11%'),
          }}>
          <Image
            source={require('./assets/image/logo-name-white.png')}
            style={{width: '90%', height: heightPercentageToDP('7%')}}
          />
        </View>
        <DrawerItems
          {...props}
          labelStyle={{fontSize: widthPercentageToDP('4%'), fontWeight: '200'}}
        />
      </View>
    ),
  },
);

const CSNavigator = createDrawerNavigator(
  {
    Cleaning: {
      screen: HomeCSNavigatorCleaning,
      navigationOptions: ({navigation}) => ({
        drawerIcon: (
          <Image
            source={require('./assets/image/cleaning.png')}
            style={{
              width: widthPercentageToDP('8%'),
              height: widthPercentageToDP('8%'),
            }}
          />
        ),
      }),
    },
    'List Machine': {
      screen: HomeCSNavigator,
      navigationOptions: ({navigation}) => ({
        drawerIcon: (
          <Image
            source={require('./assets/image/atm.png')}
            style={{
              width: widthPercentageToDP('8%'),
              height: widthPercentageToDP('8%'),
            }}
          />
        ),
      }),
    },

    Profile: {
      screen: ProfileNav,
      navigationOptions: ({navigation}) => ({
        drawerIcon: (
          <Image
            source={require('./assets/image/user.png')}
            style={{
              width: widthPercentageToDP('8%'),
              height: widthPercentageToDP('8%'),
            }}
          />
        ),
      }),
    },
  },
  {
    contentOptions: {
      activeTintColor: colors.dope_blue,
      inactiveTintColor: colors.dope_blue,
      activeBackgroundColor: colors.very_light_grayish_blue,
      inactiveBackgroundColor: 'transparent',
      iconContainerStyle: {
        opacity: 1,
      },
    },
    contentComponent: (props) => (
      <View>
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: colors.strong_orange,
            paddingVertical: widthPercentageToDP('11%'),
          }}>
          <Image
            source={require('./assets/image/Insan_white.png')}
            style={{width: '80%', height: heightPercentageToDP('11%')}}
          />
        </View>
        <DrawerItems
          {...props}
          labelStyle={{fontSize: widthPercentageToDP('4%'), fontWeight: '200'}}
        />
      </View>
    ),
  },
);

//

const MainNavigator = createAppContainer(
  createSwitchNavigator(
    {
      Login: LoginScreen,
      App: AppNavigation,
      PDI: PDINavigator,
      CS: CSNavigator,
    },
    {
      initialRouteName: 'Login',
    },
  ),
);

const MainApp = () => (
  <Provider store={store}>
    <MainNavigator />
  </Provider>
);

export default MainApp;

const styles = StyleSheet.create({});
