import {AsyncStorage} from 'react-native';
const defaultState = {
  isAuthenticated: false,
  isload: false,
};
export default (state = defaultState, action) => {
  switch (action.type) {
    case 'AUTH_REQUEST':
      return {isAuthenticated: false, authRequest: true, isload: true};
    case 'AUTH_REQUEST_FAILED':
      return {
        isAuthenticated: false,
        error: action.payload.message,
        isload: false,
      };
    case 'AUTH_REQUEST_SUCCESS':
      return {isAuthenticated: true, ...action.payload, isload: false};
    case 'AUTH_LOGOUT':
      AsyncStorage.clear();
      return {isAuthenticated: false, isLogout: true};
    default:
      return state;
  }
};
