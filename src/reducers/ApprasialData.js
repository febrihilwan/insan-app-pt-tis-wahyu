export const SET_APPRASIAL_DATA = "apprasial/setApprasialData"
export const RESET_APPRASIAL_DATA = "apprasial/resetApprasialData"


export function setApprasialData(payload){
    return {
        type: SET_APPRASIAL_DATA,
        payload
    }
}

export function resetApprasialData(){
    return {
        type: RESET_APPRASIAL_DATA
    }
}


let initialState = {
    data: []
}

export default function reducer(state = initialState, action){
    switch(action.type){
        case SET_APPRASIAL_DATA:
            return state.data = payload;
        case RESET_APPRASIAL_DATA:
            return initialState;    
        default: 
            return state;
    }
}

export const getData = state => {state.data}