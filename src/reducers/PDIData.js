export const SET_PDI_DATA = "PDI/setPDIData"
export const RESET_PDI_DATA = "PDI/resetPDIData"


export function setPDIData(payload){
    return {
        type: SET_PDI_DATA,
        payload
    }
}

export function resetPDIData(){
    return {
        type: RESET_PDI_DATA
    }
}


let initialState = {
    data: []
}

export default function reducer(state = initialState, action){
    switch(action.type){
        case SET_PDI_DATA:
            return state.data = payload;
        case RESET_PDI_DATA:
            return initialState;    
        default: 
            return state;
    }
}

export const getData = state => {state.data}