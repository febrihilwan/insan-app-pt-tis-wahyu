import _ from "lodash";
import { AsyncStorage } from "react-native";

const defaultState = {
};
export default (state = defaultState, action) => {
  const { payload } = action;
  switch (action.type) {
    case "LIST_PDI_REQUEST":
      return { onRequestListPDI: true };
    case "LIST_PDI_REQUEST_SUCCESS":
      return { 
        ...state,
        ListPDI: action.payload,
        successRequestListPDI: true
      };
    case "LIST_PDI_REQUEST_FAILED":
      return { 
        ...state,
        onRequestListPDI: false,  
        successRequestListPDI: false,
        errorRequestListPDI: true,
        errorMessagePDI: action.error
      };
    case "DETAIL_PDI_REQUEST":
      return { ...state, onRequestDetailPDI: true };
    case "DETAIL_PDI_REQUEST_SUCCESS":
      return { 
        ...state,
        DetailPDI: action.payload,
        successRequestDetailDI: true,
        onRequestDetailPDI: false
      };
    case "DETAIL_PDI_REQUEST_FAILED":
      return { 
        ...state,
        onRequestDetailPDI: false,  
        successRequestDetailDI: false,
        errorRequestDetailPDI: true,
        errorMessagePDI: action.error
      };
    case "PDI_UPDATE_REQUEST":
      return {
        ...state,
        spinner: true,
        onRequestUpdateAppraisal: true,
      };
    case "PDI_UPDATE_REQUEST_SUCCESS":
      return {
        ...state,
        spinner: false,
        successUpdateAppraisal: true,
        ...payload,
        updateApa: action.updateApa,
      };
    case "PDI_UPDATE_REQUEST_FAILED":
      return {
        ...state,
        spinner: false,
        failedInputCarDetail: true,
      };
    case "PDI_DETAIL_SAVE_STORAGE":
      AsyncStorage.setItem(`pdi-${state.id}`, state);
      return state;
    case "PDI_DETAIL_UPLOAD":
      return {
        ...state,
        [payload.photoCategory]: { ...state[payload.photoCategory], [payload.photoPart]: payload.photoDetail },
      };
    case "UPLOAD_PHOTO_PROGRESS":
      return {
        ...state,
        spinner: true,
        progresUpload: true,
        uploadProgress: payload.uploadProgress,
        displayUploadModal: true,
      };
    case "PDI_UPLOAD_REQUEST":
      return {
        ...state,
        onRequestUploadPhoto: true,
        message: "",
      };
    case "PDI_UPLOAD_REQUEST_SUCCESS":
      return {
        ...state,
        successUploadPhoto: true,
        spinner: false,
        progresUpload: false,
        message: "Foto Berhasil Diupload",
        ...payload,
      };
    case "PDI_UPLOAD_REQUEST_FAILED":
      return {
        ...state,
        errorUploadPhoto: true,
        spinner: false,
        progresUpload: false,
        message: "Foto Gagal Diupload",
      };

    case "PDI_UPDATE_DATA":
      return {
        ...state,
        [payload.partName]: state.DetailPDI[payload.partName].map(
          (item, index) => (index === payload.index ? payload.data : item),
        ),
      };

    case "PDI_FINISH_REQUEST":
      return { ...state, onRequestFinishPDI: true };
    case "PDI_FINISH_REQUEST_SUCCESS":
      return { ...payload };
    case "PDI_FINISH_REQUEST_FAILED":
      return { errorFinishPDI: true };
  

    default:
      return state;
  }
};
